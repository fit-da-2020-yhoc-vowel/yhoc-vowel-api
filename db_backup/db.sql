-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: yhoc_vowel
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.18.04.1
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */
;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */
;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */
;
/*!40101 SET NAMES utf8 */
;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */
;
/*!40103 SET TIME_ZONE='+00:00' */
;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */
;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */
;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */
;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */
;
--
-- Current Database: `yhoc_vowel`
--
CREATE DATABASE
/*!32312 IF NOT EXISTS*/
`yhoc_vowel`
/*!40100 DEFAULT CHARACTER SET utf8mb4 */
;
USE `yhoc_vowel`;
--
-- Table structure for table `category`
--
DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */
;
/*!40101 SET character_set_client = utf8 */
;
CREATE TABLE `category` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `name` tinytext NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */
;
--
-- Dumping data for table `category`
--
LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */
;
INSERT INTO `category`
VALUES (
    'c8e48d4c-75dc-4c18-b03a-099d67a38e41',
    'Phụ âm',
    '/images/banner2.png',
    0,
    '2020-06-06 10:37:47',
    '2020-06-06 10:37:47'
  ),
  (
    'deb10fb4-6277-4a5f-a52e-d4830eebeb47',
    'Nguyên âm',
    '/images/banner1.png',
    0,
    '2020-06-06 10:37:39',
    '2020-06-06 10:37:39'
  ),
  (
    'e2360b0a-aa6b-4ed1-9a33-0401f661a8b0',
    'Thanh điệu',
    '/images/banner3.png',
    0,
    '2020-06-06 10:38:00',
    '2020-06-06 10:38:00'
  );
/*!40000 ALTER TABLE `category` ENABLE KEYS */
;
UNLOCK TABLES;
--
-- Table structure for table `connect_doctor`
--
DROP TABLE IF EXISTS `connect_doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */
;
/*!40101 SET character_set_client = utf8 */
;
CREATE TABLE `connect_doctor` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `patient_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `doctor_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'WAITING',
  `invitation_origin` varchar(255) NOT NULL DEFAULT 'PATIENT_INVITATION',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `doctor_id` (`doctor_id`),
  CONSTRAINT `connect_doctor_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `user` (`id`),
  CONSTRAINT `connect_doctor_ibfk_2` FOREIGN KEY (`doctor_id`) REFERENCES `user` (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */
;
--
-- Dumping data for table `connect_doctor`
--
LOCK TABLES `connect_doctor` WRITE;
/*!40000 ALTER TABLE `connect_doctor` DISABLE KEYS */
;
/*!40000 ALTER TABLE `connect_doctor` ENABLE KEYS */
;
UNLOCK TABLES;
--
-- Table structure for table `doctor_information`
--
DROP TABLE IF EXISTS `doctor_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */
;
/*!40101 SET character_set_client = utf8 */
;
CREATE TABLE `doctor_information` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `doctor_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `address` tinytext,
  `hospital_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `doctor_id` (`doctor_id`),
  KEY `hospital_id` (`hospital_id`),
  CONSTRAINT `doctor_information_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `user` (`id`),
  CONSTRAINT `doctor_information_ibfk_2` FOREIGN KEY (`hospital_id`) REFERENCES `hospital` (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */
;
--
-- Dumping data for table `doctor_information`
--
LOCK TABLES `doctor_information` WRITE;
/*!40000 ALTER TABLE `doctor_information` DISABLE KEYS */
;
INSERT INTO `doctor_information`
VALUES (
    '08f823cb-f8a9-49f2-94a7-57d508e8b41a',
    '01c5c4b7-356b-4452-bd99-5ee44b8db2e0',
    null,
    'd3aeeaa1-8c42-41f8-ab14-a5b797e78e59',
    0,
    '2020-07-17 09:18:05',
    '2020-07-17 09:18:05'
  ),
  (
    '3476e1b3-e7a7-4bb6-9b60-31bd48ce03c4',
    'f9e89d82-42de-4975-8275-7b5a6ab3f1ff',
    null,
    'd3aeeaa1-8c42-41f8-ab14-a5b797e78e59',
    0,
    '2020-07-17 09:18:41',
    '2020-07-17 09:18:41'
  ),
  (
    '40eb1d52-6d7c-4132-bc49-b87920e034b8',
    'f9e89d82-42de-4975-8275-7b5a6ab3f1rr',
    null,
    'd3aeeaa1-8c42-41f8-ab14-a5b797e78e59',
    0,
    '2020-07-17 09:21:36',
    '2020-07-17 09:21:36'
  ),
  (
    'd4986a4b-1479-44ad-b702-e36f458313ca',
    'f9e89d82-42de-4975-8275-7b5a6ab3f1e2',
    'Skyyyy',
    '9aa04dbd-4f17-4de5-a758-d7f184a912f9',
    0,
    '2020-07-17 09:18:32',
    '2020-07-17 09:21:24'
  ),
  (
    'd5379e40-c31f-4475-83fe-4577a64359ac',
    'f9e89d82-42de-4975-8275-7b5a6ab3f1rr',
    null,
    'd3aeeaa1-8c42-41f8-ab14-a5b797e78e59',
    0,
    '2020-07-17 09:18:56',
    '2020-07-17 09:18:56'
  );
/*!40000 ALTER TABLE `doctor_information` ENABLE KEYS */
;
UNLOCK TABLES;
--
-- Table structure for table `exercise`
--
DROP TABLE IF EXISTS `exercise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */
;
/*!40101 SET character_set_client = utf8 */
;
CREATE TABLE `exercise` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `exercise_type_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `name` tinytext NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `exercise_type_id` (`exercise_type_id`),
  CONSTRAINT `exercise_ibfk_1` FOREIGN KEY (`exercise_type_id`) REFERENCES `exercise_type` (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */
;
--
-- Dumping data for table `exercise`
--
LOCK TABLES `exercise` WRITE;
/*!40000 ALTER TABLE `exercise` DISABLE KEYS */
;
INSERT INTO `exercise`
VALUES (
    '04561d7f-185f-468c-a3af-f76cfd435c30',
    '8e408d1e-9180-4b83-8e02-c7f52e9cef26',
    'PE',
    0,
    '2020-06-07 04:55:41',
    '2020-06-07 04:55:41'
  ),
  (
    '08047fcf-56b6-4085-b861-4d5248be25f1',
    '66758e44-6537-4446-8909-3c2da5212712',
    'HO',
    0,
    '2020-06-07 05:00:24',
    '2020-06-07 05:00:24'
  ),
  (
    '13e91368-0094-4cfa-87d1-68e91c5d5168',
    '81d6f089-43dc-4dab-9329-c9db9a88a395',
    'TẠ',
    0,
    '2020-06-07 06:42:41',
    '2020-06-07 06:42:41'
  ),
  (
    '16ef8159-15f6-4573-b3fa-3a8865046c52',
    '66758e44-6537-4446-8909-3c2da5212712',
    'HI',
    0,
    '2020-06-07 05:00:20',
    '2020-06-07 05:00:20'
  ),
  (
    '17ccde0e-bc51-4ae5-ae70-62216a9ba300',
    'c341cb2d-ff59-4690-94b8-638d301aa5de',
    'FI',
    0,
    '2020-06-07 04:58:13',
    '2020-06-07 04:58:13'
  ),
  (
    '1e092047-4fe4-4b28-b09c-c4b06944677d',
    '81d6f089-43dc-4dab-9329-c9db9a88a395',
    'TÃ',
    0,
    '2020-06-07 06:42:46',
    '2020-06-07 06:42:46'
  ),
  (
    '297a3999-52a5-469b-b72b-aa8287f7815c',
    '245501c5-9534-4b69-be94-e2c9c4263d57',
    'MẠ',
    0,
    '2020-06-07 06:43:36',
    '2020-06-07 06:43:36'
  ),
  (
    '299cb4ca-68ca-4193-8057-27cf66709ce9',
    '81d6f089-43dc-4dab-9329-c9db9a88a395',
    'TẢ',
    0,
    '2020-06-07 06:42:35',
    '2020-06-07 06:42:35'
  ),
  (
    '2bd55c62-f4f7-4567-82d6-37a9a0ff1289',
    '4adeca72-ffa0-4006-8599-51a8f98e0446',
    'U',
    0,
    '2020-06-07 04:40:46',
    '2020-06-07 04:40:46'
  ),
  (
    '2f61f1d5-9c36-4232-8296-e1223ef28730',
    '245501c5-9534-4b69-be94-e2c9c4263d57',
    'MẢ',
    0,
    '2020-06-07 06:43:31',
    '2020-06-07 06:43:31'
  ),
  (
    '344f7bae-5b60-445f-9f2f-d6d0baf2e4b3',
    '125472b4-7034-4b33-ab1a-d711fc08a137',
    'KO',
    0,
    '2020-06-07 04:57:38',
    '2020-06-07 04:57:38'
  ),
  (
    '3de7e168-ed70-457c-bcac-aa6b43a3bb42',
    'f6690034-a876-440f-8449-3353c3ca10a7',
    'SA',
    0,
    '2020-06-07 04:58:46',
    '2020-06-07 04:58:46'
  ),
  (
    '414b3b0d-011d-4119-a335-414660290124',
    'c341cb2d-ff59-4690-94b8-638d301aa5de',
    'FO',
    0,
    '2020-06-07 04:58:17',
    '2020-06-07 04:58:17'
  ),
  (
    '41bf11b6-2e7d-4924-9d51-bdfa46bc5c31',
    '245501c5-9534-4b69-be94-e2c9c4263d57',
    'MÃ',
    0,
    '2020-06-07 06:43:41',
    '2020-06-07 06:43:41'
  ),
  (
    '441e59f3-368c-4696-80bf-e3322405883d',
    '67a9640a-3799-436c-b21e-06e0b27b9227',
    'O',
    0,
    '2020-06-07 04:40:29',
    '2020-06-07 04:40:29'
  ),
  (
    '4478d17a-ef8f-421e-9dad-a1ec1053a919',
    '66758e44-6537-4446-8909-3c2da5212712',
    'HA',
    0,
    '2020-06-07 05:00:11',
    '2020-06-07 05:00:11'
  ),
  (
    '47772fb5-3168-4162-b3fe-e1a9ef442318',
    '3e3916fc-f72f-40b3-8830-82a60271865a',
    'CHO',
    0,
    '2020-06-07 04:59:42',
    '2020-06-07 04:59:42'
  ),
  (
    '5ccb4beb-be87-47b8-84ff-d59c916910b0',
    '9ff3838c-a19d-4a9d-afcb-46c4ec16115a',
    'I',
    0,
    '2020-06-07 04:40:12',
    '2020-06-07 04:40:12'
  ),
  (
    '60da16b7-f4fd-4c03-88e1-4ad86989799e',
    '3e3916fc-f72f-40b3-8830-82a60271865a',
    'CHA',
    0,
    '2020-06-07 04:59:28',
    '2020-06-07 04:59:28'
  ),
  (
    '610f2964-cd98-4abe-a1ff-d0978c863fb6',
    '3e3916fc-f72f-40b3-8830-82a60271865a',
    'CHI',
    0,
    '2020-06-07 04:59:37',
    '2020-06-07 04:59:37'
  ),
  (
    '68ea0d1b-1c4c-406b-b657-edd70bbc23a2',
    'c341cb2d-ff59-4690-94b8-638d301aa5de',
    'FA',
    0,
    '2020-06-07 04:58:03',
    '2020-06-07 04:58:03'
  ),
  (
    '6d9cf54b-472b-494e-a644-a1c967488370',
    '125472b4-7034-4b33-ab1a-d711fc08a137',
    'KE',
    0,
    '2020-06-07 04:57:29',
    '2020-06-07 04:57:29'
  ),
  (
    '6e029e6a-9274-40a6-8e8e-bd52086960d6',
    'f6690034-a876-440f-8449-3353c3ca10a7',
    'SO',
    0,
    '2020-06-07 04:59:00',
    '2020-06-07 04:59:00'
  ),
  (
    '6fa89028-0ddb-44cf-821f-7d897156167c',
    '8e408d1e-9180-4b83-8e02-c7f52e9cef26',
    'PI',
    0,
    '2020-06-07 04:55:46',
    '2020-06-07 04:55:46'
  ),
  (
    '73041751-f845-4587-abfa-fd7c39ca9a33',
    '125472b4-7034-4b33-ab1a-d711fc08a137',
    'KI',
    0,
    '2020-06-07 04:57:33',
    '2020-06-07 04:57:33'
  ),
  (
    '752456fc-dc0d-47c2-9ba6-dc357760e099',
    'abb5f16a-f0e7-44d9-a8e7-877d1bca7dcb',
    'TU',
    0,
    '2020-06-07 04:56:52',
    '2020-06-07 04:56:52'
  ),
  (
    '75badf69-0704-4199-93fc-1319315df2b9',
    '66758e44-6537-4446-8909-3c2da5212712',
    'HE',
    0,
    '2020-06-07 05:00:15',
    '2020-06-07 05:00:15'
  ),
  (
    '762e7cdf-c7ce-4b5a-9bfa-e3b664bea08a',
    'a9a84629-aa44-474c-83e2-a5c548d0e82a',
    'E',
    0,
    '2020-06-07 04:39:39',
    '2020-06-07 04:39:39'
  ),
  (
    '8953f865-07cb-44ac-bb8f-4f9afdd54f2f',
    '245501c5-9534-4b69-be94-e2c9c4263d57',
    'MA',
    0,
    '2020-06-07 06:43:15',
    '2020-06-07 06:43:15'
  ),
  (
    '8b71146f-86c4-4b09-a2a9-7aaa19e4478f',
    '8e408d1e-9180-4b83-8e02-c7f52e9cef26',
    'PO',
    0,
    '2020-06-07 04:55:51',
    '2020-06-07 04:55:51'
  ),
  (
    '92220956-369a-4319-bdae-428cb45e6ed2',
    'abb5f16a-f0e7-44d9-a8e7-877d1bca7dcb',
    'TO',
    0,
    '2020-06-07 04:56:48',
    '2020-06-07 04:56:48'
  ),
  (
    '92227433-c6d3-4e2a-b4ea-77e3d9f4a239',
    '8e408d1e-9180-4b83-8e02-c7f52e9cef26',
    'PA',
    0,
    '2020-06-07 04:55:19',
    '2020-06-07 04:55:19'
  ),
  (
    '9bf9d262-0f8d-4aaf-9075-1b4dfc0dfd92',
    'c341cb2d-ff59-4690-94b8-638d301aa5de',
    'FE',
    0,
    '2020-06-07 04:58:08',
    '2020-06-07 04:58:08'
  ),
  (
    'a83c8737-20d2-43de-b89f-fe0c5c7e4cea',
    '245501c5-9534-4b69-be94-e2c9c4263d57',
    'MÀ',
    0,
    '2020-06-07 06:43:26',
    '2020-06-07 06:43:26'
  ),
  (
    'b128860c-78be-47fb-9467-6107208afdcb',
    '245501c5-9534-4b69-be94-e2c9c4263d57',
    'MÁ',
    0,
    '2020-06-07 06:43:20',
    '2020-06-07 06:43:20'
  ),
  (
    'b1f69d7f-a8a2-43e8-b0f2-8b3ceacc4df6',
    'f6690034-a876-440f-8449-3353c3ca10a7',
    'SU',
    0,
    '2020-06-07 04:59:05',
    '2020-06-07 04:59:05'
  ),
  (
    'c9e68b88-288c-4033-96c9-692dd242c875',
    '3e3916fc-f72f-40b3-8830-82a60271865a',
    'CHU',
    0,
    '2020-06-07 04:59:47',
    '2020-06-07 04:59:47'
  ),
  (
    'cabcc7f6-d33b-40c1-b709-8a43e6ad7e3f',
    '8e408d1e-9180-4b83-8e02-c7f52e9cef26',
    'PU',
    0,
    '2020-06-07 04:55:56',
    '2020-06-07 04:55:56'
  ),
  (
    'cdf8f0a9-7785-496c-8e46-ea8cda537ddc',
    'abb5f16a-f0e7-44d9-a8e7-877d1bca7dcb',
    'TI',
    0,
    '2020-06-07 04:56:43',
    '2020-06-07 04:56:43'
  ),
  (
    'ce67a84d-c25a-4a18-9a85-f5711f6a4a06',
    '125472b4-7034-4b33-ab1a-d711fc08a137',
    'KU',
    0,
    '2020-06-07 04:57:42',
    '2020-06-07 04:57:42'
  ),
  (
    'd6b5ff64-1ff1-480a-938f-ed196517ec67',
    '3e3916fc-f72f-40b3-8830-82a60271865a',
    'CHE',
    0,
    '2020-06-07 04:59:32',
    '2020-06-07 04:59:32'
  ),
  (
    'd9d7b156-1b19-4252-8bc1-650dc2ca5bf8',
    '81d6f089-43dc-4dab-9329-c9db9a88a395',
    'TA',
    0,
    '2020-06-07 05:16:33',
    '2020-06-07 05:16:33'
  ),
  (
    'dc45a878-8f63-4e93-a0e0-39234f010b69',
    'c341cb2d-ff59-4690-94b8-638d301aa5de',
    'FU',
    0,
    '2020-06-07 04:58:22',
    '2020-06-07 04:58:22'
  ),
  (
    'e112a39a-f6b1-4125-a28e-55a500d8fb27',
    'abb5f16a-f0e7-44d9-a8e7-877d1bca7dcb',
    'TE',
    0,
    '2020-06-07 04:56:38',
    '2020-06-07 04:56:38'
  ),
  (
    'e12e0056-091f-46d0-88ce-f347c3a746fb',
    '850910e4-c17c-4e47-a8e9-7d034014e0b2',
    'A',
    0,
    '2020-06-07 04:38:06',
    '2020-06-07 04:38:06'
  ),
  (
    'e93b5a62-5e99-4230-83a6-8a624c3b4dac',
    'f6690034-a876-440f-8449-3353c3ca10a7',
    'SI',
    0,
    '2020-06-07 04:58:55',
    '2020-06-07 04:58:55'
  ),
  (
    'eac78870-38f3-41dd-b866-c87b0a08fa57',
    '81d6f089-43dc-4dab-9329-c9db9a88a395',
    'TÁ',
    0,
    '2020-06-07 06:37:59',
    '2020-06-07 06:37:59'
  ),
  (
    'eaf7a425-c796-49dd-99d0-031260597dbd',
    '81d6f089-43dc-4dab-9329-c9db9a88a395',
    'TÀ',
    0,
    '2020-06-07 06:42:27',
    '2020-06-07 06:42:27'
  ),
  (
    'ec566fb0-c7d3-49df-a9fe-78e74d11f038',
    '125472b4-7034-4b33-ab1a-d711fc08a137',
    'KA',
    0,
    '2020-06-07 04:57:24',
    '2020-06-07 04:57:24'
  ),
  (
    'f218a349-fa75-4f53-a3e0-a621f598d1de',
    'f6690034-a876-440f-8449-3353c3ca10a7',
    'SE',
    0,
    '2020-06-07 04:58:51',
    '2020-06-07 04:58:51'
  ),
  (
    'f732c465-03dd-4b4f-829a-87304c47f062',
    'abb5f16a-f0e7-44d9-a8e7-877d1bca7dcb',
    'TA',
    0,
    '2020-06-07 04:56:34',
    '2020-06-07 04:56:34'
  ),
  (
    'f8114a2f-ad23-4308-8269-d4a7678a2ef6',
    '66758e44-6537-4446-8909-3c2da5212712',
    'HU',
    0,
    '2020-06-07 05:00:28',
    '2020-06-07 05:00:28'
  );
/*!40000 ALTER TABLE `exercise` ENABLE KEYS */
;
UNLOCK TABLES;
--
-- Table structure for table `exercise_type`
--
DROP TABLE IF EXISTS `exercise_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */
;
/*!40101 SET character_set_client = utf8 */
;
CREATE TABLE `exercise_type` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `category_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `name` tinytext NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `exercise_type_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */
;
--
-- Dumping data for table `exercise_type`
--
LOCK TABLES `exercise_type` WRITE;
/*!40000 ALTER TABLE `exercise_type` DISABLE KEYS */
;
INSERT INTO `exercise_type`
VALUES (
    '125472b4-7034-4b33-ab1a-d711fc08a137',
    'c8e48d4c-75dc-4c18-b03a-099d67a38e41',
    'K',
    0,
    0,
    '2020-06-07 04:51:05',
    '2020-06-07 04:51:05'
  ),
  (
    '245501c5-9534-4b69-be94-e2c9c4263d57',
    'e2360b0a-aa6b-4ed1-9a33-0401f661a8b0',
    'M',
    0,
    0,
    '2020-06-07 04:53:15',
    '2020-06-07 04:53:15'
  ),
  (
    '3e3916fc-f72f-40b3-8830-82a60271865a',
    'c8e48d4c-75dc-4c18-b03a-099d67a38e41',
    'CH',
    0,
    0,
    '2020-06-07 04:51:31',
    '2020-06-07 04:51:31'
  ),
  (
    '4adeca72-ffa0-4006-8599-51a8f98e0446',
    'deb10fb4-6277-4a5f-a52e-d4830eebeb47',
    'U',
    0,
    0,
    '2020-06-06 10:58:42',
    '2020-06-06 10:58:42'
  ),
  (
    '66758e44-6537-4446-8909-3c2da5212712',
    'c8e48d4c-75dc-4c18-b03a-099d67a38e41',
    'H',
    0,
    0,
    '2020-06-07 04:51:35',
    '2020-06-07 04:51:35'
  ),
  (
    '67a9640a-3799-436c-b21e-06e0b27b9227',
    'deb10fb4-6277-4a5f-a52e-d4830eebeb47',
    'O',
    0,
    0,
    '2020-06-06 10:58:37',
    '2020-06-06 10:58:37'
  ),
  (
    '81d6f089-43dc-4dab-9329-c9db9a88a395',
    'e2360b0a-aa6b-4ed1-9a33-0401f661a8b0',
    'T',
    0,
    0,
    '2020-06-07 04:53:09',
    '2020-06-07 04:53:09'
  ),
  (
    '850910e4-c17c-4e47-a8e9-7d034014e0b2',
    'deb10fb4-6277-4a5f-a52e-d4830eebeb47',
    'A',
    0,
    0,
    '2020-06-06 10:57:45',
    '2020-06-06 10:57:45'
  ),
  (
    '8e408d1e-9180-4b83-8e02-c7f52e9cef26',
    'c8e48d4c-75dc-4c18-b03a-099d67a38e41',
    'P',
    0,
    0,
    '2020-06-07 04:50:56',
    '2020-06-07 04:50:56'
  ),
  (
    '9ff3838c-a19d-4a9d-afcb-46c4ec16115a',
    'deb10fb4-6277-4a5f-a52e-d4830eebeb47',
    'I',
    0,
    0,
    '2020-06-06 10:58:33',
    '2020-06-06 10:58:33'
  ),
  (
    'a9a84629-aa44-474c-83e2-a5c548d0e82a',
    'deb10fb4-6277-4a5f-a52e-d4830eebeb47',
    'E',
    0,
    0,
    '2020-06-06 10:58:28',
    '2020-06-06 10:58:28'
  ),
  (
    'abb5f16a-f0e7-44d9-a8e7-877d1bca7dcb',
    'c8e48d4c-75dc-4c18-b03a-099d67a38e41',
    'T',
    0,
    0,
    '2020-06-07 04:51:01',
    '2020-06-07 04:51:01'
  ),
  (
    'c341cb2d-ff59-4690-94b8-638d301aa5de',
    'c8e48d4c-75dc-4c18-b03a-099d67a38e41',
    'F',
    0,
    0,
    '2020-06-07 04:51:17',
    '2020-06-07 04:51:17'
  ),
  (
    'f6690034-a876-440f-8449-3353c3ca10a7',
    'c8e48d4c-75dc-4c18-b03a-099d67a38e41',
    'S',
    0,
    0,
    '2020-06-07 04:51:26',
    '2020-06-07 04:51:26'
  );
/*!40000 ALTER TABLE `exercise_type` ENABLE KEYS */
;
UNLOCK TABLES;
--
-- Table structure for table `hospital`
--
DROP TABLE IF EXISTS `hospital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */
;
/*!40101 SET character_set_client = utf8 */
;
CREATE TABLE `hospital` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `name` tinytext,
  `address` tinytext,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */
;
--
-- Dumping data for table `hospital`
--
LOCK TABLES `hospital` WRITE;
/*!40000 ALTER TABLE `hospital` DISABLE KEYS */
;
INSERT INTO `hospital`
VALUES (
    '9aa04dbd-4f17-4de5-a758-d7f184a912f9',
    'Bệnh viện Quân Y',
    'Quận 3, Tp HCM',
    0,
    '2020-07-02 15:04:58',
    '2020-07-02 15:04:58'
  ),
  (
    'd3aeeaa1-8c42-41f8-ab14-a5b797e78e59',
    'Bệnh viện chợ rẫy',
    'Quận 1, Tp HCM',
    0,
    '2020-07-02 14:55:05',
    '2020-07-02 14:55:05'
  );
/*!40000 ALTER TABLE `hospital` ENABLE KEYS */
;
UNLOCK TABLES;
--
-- Table structure for table `medical_record`
--
DROP TABLE IF EXISTS `medical_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */
;
/*!40101 SET character_set_client = utf8 */
;
CREATE TABLE `medical_record` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `patient_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `images` json DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  CONSTRAINT `medical_record_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `user` (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */
;
--
-- Dumping data for table `medical_record`
--
LOCK TABLES `medical_record` WRITE;
/*!40000 ALTER TABLE `medical_record` DISABLE KEYS */
;
/*!40000 ALTER TABLE `medical_record` ENABLE KEYS */
;
UNLOCK TABLES;
--
-- Table structure for table `path_suggestion`
--
DROP TABLE IF EXISTS `path_suggestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */
;
/*!40101 SET character_set_client = utf8 */
;
CREATE TABLE `path_suggestion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patient_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `doctor_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `workout_times_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `workout_times_id` (`workout_times_id`),
  KEY `patient_id` (`patient_id`),
  KEY `doctor_id` (`doctor_id`),
  CONSTRAINT `path_suggestion_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `user` (`id`),
  CONSTRAINT `path_suggestion_ibfk_2` FOREIGN KEY (`doctor_id`) REFERENCES `user` (`id`),
  CONSTRAINT `path_suggestion_ibfk_3` FOREIGN KEY (`workout_times_id`) REFERENCES `workout_times` (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */
;
--
-- Dumping data for table `path_suggestion`
--
LOCK TABLES `path_suggestion` WRITE;
/*!40000 ALTER TABLE `path_suggestion` DISABLE KEYS */
;
/*!40000 ALTER TABLE `path_suggestion` ENABLE KEYS */
;
UNLOCK TABLES;
--
-- Table structure for table `practice`
--
DROP TABLE IF EXISTS `practice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */
;
/*!40101 SET character_set_client = utf8 */
;
CREATE TABLE `practice` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `workout_times_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `exercise_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `score` int(11) NOT NULL,
  `audio_path` varchar(255) NOT NULL,
  `analyzed_data` json NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `workout_times_id` (`workout_times_id`),
  KEY `exercise_id` (`exercise_id`),
  CONSTRAINT `practice_ibfk_1` FOREIGN KEY (`workout_times_id`) REFERENCES `workout_times` (`id`),
  CONSTRAINT `practice_ibfk_2` FOREIGN KEY (`exercise_id`) REFERENCES `exercise` (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */
;
--
-- Dumping data for table `practice`
--
LOCK TABLES `practice` WRITE;
/*!40000 ALTER TABLE `practice` DISABLE KEYS */
;
/*!40000 ALTER TABLE `practice` ENABLE KEYS */
;
UNLOCK TABLES;
--
-- Table structure for table `selecting_path`
--
DROP TABLE IF EXISTS `selecting_path`;
/*!40101 SET @saved_cs_client     = @@character_set_client */
;
/*!40101 SET character_set_client = utf8 */
;
CREATE TABLE `selecting_path` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patient_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `current_path` int(10) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `patient_id` (`patient_id`),
  KEY `current_path` (`current_path`),
  CONSTRAINT `selecting_path_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `user` (`id`),
  CONSTRAINT `selecting_path_ibfk_2` FOREIGN KEY (`current_path`) REFERENCES `path_suggestion` (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */
;
--
-- Dumping data for table `selecting_path`
--
LOCK TABLES `selecting_path` WRITE;
/*!40000 ALTER TABLE `selecting_path` DISABLE KEYS */
;
/*!40000 ALTER TABLE `selecting_path` ENABLE KEYS */
;
UNLOCK TABLES;
--
-- Table structure for table `user`
--
DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */
;
/*!40101 SET character_set_client = utf8 */
;
CREATE TABLE `user` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `full_name` tinytext,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(15) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `role` varchar(10) DEFAULT NULL,
  `is_full_filled` tinyint(1) DEFAULT '0',
  `is_created` tinyint(1) DEFAULT '0',
  `sign_up_method` varchar(10) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */
;
--
-- Dumping data for table `user`
--
LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */
;
INSERT INTO `user`
VALUES (
    '01c5c4b7-356b-4452-bd99-5ee44b8db2e1',
    'Huỳnh Đức Anh',
    '$2b$10$UR7XDoNUuUEkzfWINwVoBOZGHBwXyFha/k/LvR8PBpGQEwIwzbeO.',
    'datn.devteam@gmail.com',
    '+84375905219',
    NULL,
    '2020-06-21 00:00:00',
    0,
    'ADMIN',
    0,
    1,
    'EMAIL',
    '2020-06-18 18:06:37',
    '2020-06-18 18:06:37'
  ),
  (
    'f9e89d82-42de-4975-8275-7b5a6ab3f1e2',
    'Lê Quốc Dũng',
    '$2b$10$UR7XDoNUuUEkzfWINwVoBOZGHBwXyFha/k/LvR8PBpGQEwIwzbeO.',
    'bacsidung@gmail.com',
    '+84375905222',
    NULL,
    '2020-06-21 00:00:00',
    0,
    'DOCTOR',
    0,
    1,
    'EMAIL',
    '2020-06-18 18:06:37',
    '2020-06-18 18:06:37'
  ),
  (
    'f9e89d82-42de-4975-8275-7b5a6ab3f1ff',
    'Đào Thị Như Quỳnh',
    '$2b$10$UR7XDoNUuUEkzfWINwVoBOZGHBwXyFha/k/LvR8PBpGQEwIwzbeO.',
    'bacsiquynh@gmail.com',
    '+84375905211',
    NULL,
    '2020-06-21 00:00:00',
    0,
    'DOCTOR',
    0,
    1,
    'EMAIL',
    '2020-06-18 18:06:37',
    '2020-06-18 18:06:37'
  );
/*!40000 ALTER TABLE `user` ENABLE KEYS */
;
UNLOCK TABLES;
--
-- Table structure for table `workout_times`
--
DROP TABLE IF EXISTS `workout_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */
;
/*!40101 SET character_set_client = utf8 */
;
CREATE TABLE `workout_times` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `patient_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `exercise_type_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `exercise_list` json NOT NULL,
  `score` int(11) DEFAULT NULL,
  `analyzed_data` json DEFAULT NULL,
  `workout_type` varchar(25) DEFAULT 'PRACTICE',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `exercise_type_id` (`exercise_type_id`),
  CONSTRAINT `workout_times_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `user` (`id`),
  CONSTRAINT `workout_times_ibfk_2` FOREIGN KEY (`exercise_type_id`) REFERENCES `exercise_type` (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */
;
--
-- Dumping data for table `workout_times`
--
LOCK TABLES `workout_times` WRITE;
/*!40000 ALTER TABLE `workout_times` DISABLE KEYS */
;
/*!40000 ALTER TABLE `workout_times` ENABLE KEYS */
;
UNLOCK TABLES;
--
-- Table structure for table `workout_times_comments`
--
DROP TABLE IF EXISTS `workout_times_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */
;
/*!40101 SET character_set_client = utf8 */
;
CREATE TABLE `workout_times_comments` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `doctor_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `workout_time_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `doctor_id` (`doctor_id`),
  KEY `workout_time_id` (`workout_time_id`),
  CONSTRAINT `workout_times_comments_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `user` (`id`),
  CONSTRAINT `workout_times_comments_ibfk_2` FOREIGN KEY (`workout_time_id`) REFERENCES `workout_times` (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */
;
--
-- Dumping data for table `workout_times_comments`
--
LOCK TABLES `workout_times_comments` WRITE;
/*!40000 ALTER TABLE `workout_times_comments` DISABLE KEYS */
;
/*!40000 ALTER TABLE `workout_times_comments` ENABLE KEYS */
;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */
;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */
;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */
;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */
;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */
;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */
;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */
;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */
;
-- Dump completed on 2020-07-16 14:13:34