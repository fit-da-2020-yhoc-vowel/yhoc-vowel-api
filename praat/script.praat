form Test commandline calls
        sentence File_name null
endform

Read from file: file_name$
current_sound$ = selected$("Sound")

selectObject: 1
To Pitch... 0.005 70 680
pitchID = selected("Pitch")

selectObject: 1, 2
To PointProcess (cc)

selectObject: 1,2,3
voiceReport$ = Voice report: 0, 0, 75, 500, 1.3, 1.6, 0.03, 0.45
jitter = extractNumber (voiceReport$, "Jitter (local): ")
shimmer = extractNumber (voiceReport$, "Shimmer (local): ")
hnr = extractNumber (voiceReport$, "Mean harmonics-to-noise ratio: ")
appendInfo: "", jitter, ",", shimmer, ",", hnr