const camelcaseKeys = require('camelcase-keys');

module.exports = {
  transformRequest(req, _res, next) {
    req.body = camelcaseKeys(req.body, { deep: true });
    req.query = camelcaseKeys(req.query, { deep: true });
    next();
  },
};
