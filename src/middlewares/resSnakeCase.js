const snakeCase = require('snakecase-keys');
const { InternalServerError } = require('@utils');

function formattedResponseJson(data) {
  return snakeCase(data, { deep: true });
}

module.exports = {
  resSnakeCase: (_req, res, next) => {
    try {
      const originalResJson = res.json;
      res.json = function (obj) {
        let body = JSON.parse(JSON.stringify(obj));
        body = formattedResponseJson(body);
        originalResJson.call(this, body);
      };
      next();
    } catch (e) {
      next(
        new InternalServerError(
          new Error(e),
          'Internal Server Error',
        ),
      );
    }
  },
};
