const passport = require('passport');
const {
  InternalServerError,
  ErrorBase,
  Unauthorized,
  Forbidden,
} = require('@utils');
const { role } = require('@constants');

function authenticate() {
  return (req, res, next) => {
    passport.authenticate(
      'phoneJWT',
      { session: false },
      (error, user, _info) => {
        if (error) {
          if (error instanceof ErrorBase) {
            return next(error);
          }
          return next(
            new InternalServerError(
              new Error(error.message),
              'Internal Server Error',
            ),
          );
        }
        if (!user) {
          return next(
            new Unauthorized(new Error('Token không hợp lệ.')),
          );
        }

        req.user = user;
        return next();
      },
    )(req, res, next);
  };
}

function authorize(roles = []) {
  let authorizeRoles = roles;
  if (typeof roles === 'string') {
    authorizeRoles = [roles];
  }

  return (req, res, next) => {
    let isExisted = false;
    for (let i = 0; i < authorizeRoles.length; i += 1) {
      if (Object.values(role).includes(authorizeRoles[i])) {
        isExisted = true;
        break;
      }
    }

    if (!isExisted) {
      throw new InternalServerError(
        new Error('Internal Server Error'),
      );
    }

    if (
      authorizeRoles.length &&
      !authorizeRoles.includes(req.user.role)
    ) {
      throw new Forbidden(new Error('Bạn không có quyền truy cập.'));
    }

    if (!req.user.isCreated) {
      throw new Forbidden(new Error('Người dùng không tồn tại.'));
    }
    next();
  };
}

module.exports = {
  authenticate,
  authorize,
};
