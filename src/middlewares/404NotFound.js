module.exports = {
  resourceNotFound(req, res) {
    return res.status(404).json({
      url: req.originalUrl,
      message: 'Resource not found!',
    });
  },
};
