const errorMiddleware = require('./errorMiddleware');
const resourceNotFound = require('./404NotFound');
const transformRequest = require('./transformRequest');
const resSnakeCase = require('./resSnakeCase');

module.exports = {
  ...errorMiddleware,
  ...resourceNotFound,
  ...transformRequest,
  ...resSnakeCase,
};
