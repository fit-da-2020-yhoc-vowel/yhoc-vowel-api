const { logger } = require('@config/winston');
const { ErrorBase } = require('@utils');
const httpStatus = require('http-status');

module.exports = {
  errorMiddleware(error, req, res, _next) {
    const retError = error;
    logger.error(
      `${retError.statusCode || httpStatus.INTERNAL_SERVER_ERROR} - ${
        req.method
      } - ${req.originalUrl} - ${req.ip} - ${JSON.stringify(
        retError,
      )} - ${JSON.stringify(req.body || {})}`,
    );
    if (retError instanceof ErrorBase) {
      // Remove debug info from response if env is production.
      if (process.env.NODE_ENV === 'production') {
        delete retError.debug;
      }

      res.setHeader('Content-Type', 'application/problem+json');
      return res.status(retError.statusCode).json(retError);
    }
    return res
      .status(retError.statusCode || httpStatus.INTERNAL_SERVER_ERROR)
      .json({
        type: retError.type || httpStatus['500_NAME'],
        message: 'Internal Server Error',
        statusCode:
          retError.statusCode || httpStatus.INTERNAL_SERVER_ERROR,
      });
  },
};
