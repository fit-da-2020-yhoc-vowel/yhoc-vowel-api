const router = require('express').Router();
const httpStatus = require('http-status');
const cateService = require('@services/categoriesService');
const exerTypesService = require('@services/exerciseTypesService');
const { V, BadRequest } = require('@utils');
const { role: systemRole } = require('@constants');
const { authenticate, authorize } = require('@middlewares/auth');

// get all categories
router.get(
  '/',
  authenticate(),
  authorize([
    systemRole.admin,
    systemRole.doctor,
    systemRole.patient,
  ]),
  async (req, res, next) => {
    try {
      const result = await cateService.getAllCategories();
      return res.status(httpStatus.OK).json({ categories: result });
    } catch (e) {
      return next(e);
    }
  },
);

// get exerciseType in category by ID
router.get(
  '/:id',
  authenticate(),
  authorize([
    systemRole.admin,
    systemRole.doctor,
    systemRole.patient,
  ]),
  async (req, res, next) => {
    const { id } = req.params;
    try {
      const { error } = V.string().guid().validate(id);

      if (error) {
        throw new BadRequest(
          new Error(error),
          'Id không đúng định dạng.',
        );
      }

      const result = await cateService.getCategoryById(id);
      return res.status(httpStatus.OK).json(result);
    } catch (e) {
      return next(e);
    }
  },
);

router.get(
  '/:id/exercise-types',
  authenticate(),
  authorize([
    systemRole.admin,
    systemRole.doctor,
    systemRole.patient,
  ]),
  async (req, res, next) => {
    const { id } = req.params;
    try {
      const { error } = V.string().guid().validate(id);

      if (error) {
        throw new BadRequest(
          new Error(error),
          'Id không đúng định dạng.',
        );
      }

      const result = await exerTypesService.findExerciseTypesByCateID(
        id,
      );
      return res.status(httpStatus.OK).json({
        exerciseTypes: result,
      });
    } catch (e) {
      return next(e);
    }
  },
);

// create a new category
router.post(
  '/',
  authenticate(),
  authorize([systemRole.admin]),
  async (req, res, next) => {
    const { name, thumbnail } = req.body;
    try {
      const result = await cateService.createCategory({
        name,
        thumbnail,
      });
      return res.status(httpStatus.OK).json(result);
    } catch (e) {
      return next(e);
    }
  },
);

// delete a category
router.delete(
  '/:id',
  authenticate(),
  authorize([systemRole.admin]),
  async (req, res, next) => {
    const { id } = req.params;
    const { error } = V.string().guid().validate(id);

    if (error) {
      throw new BadRequest(
        new Error(error),
        'Id không đúng định dạng.',
      );
    }
    try {
      const result = await cateService.deleteCategory(id);
      return res.status(httpStatus.OK).json(result);
    } catch (e) {
      return next(e);
    }
  },
);
module.exports = router;
