const router = require('express').Router();
const { isArray } = require('lodash');
const httpStatus = require('http-status');
const userService = require('@services/userService');
const medicalRecordService = require('@services/medicalRecordService');
const {
  V,
  BadRequest,
  formatDate,
  Forbidden,
  NotExist,
  InternalServerError,
  Pagination,
} = require('@utils');
const {
  role: systemRole,
  gender: genderEnum,
  connectDoctor: ConnectionType,
} = require('@constants');
const { authenticate, authorize } = require('@middlewares/auth');
const uploadImagesService = require('@services/uploadImagesService');
const db = require('@models');
const connectDoctorService = require('../services/connectDoctorService');

router.get(
  '/me',
  authenticate(),
  authorize([
    systemRole.patient,
    systemRole.admin,
    systemRole.doctor,
  ]),
  async (req, res, next) => {
    const { id } = req.user;
    try {
      const result = await userService.getUserInfoById(id);

      return res.status(httpStatus.OK).json({
        ...result,
        birthday: formatDate(result.birthday),
      });
    } catch (e) {
      return next(e);
    }
  },
);

/**
 * body: full_name, birthday, gender
 */
router.put(
  '/update-info',
  authenticate(),
  authorize([
    systemRole.patient,
    systemRole.doctor,
    systemRole.admin,
  ]),
  async (req, res, next) => {
    const {
      fullName,
      birthday,
      gender,
      phone,
      hospitalId = null,
      address = null,
    } = req.body;
    const { id, role: userRole } = req.user;

    try {
      const schema = V.object({
        id: V.string().guid({
          version: ['uuidv4'],
        }),
        phone: V.string().phone(),
        fullName: V.string().fullName(),
        birthday: V.string().birthday(),
        gender: V.number().in(Object.values(genderEnum)),
      });

      const validatedValues = schema.validate({
        id,
        fullName,
        birthday,
        gender,
        phone,
      });

      const { error, value } = validatedValues;

      if (error) {
        const detail = error.details[0];
        throw new BadRequest(
          new Error(detail.message),
          detail.message,
        );
      }

      await db.withTransaction(async () => {
        await userService.updateUserInfo({
          ...value,
          birthday: value.birthday.toDate(),
        });

        if (
          userRole === systemRole.doctor &&
          (hospitalId || address != null)
        ) {
          if (hospitalId) {
            const { error: errId } = V.string()
              .guid({
                version: ['uuidv4'],
              })
              .validate(hospitalId);

            if (errId) {
              throw new BadRequest(
                new Error(errId),
                'Id bệnh viện không đúng định dạng.',
              );
            }
          }
          if (address) {
            const { error: errAddress } = V.string()
              .max(255)
              .validate(address);

            if (errAddress) {
              throw new BadRequest(
                new Error(errAddress),
                'Địa chỉ tối đa 255 ký tự',
              );
            }
          }

          await userService.updateDoctorInfo(id, hospitalId, address);
        }
      });
      const result = await userService.getUserInfoById(id);

      return res.status(httpStatus.OK).json({
        user: {
          ...result,
          birthday: formatDate(result.birthday),
        },
      });
    } catch (err) {
      return next(err);
    }
  },
);

/**
 * body: password, newPassword, confirmNewPassword
 * succeed: {isUpdated: true}
 */
router.put(
  '/update-password',
  authenticate(),
  authorize([
    systemRole.patient,
    systemRole.doctor,
    systemRole.admin,
  ]),
  async (req, res, next) => {
    const { password, newPassword, confirmNewPassword } = req.body;
    const { id } = req.user;
    // verify password
    try {
      const validatedValues = V.string()
        .password()
        .validate(password);
      const { error } = validatedValues;
      if (error) {
        throw new BadRequest(new Error(error.details[0].message));
      }
      if (!(await userService.checkPassword(id, password))) {
        throw new Forbidden(new Error('Mật khẩu không chính xác'));
      }
    } catch (e) {
      return next(e);
    }
    const schema = V.object({
      newPassword: V.string().required().password(),
      confirmNewPassword: V.string()
        .required()
        .confirmPassword(V.ref('newPassword')),
    });
    try {
      const validatedValues1 = schema.validate({
        newPassword,
        confirmNewPassword,
      });

      const { error } = validatedValues1;
      if (error) {
        const detail = error.details[0];
        throw new BadRequest(
          new Error(`Mật khẩu mới: ${detail.message}`),
          `Mật khẩu mới: ${detail.message}`,
        );
      }
      const isUpdated = await userService.changePassword(
        id,
        newPassword,
      );

      if (!isUpdated) {
        throw new InternalServerError(
          new Error('Không thể thay đổi mật khẩu.'),
        );
      }
      return res.status(httpStatus.OK).json({
        isUpdated,
        // password: status,
      });
    } catch (e) {
      return next(e);
    }
  },
);

/**
 * upload file image .png and .jpg
 */

router.put(
  '/upload-avatar',
  authenticate(),
  authorize([
    systemRole.patient,
    systemRole.doctor,
    systemRole.admin,
  ]),
  uploadImagesService.uploadAvatar,
  async (req, res, next) => {
    const { id } = req.user;
    try {
      const result = await userService.setAvatar(
        id,
        `/avatars/${req.file.filename}`,
      );

      return res.status(httpStatus.OK).json({
        avatar: result.avatar,
      });
    } catch (e) {
      return next(e);
    }
  },
);

router.get(
  '/medical-record',
  authenticate(),
  authorize([systemRole.patient]),
  async (req, res, next) => {
    const { id } = req.user;
    try {
      const result = await medicalRecordService.getMedicalRecordByPatientId(
        id,
      );
      return res
        .status(httpStatus.OK)
        .json({ images: result.images || [] });
    } catch (e) {
      return next(e);
    }
  },
);

router.get(
  '/get-medical-record?',
  authenticate(),
  authorize([systemRole.doctor]),
  async (req, res, next) => {
    const { id: doctorId } = req.user;
    const { patientId } = req.query;
    try {
      const { error } = V.string()
        .guid()
        .required()
        .validate(patientId);

      if (error) {
        throw new BadRequest(new Error(error));
      }

      const result = await medicalRecordService.getMedicalRecordByPatientId(
        patientId,
        doctorId,
      );
      return res
        .status(httpStatus.OK)
        .json({ images: result.images || [] });
    } catch (e) {
      return next(e);
    }
  },
);

router.put(
  '/upload-record-images',
  authenticate(),
  authorize([systemRole.patient]),
  uploadImagesService.uploadImagesRecord,
  async (req, res, next) => {
    const { id } = req.user;
    try {
      const images = req.files.map(
        (image) => `/medical-record/${image.filename}`,
      );
      await medicalRecordService.addImages(id, images);
      return res.status(httpStatus.OK).json({ newImages: images });
    } catch (e) {
      return next(e);
    }
  },
);

router.delete(
  '/delete-record-image/:filename',
  authenticate(),
  authorize(systemRole.patient),
  async (req, res, next) => {
    const { id: patientId } = req.user;
    const { filename } = req.params;
    try {
      const { error } = V.string()
        .min(1)
        .required()
        .validate(filename);
      if (error) {
        throw new BadRequest(new Error(error));
      }

      // check user has this image
      const medicalRecord = await medicalRecordService.findMedicalRecordByPatientId(
        patientId,
      );

      const imageUrl = `/medical-record/${filename}`;

      if (
        !medicalRecord ||
        !isArray(medicalRecord.images) ||
        medicalRecord.images.length === 0 ||
        !medicalRecord.images.includes(imageUrl)
      ) {
        throw new NotExist(
          new Error('Không tồn tại ảnh trong hồ sơ.'),
        );
      }

      const result = await medicalRecordService.deleteImage(
        patientId,
        imageUrl,
      );
      return res.status(httpStatus.OK).json(result);
    } catch (e) {
      return next(e);
    }
  },
);

router.get(
  '/all',
  authenticate(),
  authorize(systemRole.admin),
  async (req, res, next) => {
    const { role } = req.query;
    try {
      const {
        error: paginationError,
        limit,
        page,
      } = Pagination.isValid(req.query.limit, req.query.page);

      if (paginationError) {
        throw new BadRequest(new Error(paginationError));
      }

      const { error } = V.string()
        .in(Object.values(systemRole))
        .required()
        .validate(role);

      if (error) {
        throw new BadRequest(new Error(error));
      }

      const result = await userService.getUsers(role, page, limit);

      return res.status(httpStatus.OK).json({
        users: result,
        limit,
        page,
      });
    } catch (e) {
      return next(e);
    }
  },
);

router.get(
  '/count-all',
  authenticate(),
  authorize(systemRole.admin),
  async (req, res, next) => {
    const { role } = req.query;

    try {
      const { error } = V.string()
        .in(Object.values(systemRole))
        .required()
        .validate(role);

      if (error) {
        throw new BadRequest(new Error(error));
      }
      const result = await userService.countUsers(role);

      return res.status(httpStatus.OK).json({
        total: result.total_user,
      });
    } catch (e) {
      return next(e);
    }
  },
);

router.get(
  '/search',
  authenticate(),
  authorize(systemRole.admin),
  async (req, res, next) => {
    const { name, role } = req.query;
    try {
      const {
        error: paginationError,
        limit,
        page,
      } = Pagination.isValid(req.query.limit, req.query.page);

      if (paginationError) {
        throw new BadRequest(new Error(paginationError));
      }

      const schema = V.object({
        name: V.string(),
        role: V.string()
          .in(Object.values(systemRole))
          .error(new Error('Role không đúng.')),
      });

      const { error } = schema.validate({
        name,
        role,
      });

      if (error) {
        throw new BadRequest(new Error(error));
      }

      const [users, total] = await Promise.all([
        userService.searchUserByName(name, role, page, limit),
        userService.countSearchUser(name, role),
      ]);

      return res.status(httpStatus.OK).json({
        users,
        total,
        page,
        limit,
      });
    } catch (e) {
      return next(e);
    }
  },
);

router.get(
  '/dashboard',
  authenticate(),
  authorize([systemRole.doctor, systemRole.admin]),
  async (req, res, next) => {
    const { id: userId, role: userRole } = req.user;
    try {
      // first of all: show number of connected patients, number of pending invitations from patient
      // get list of 5 patients with newest workouttimes
      const [
        numOfConnectedPatients = 0,
        numOfWaitingInvitations = 0,
        workouts,
      ] = await Promise.all([
        connectDoctorService.countConnections({
          userId,
          userRole,
          connectionStatus: ConnectionType.ACCEPTED,
        }),
        connectDoctorService.countConnections({
          userId,
          userRole,
          connectionStatus: ConnectionType.WAITING,
          invitationOrigin: ConnectionType.PATIENT_INVITATION,
        }),
        connectDoctorService.get5LastedPatientWorkouts(userId),
      ]);

      return res.status(httpStatus.OK).json({
        numOfConnectedPatients,
        numOfWaitingInvitations,
        workouts,
      });
    } catch (e) {
      return next(e);
    }
  },
);

module.exports = router;
