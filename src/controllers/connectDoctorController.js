const router = require('express').Router();
const httpStatus = require('http-status');
const connDoctorService = require('@services/connectDoctorService');
const userService = require('@services/userService');
const { V, BadRequest, NotExist, Forbidden } = require('@utils');
const { role: systemRole } = require('@constants');
const { authenticate, authorize } = require('@middlewares/auth');
const { connectDoctor } = require('@constants');

// issue: name = null -> search all doctor -> for test
router.get(
  '/search-doctor?',
  authenticate(),
  authorize([systemRole.patient]),
  async (req, res, next) => {
    const { id } = req.user;
    const { name } = req.query;
    try {
      if (name) {
        const { error: errName } = V.string()
          .searchText()
          .validate(name);
        if (errName) {
          throw new BadRequest(new Error(errName.details[0].message));
        }
      }
      const result = await connDoctorService.searchDoctorByName(
        id,
        name,
      );

      return res.status(httpStatus.OK).json({
        connections: result,
      });
    } catch (e) {
      return next(e);
    }
  },
);

router.get(
  '/search-patient?',
  authenticate(),
  authorize([systemRole.doctor]),
  async (req, res, next) => {
    const { id: doctorId } = req.user;
    const { name } = req.query;
    const page = req.query.page || 1;
    const limit = req.query.limit || 20;

    try {
      const schema = V.object({
        page: V.number().integer().min(1).required(),
        limit: V.number().integer().min(5).required(),
      });

      const { error, value } = schema.validate({
        page,
        limit,
      });
      if (error) {
        throw new BadRequest(new Error(error.details[0].message));
      }
      if (name) {
        const { error: errName } = V.string()
          .searchText()
          .validate(name);
        if (errName) {
          throw new BadRequest(new Error(errName.details[0].message));
        }
      }
      const result = await connDoctorService.searchPatientByName(
        doctorId,
        name,
        value.page,
        value.limit,
      );

      return res.status(httpStatus.OK).json({
        connections: result,
      });
    } catch (e) {
      return next(e);
    }
  },
);

router.get(
  '/get-connections?',
  authenticate(),
  authorize([systemRole.patient, systemRole.doctor]),
  async (req, res, next) => {
    const { id, role } = req.user;
    const { status, invitationOrigin } = req.query;
    try {
      const result = await connDoctorService.getConnectionsByUserId(
        id,
        role,
        status,
        invitationOrigin,
      );
      return res.status(httpStatus.OK).json({
        connections: result,
      });
    } catch (e) {
      return next(e);
    }
  },
);

// send request
router.put(
  '/invite',
  authenticate(),
  authorize([systemRole.patient, systemRole.doctor]),
  async (req, res, next) => {
    const sender = req.user;
    const { targetId } = req.body;
    let target;
    let result;
    try {
      const { error } = V.string()
        .guid()
        .required()
        .validate(targetId);

      if (error) {
        throw new BadRequest(
          new Error(error),
          'Id không đúng định dạng.',
        );
      }
      target = await userService.getUserById(targetId);
      if (!target) {
        throw new NotExist(
          new Error(error),
          'Người được mời không tồn tại',
        );
      }
    } catch (error) {
      return next(error);
    }

    try {
      if (sender.role === systemRole.patient) {
        if (target.role !== systemRole.doctor) {
          throw new Forbidden(
            new Error('Người được mời phải là bác sĩ'),
            'Người được mời phải là bác sĩ',
          );
        }
        result = await connDoctorService.sendRequestConnection(
          sender.id,
          targetId,
          connectDoctor.PATIENT_INVITATION,
        );
      } else {
        if (target.role !== systemRole.patient) {
          throw new Forbidden(
            new Error('Người được mời phải là bệnh nhân'),
            'Người được mời phải là bệnh nhân',
          );
        }
        result = await connDoctorService.sendRequestConnection(
          targetId,
          sender.id,
          connectDoctor.DOCTOR_INVITATION,
        );
      }
      return res.status(httpStatus.OK).json({ connection: result });
    } catch (e) {
      return next(e);
    }
  },
);

// accept doctor/patient's request by request id
router.put(
  '/accept',
  authenticate(),
  authorize([systemRole.patient, systemRole.doctor]),
  async (req, res, next) => {
    const { connectionId } = req.body;
    const { user } = req;
    try {
      const { error } = V.string()
        .guid()
        .required()
        .validate(connectionId);

      if (error) {
        throw new BadRequest(
          new Error(error),
          'Id không đúng định dạng.',
        );
      }

      const result = await connDoctorService.acceptConnection(
        connectionId,
        user.id,
        user.role,
      );
      return res.status(httpStatus.OK).json({ connection: result });
    } catch (e) {
      return next(e);
    }
  },
);

// delete connection between doctor and patient
router.put(
  '/delete',
  authenticate(),
  authorize([systemRole.patient, systemRole.doctor]),
  async (req, res, next) => {
    const { connectionId } = req.body;
    const userId = req.user.id;
    try {
      const { error } = V.string()
        .guid()
        .required()
        .validate(connectionId);

      if (error) {
        throw new BadRequest(
          new Error(error),
          'Id không đúng định dạng.',
        );
      }

      const result = await connDoctorService.deleteConnection(
        connectionId,
        userId,
      );
      return res.status(httpStatus.OK).json({ connection: result });
    } catch (e) {
      return next(e);
    }
  },
);

// ------------------------------------API TEST -------------------------------------
// send request
router.put('/invite-patient-test', async (req, res, next) => {
  const { doctorId, patientId } = req.body;
  try {
    const { error } = V.string().guid().validate(patientId);

    if (error) {
      throw new BadRequest(
        new Error(error),
        'Id không đúng định dạng.',
      );
    }
    const result = await connDoctorService.sendRequestConnection(
      patientId,
      doctorId,
      connectDoctor.DOCTOR_INVITATION,
    );
    return res.status(httpStatus.OK).json({ connection: result });
  } catch (e) {
    return next(e);
  }
});

router.put('/doctor-accept-test', async (req, res, next) => {
  const { connectionId, doctorId } = req.body;
  try {
    const { error } = V.string().guid().validate(connectionId);

    if (error) {
      throw new BadRequest(
        new Error(error),
        'Id không đúng định dạng.',
      );
    }

    const result = await connDoctorService.acceptConnection(
      connectionId,
      doctorId,
      systemRole.doctor,
    );
    return res.status(httpStatus.OK).json({ connection: result });
  } catch (e) {
    return next(e);
  }
});

module.exports = router;
