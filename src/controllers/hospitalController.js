const router = require('express').Router();
const adminService = require('@services/adminService');
const { authenticate, authorize } = require('@middlewares/auth');
const httpStatus = require('http-status');
const { role: systemRole } = require('@constants');

router.post(
  '/create',
  authenticate(),
  authorize([systemRole.admin]),
  async (req, res, next) => {
    const { name, address } = req.body;
    try {
      const result = await adminService.createNewHospital(
        name,
        address,
      );
      return res.status(httpStatus.OK).json({
        result,
      });
    } catch (e) {
      return next(e);
    }
  },
);

router.get(
  '/',
  authenticate(),
  authorize([systemRole.admin, systemRole.doctor]),
  async (req, res, next) => {
    try {
      const result = await adminService.getAllHospitals();
      return res.status(httpStatus.OK).json({
        hospitals: result,
      });
    } catch (e) {
      return next(e);
    }
  },
);

module.exports = router;
