const router = require('express').Router();
const adminService = require('@services/adminService');
const { authenticate, authorize } = require('@middlewares/auth');
const httpStatus = require('http-status');
const {
  role: systemRole,
  connectDoctor: systemConnection,
  workoutType: systemWorkoutType,
} = require('@constants');
const {
  V,
  BadRequest,
  Forbidden,
  InternalServerError,
} = require('@utils');
const connectDoctorService = require('@services/connectDoctorService');
const workoutTimesService = require('@services/workoutTimesService');
const practiceAppreciationService = require('@services/practiceAppreciationService');
const practiceService = require('@services/practiceService');
const commentService = require('../services/commentService');

router.post(
  '/create',
  authenticate(),
  authorize([systemRole.admin]),
  async (req, res, next) => {
    const { email, fullName, phone, password, hospitalId } = req.body;
    try {
      const schema = V.object({
        email: V.string().email().required(),
        fullName: V.string().fullName(),
        password: V.string().password().required(),
        phone: V.string().phone().required(),
        hospitalId: V.string().guid({
          version: ['uuidv4'],
        }),
      });

      const validatedValues = schema.validate({
        email,
        fullName,
        phone,
        password,
        hospitalId,
      });

      const { error, value } = validatedValues;
      if (error) {
        const detail = error.details[0];
        throw new BadRequest(
          new Error(detail.message),
          detail.message,
        );
      }
      const result = await adminService.createAccountDoctor(
        value.email,
        value.fullName,
        value.phone.number,
        value.password,
        value.hospitalId,
      );
      return res.status(httpStatus.OK).json({
        user: {
          id: result.user.id,
          fullName: result.user.fullName,
          phone: result.user.phone,
          email: result.user.email,
          hospitalId: result.user.hospitalId,
        },
      });
    } catch (err) {
      return next(err);
    }
  },
);

router.get(
  '/patient-profile',
  authenticate(),
  authorize(systemRole.doctor),
  async (req, res, next) => {
    const { patientId, limit = 10, page = 1 } = req.query;
    const { id: doctorId } = req.user;

    try {
      const schema = V.object({
        patientId: V.string().guid().required(),
        limit: V.number().min(5).required(),
        page: V.number().min(1).required(),
      });

      const { error } = schema.validate({
        patientId,
        limit,
        page,
      });

      if (error) {
        throw new BadRequest(
          new Error(error),
          'Id không đúng định dạng.',
        );
      }

      // patient profile, patient medical record, practice histories with 10 records
      const patient = await connectDoctorService.getPatientProfile({
        doctorId,
        patientId,
      });

      if (!patient) {
        return res.status(httpStatus.OK).json({
          patient: null,
        });
      }

      const [workoutTimesList, totalWorkout] = await Promise.all([
        workoutTimesService.getAllWorkout({
          patientId,
          limit,
          page,
        }),
        workoutTimesService.countWorkout({
          patientId,
          isFinished: false,
        }),
      ]);

      const appreciatedWorkouts = (workoutTimesList || []).map(
        (wk) => {
          const common = {
            id: wk.id,
            patientId: wk.patientId,
            score: wk.score,
            workoutType: wk.workoutType,
            exerciseList: wk.exerciseList,
            analyzedData: wk.analyzedData,
            createdAt: wk.createdAt,
            updatedAt: wk.updatedAt,
            ...practiceAppreciationService.appreciateResult(wk.score),
          };

          switch (wk.workoutType) {
            case systemWorkoutType.PRACTICE:
              return {
                ...common,
                exerciseTypeId: wk.exerciseTypeId,
                exerciseTypeName: wk.exerciseTypeName,
              };
            case systemWorkoutType.CHECKING:
              return common;
            case systemWorkoutType.PATHS:
              return {
                ...common,
                doctorId: wk.doctorId,
                doctorName: wk.doctorName,
              };
            default:
              throw new InternalServerError(
                new Error(
                  'This workout type is not contain in system',
                ),
              );
          }
        },
      );

      return res.status(httpStatus.OK).json({
        patient: {
          id: patient.id,
          fullName: patient.fullName,
          phone: patient.phone,
          avatar: patient.avatar,
          birthday: patient.birthday,
          gender: patient.gender,
          medicalRecord: patient.medicalRecord,
        },
        histories: appreciatedWorkouts,
        limit,
        page,
        total: totalWorkout,
      });
    } catch (e) {
      return next(e);
    }
  },
);

router.get(
  '/patient-profile/histories',
  authenticate(),
  authorize(systemRole.doctor),
  async (req, res, next) => {
    const { workoutTimesId } = req.query;
    const { id: doctorId } = req.user;

    try {
      const { error } = V.string()
        .guid()
        .required()
        .validate(workoutTimesId);

      if (error) {
        throw new BadRequest(
          new Error(error),
          'Id không đúng định dạng.',
        );
      }

      // find this workout
      // get patientId and check connection between doctor and patient -> ACCEPTED
      // get practices by workoutTimesId
      // get previous comments
      const workoutTimes = await workoutTimesService.findWorkoutTimesById(
        workoutTimesId,
      );

      if (!workoutTimes) {
        throw new Forbidden(
          new Error('Bệnh nhân không có lần tập này.'),
        );
      }

      // if (!workoutTimes.score) {
      //   return res.sendStatus(httpStatus.NO_CONTENT);
      // }

      const { patientId } = workoutTimes;

      const connection = await connectDoctorService.getSingleConnection(
        {
          doctorId,
          patientId,
        },
      );

      if (
        !(
          connection &&
          connection.status === systemConnection.ACCEPTED
        )
      ) {
        throw new Forbidden(
          new Error('Bạn chưa liên kết với bệnh nhân này.'),
        );
      }

      const [practices, comments] = await Promise.all([
        practiceService.getPracticesInWorkoutTimes(workoutTimesId),
        commentService.getCommentByWorkoutTimeId(workoutTimesId),
      ]);

      const appreciatedPractices = (practices || []).map((ptc) => ({
        ...ptc,
        ...practiceAppreciationService.appreciateResult(ptc.score),
      }));

      return res.status(httpStatus.OK).json({
        workoutTimesId,
        patientId,
        practices: appreciatedPractices,
        comments,
      });
    } catch (e) {
      return next(e);
    }
  },
);

module.exports = router;
