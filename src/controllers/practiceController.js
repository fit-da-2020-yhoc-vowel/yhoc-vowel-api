const { isArray } = require('lodash');
const router = require('express').Router();
const httpStatus = require('http-status');
const moment = require('moment');
const { authenticate, authorize } = require('@middlewares/auth');
const { transformRequest } = require('@middlewares');
const uploadService = require('@services/uploadService');
const {
  role: UserRole,
  workoutType: systemWorkoutType,
  connectDoctor,
  MAX_CHECKING_WORKOUT_TIMES,
  MINIMIZE_PATH_PRACTICE,
} = require('@constants');
const { V } = require('@utils');
const {
  BadRequest,
  InternalServerError,
  Forbidden,
} = require('@utils');
const db = require('@models');
const practiceService = require('@services/practiceService');
const redisService = require('@services/redisService');
const exerciseService = require('@services/exercisesService');
const workoutTimesService = require('@services/workoutTimesService');
const practiceAppreciationService = require('@services/practiceAppreciationService');
const connectDoctorService = require('@services/connectDoctorService');
const workoutType = require('../constants/workoutType');
const pathSuggestionService = require('../services/pathSuggestionService');
const selectingPathService = require('../services/selectingPathService');

// flow:
// kiểm tra user có lần tập trong redis hay không? "practice_times_`user_id`": { workout_times_id, created_at }
// nếu có và bài tập hiện tại không bằng exercise_type_id -> remove workout_times -> tạo mới workout_id và exercise_type_id.
/**
 * exerciseId: exercise_id, audio
 * note: chặn user send bài tập lại trong 1 lần tập. tức là: workout chỉ có đúng số lượng bài tập của exercise_type_id
 *
 * structure of redis: {workoutTimesId, createdAt, exercises, done}
 */

router.post(
  '/',
  authenticate(),
  authorize([UserRole.patient]),
  uploadService.uploadSingleAudio,
  transformRequest,
  async (req, res, next) => {
    // TODO: should delete file if error ocurred
    // console.log(req.file);
    // console.log(req.body);
    try {
      const { id: patientId } = req.user;
      const {
        exerciseId,
        workoutTimesType = workoutType.PRACTICE,
      } = req.body;

      const schema = V.object({
        exerciseId: V.string().guid().required(),
        workoutTimesType: V.string()
          .in(Object.values(workoutType))
          .required(),
      });

      const { error } = schema.validate({
        exerciseId,
        workoutTimesType,
      });

      if (error) {
        throw new BadRequest(new Error(error.details[0].message));
      }

      if (!req.file) {
        throw new BadRequest(new Error('Không có file thu âm.'));
      }

      const [redisWorkoutTimes, exercise] = await Promise.all([
        redisService.findWorkoutTimes(patientId, workoutTimesType),
        exerciseService.findExerciseById(exerciseId),
      ]);
      if (!exercise) {
        throw new BadRequest(new Error('Không tồn tại bài tập.'));
      }
      let workoutTimesId = null;
      let returnObject = {};
      await db.withTransaction(async () => {
        if (
          !redisWorkoutTimes ||
          !redisWorkoutTimes.exerciseList.includes(exerciseId) ||
          redisWorkoutTimes.exerciseList.length ===
            redisWorkoutTimes.done.length
        ) {
          // create new workoutTimes -> create new practice

          let newWorkoutTimes = null;
          switch (workoutTimesType) {
            case systemWorkoutType.PRACTICE:
              newWorkoutTimes = await workoutTimesService.createPracticeWorkoutTimes(
                patientId,
                exercise.exerciseTypeId,
              );
              workoutTimesId = newWorkoutTimes.id;
              break;
            case systemWorkoutType.CHECKING:
              if (!redisWorkoutTimes) {
                throw new BadRequest(
                  new Error('Không tồn tại dữ liệu lần tập.'),
                );
              }
              if (
                redisWorkoutTimes &&
                !redisWorkoutTimes.exerciseList.includes(exerciseId)
              ) {
                throw new BadRequest(
                  new Error(
                    'Bài tập này không tồn tại trong danh sách bài kiểm tra.',
                  ),
                );
              }
              throw new InternalServerError(
                new Error('Không có lần kiểm tra.'),
              );
            case systemWorkoutType.PATHS:
              if (!redisWorkoutTimes) {
                throw new BadRequest(
                  new Error('Không tồn tại dữ liệu lộ trình.'),
                );
              }
              workoutTimesId = redisWorkoutTimes.id;
              if (
                redisWorkoutTimes &&
                !redisWorkoutTimes.exerciseList.includes(exerciseId)
              ) {
                throw new BadRequest(
                  new Error(
                    'Bài tập này không tồn tại trong danh sách bài tập của lộ trình.',
                  ),
                );
              }

              // throw new InternalServerError(
              //   new Error('Không tồn tại lộ trình này.'),
              // );
              break;
            default:
              throw new InternalServerError(
                new Error('Internal Server Error'),
              );
          }
        } else {
          workoutTimesId = redisWorkoutTimes.id;
        }

        // if this is a CHECKING or PRACTICE
        // allow repeat last done practice, but not before

        const { done: doneIds } = redisWorkoutTimes || {};

        switch (workoutTimesType) {
          case systemWorkoutType.PRACTICE:
          case systemWorkoutType.CHECKING:
            if (
              doneIds &&
              doneIds.length > 0 &&
              doneIds.includes(exerciseId) &&
              doneIds[doneIds.length - 1] !== exerciseId
            ) {
              throw new Forbidden(
                new Error('Bạn chỉ được tập lại bài tập hiện tại.'),
              );
            }
            break;
          case systemWorkoutType.PATHS:
            break;
          default:
        }

        const practice = await practiceService.createPractice(
          workoutTimesId,
          exerciseId,
          req.file,
        );

        // set exercise is done to redis

        const currentRedisWorkoutTimes = await redisService.findWorkoutTimes(
          patientId,
          workoutTimesType,
        );

        const newDoneExercises = !currentRedisWorkoutTimes.done.includes(
          practice.exerciseId,
        )
          ? [...currentRedisWorkoutTimes.done, practice.exerciseId]
          : currentRedisWorkoutTimes.done;

        const newRedisWorkoutTimes = await redisService.createWorkoutTimes(
          patientId,
          {
            ...currentRedisWorkoutTimes,
            done: newDoneExercises,
          },
          workoutTimesType,
        );

        const isDone =
          newDoneExercises.length ===
          newRedisWorkoutTimes.exerciseList.length;

        returnObject = {
          practice: {
            ...practice.toJSON(),
            ...practiceAppreciationService.appreciateResult(
              practice.score,
            ),
          },
          isFinished: false,
          current: newRedisWorkoutTimes.done.length,
          totalExercise: newRedisWorkoutTimes.exerciseList.length,
        };

        if (isDone) {
          // calculate total score and update in WorkoutTimes
          const finalWorkoutTimes = await workoutTimesService.updateWorkoutTimesScore(
            workoutTimesId,
          );
          returnObject = {
            ...returnObject,
            workoutTimes: {
              ...finalWorkoutTimes.toJSON(),
              ...practiceAppreciationService.appreciateResult(
                finalWorkoutTimes.score,
              ),
            },
            isFinished: true,
          };
        } else {
          // find nextPractice
          let nextExerciseId = null;
          if (workoutTimesType === systemWorkoutType.PATHS) {
            const currentExIdx = newRedisWorkoutTimes.exerciseList.findIndex(
              (exId) => exerciseId === exId,
            );

            if (
              currentExIdx + 1 <=
              newRedisWorkoutTimes.exerciseList.length - 1
            ) {
              nextExerciseId =
                newRedisWorkoutTimes.exerciseList[currentExIdx + 1];
            }
          } else {
            const practicingExercises = (
              newRedisWorkoutTimes.exerciseList || []
            ).filter(
              (exId) =>
                !(newRedisWorkoutTimes.done || []).includes(exId),
            );
            if (practicingExercises && practicingExercises.length) {
              [nextExerciseId] = practicingExercises;
            }
          }

          let nextExercise = null;
          if (nextExerciseId) {
            nextExercise = await exerciseService.findExerciseById(
              nextExerciseId,
            );
          }

          returnObject = { ...returnObject, nextExercise };
        }
        // nếu bài tập này là bài cuối cùng trong list -> tính toán kết quả và thông báo.
      });
      return res.status(httpStatus.CREATED).json(returnObject);
    } catch (e) {
      return next(e);
    }
  },
);

/**
 * Flow: nếu không có trong redis -> tạo mới 1 checking_workout_times trong db & redis.
 * nếu có thì trả về
 * structure of checking_workout_times: {workoutTimesId, exercises, done, createdAt}
 */
router.put(
  '/new-checking-times',
  authenticate(),
  authorize([UserRole.patient]),
  transformRequest,
  async (req, res, next) => {
    const { id: patientId } = req.user;

    try {
      // check: trong ngày hôm nay đã kiểm tra bao nhiêu lần.
      // nếu >= MAX_CHECKING thì không cho tập nữa.

      const workoutTimesListInDate = await workoutTimesService.findWorkoutTimesInDate(
        {
          patientId,
          date: new Date(),
          workoutType: systemWorkoutType.CHECKING,
        },
      );

      if (
        workoutTimesListInDate &&
        workoutTimesListInDate.length >= MAX_CHECKING_WORKOUT_TIMES
      ) {
        return res.status(httpStatus.OK).json({
          currentTimes: workoutTimesListInDate.length,
          totalTimes: MAX_CHECKING_WORKOUT_TIMES,
        });
      }

      const redisWorkoutTimes = await redisService.findWorkoutTimes(
        patientId,
        systemWorkoutType.CHECKING,
      );

      // trả về bài tập tiếp theo.
      // nếu vẫn đang tập (chưa tập xong các bài tập.)
      if (
        redisWorkoutTimes &&
        moment(redisWorkoutTimes.created_at).isSame(
          new Date(),
          'day',
        ) &&
        redisWorkoutTimes.done.length <
          redisWorkoutTimes.exerciseList.length
      ) {
        // tim bai tap tiep theo.
        const practicingExercises = (
          redisWorkoutTimes.exerciseList || []
        ).filter(
          (exerciseId) =>
            !(redisWorkoutTimes.done || []).includes(exerciseId),
        );
        let nextExercise = null;
        if (practicingExercises && practicingExercises.length) {
          nextExercise = await exerciseService.findExerciseById(
            practicingExercises[0],
          );
        }

        return res.status(httpStatus.OK).json({
          ...redisWorkoutTimes,
          nextExercise,
          currentTimes: workoutTimesListInDate.length + 1,
          totalTimes: MAX_CHECKING_WORKOUT_TIMES,
        });
      }

      // nếu đã tập xong hoặc chưa có checking_workout_times trong redis.
      // tạo workout mới, save vào redis
      const checkingWorkoutTimes = await workoutTimesService.createCheckingWorkoutTimes(
        patientId,
      );

      // lay bai tap tiep theo
      let nextExercise = null;
      if (
        checkingWorkoutTimes.exerciseList &&
        checkingWorkoutTimes.exerciseList.length
      ) {
        nextExercise = await exerciseService.findExerciseById(
          checkingWorkoutTimes.exerciseList[0],
        );
      }
      return res.status(httpStatus.OK).json({
        ...checkingWorkoutTimes.toJSON(),
        nextExercise,
        done: [],
        currentTimes: workoutTimesListInDate.length + 1,
        totalTimes: MAX_CHECKING_WORKOUT_TIMES,
      });
    } catch (e) {
      return next(e);
    }
  },
);

router.put(
  '/new-paths',
  authenticate(),
  authorize(UserRole.doctor),
  async (req, res, next) => {
    const { id: doctorId } = req.user;
    const { patientId, exerciseList } = req.body;

    try {
      // validate
      const schema = V.object({
        patientId: V.string().guid().required(),
        exerciseList: V.array()
          .unique((a, b) => a === b)
          .items(V.string().guid()),
      });

      const { error } = schema.validate({
        patientId,
        exerciseList,
      });

      if (error) {
        throw new BadRequest(
          new Error(error),
          'Payload không đúng định dạng.',
        );
      }

      // check all of exercises are existed
      // check: doctor must be followed by patient
      const [exercises, connection] = await Promise.all([
        exerciseService.findExercisesWithListId(exerciseList),
        connectDoctorService.getSingleConnection({
          doctorId,
          patientId,
        }),
      ]);

      if (
        !(exerciseList || []).every(
          (ex) =>
            (exercises || []).findIndex((val) => val.id === ex) !==
            -1,
        )
      ) {
        throw new BadRequest(
          new Error('Có id bài tập không tồn tại.'),
        );
      }

      if (exerciseList.length < MINIMIZE_PATH_PRACTICE) {
        throw new BadRequest(
          new Error(
            `Số lượng bài tập trong lộ trình phải lớn hơn ${MINIMIZE_PATH_PRACTICE}`,
          ),
        );
      }

      if (
        !(connection && connection.status === connectDoctor.ACCEPTED)
      ) {
        throw new BadRequest(
          new Error('Bạn chưa kết nối với bệnh nhân này.'),
        );
      }

      // check if doctor has already suggested and patient has not done yet.
      // nếu bệnh nhân chưa tập bao giờ -> cho phép bác sĩ sửa lại lộ trình tập.
      // nếu bệnh nhân đang tập dỡ tập bài tập của bác sĩ -> không cho bác sĩ set lại.
      // nếu bệnh nhân đã tập xong -> bác sĩ tạo mới 1 lộ trình.

      // find Path of this doctor with patient
      const [paths, selectingPath] = await Promise.all([
        pathSuggestionService.findPathSuggestion({
          patientId,
          doctorId,
        }),
        selectingPathService.getSelectingPathOfPatient(patientId),
      ]);

      if (
        paths &&
        isArray(paths) &&
        paths.some((p) => p.score === null)
      ) {
        throw new Forbidden(
          new Error(
            'Bệnh nhân luyện tập lộ trình của bạn chưa xong.',
          ),
        );
      }

      if (!selectingPath) {
        throw new InternalServerError(
          new Error('Missing SelectingPath'),
        );
      }

      let newPath = null;
      await db.withTransaction(async () => {
        // create new workout with type is PATHS
        const newPathWorkout = await workoutTimesService.createPathWorkoutTimes(
          {
            patientId,
            exerciseList,
          },
        );

        if (!newPathWorkout) {
          throw new InternalServerError(
            new Error('không thể tạo lần tập lúc này.'),
          );
        }

        // create new path
        newPath = await pathSuggestionService.createNewPathSuggestion(
          {
            patientId,
            doctorId,
            workoutTimesId: newPathWorkout.id,
          },
        );

        if (selectingPath.doctorId === doctorId) {
          Promise.resolve(
            selectingPathService.updateCurrentPath(
              patientId,
              newPath.id,
            ),
          );
          Promise.resolve(
            redisService.createWorkoutTimes(
              patientId,
              {
                ...newPathWorkout.toJSON(),
                done: [],
              },
              workoutType.PATHS,
              0,
            ),
          );
        }
      });
      return res.status(httpStatus.CREATED).json(newPath);
    } catch (e) {
      return next(e);
    }
  },
);

router.put(
  '/select-path',
  authenticate(),
  authorize(UserRole.patient),
  async (req, res, next) => {
    const { doctorId } = req.query;
    const { id: patientId } = req.user;

    try {
      const { error } = V.string()
        .guid()
        .required()
        .validate(doctorId);

      if (error) {
        throw new BadRequest(
          new Error(error),
          'Id không đúng định dạng.',
        );
      }

      const [connection, notCompletedPaths] = await Promise.all([
        connectDoctorService.getSingleConnection({
          doctorId,
          patientId,
        }),
        pathSuggestionService.findPathsOfPatient(patientId, false),
      ]);

      if (
        !connection ||
        connection.status !== connectDoctor.ACCEPTED
      ) {
        throw new Forbidden(
          new Error('Bạn chưa có kết nối với bác sĩ này.'),
        );
      }

      if (
        !notCompletedPaths ||
        (isArray(notCompletedPaths) && notCompletedPaths.length === 0)
      ) {
        throw new Forbidden(
          new Error('Các lộ trình của bạn đã được hoàn thành.'),
        );
      }
      let pathIndex = -1;
      for (let i = 0; i < notCompletedPaths.length; i += 1) {
        if (notCompletedPaths[i].doctorId === doctorId) {
          pathIndex = i;
          break;
        }
      }
      if (pathIndex === -1) {
        throw new Forbidden(
          new Error('Bạn không có lộ trình do bác sĩ này đề xuất.'),
        );
      }

      // get current path, if pathId === currentPath -> throw exception
      const selectingPath = await selectingPathService.getSelectingPathOfPatient(
        patientId,
      );

      if (!selectingPath) {
        throw new InternalServerError(
          new Error("Not found patient's SelectingPath"),
        );
      }

      if (selectingPath.id === notCompletedPaths[pathIndex].pathId) {
        throw new BadRequest(
          new Error('Đây là lộ trình hiện tại của bạn.'),
        );
      }

      // update selecting path
      // get list of practices
      const updatedSelectingPath = await selectingPathService.updateCurrentPath(
        patientId,
        notCompletedPaths[pathIndex].pathId,
      );
      const [workoutTimes, practices] = await Promise.all([
        workoutTimesService.getCurrentWorkoutPath(patientId),
        practiceService.getPracticesInWorkoutTimes(
          updatedSelectingPath.workoutTimesId,
        ),
      ]);
      // get done pracices
      const done = (practices || []).map((p) => p.id);
      // update redis.
      const redisPayload = {
        ...workoutTimes,
        done,
      };
      await redisService.createWorkoutTimes(
        patientId,
        redisPayload,
        workoutType.PATHS,
        0,
      );

      return res.status(httpStatus.OK).json(updatedSelectingPath);
    } catch (e) {
      return next(e);
    }
  },
);

router.get(
  '/paths',
  authenticate(),
  authorize(UserRole.patient),
  async (req, res, next) => {
    const { id: patientId } = req.user;

    try {
      let paths = await pathSuggestionService.findPathsOfPatient(
        patientId,
        false,
      );

      paths = (paths || []).map((p) => ({
        pathId: p.pathId,
        doctorId: p.doctorId,
        doctorName: p.doctorName,
        workoutTimesId: p.id,
        isSelecting: p.isSelecting,
      }));

      return res.status(httpStatus.OK).json({
        paths,
      });
    } catch (e) {
      return next(e);
    }
  },
);

router.get(
  '/paths/current',
  authenticate(),
  authorize(UserRole.patient),
  async (req, res, next) => {
    const { id: patientId } = req.user;
    try {
      const { workoutTimes, doctor } =
        (await pathSuggestionService.getSelectingPathWithDoctor(
          patientId,
        )) || {};

      if (!workoutTimes || !doctor) {
        return res.sendStatus(httpStatus.NO_CONTENT);
      }

      const exerciseList = await exerciseService.findExercisesWithListId(
        workoutTimes.exerciseList || [],
      );

      // find completed practices in this workout
      const redisWorkoutTimes = await redisService.findWorkoutTimes(
        patientId,
        workoutType.PATHS,
      );

      if (!redisWorkoutTimes) {
        throw new InternalServerError(
          new Error("Can't get workout from redis."),
          'Internal Server Error',
        );
      }

      if (redisWorkoutTimes.id !== workoutTimes.id) {
        throw new InternalServerError(
          new Error(
            'Lộ trình không thể làm lúc này. Vui lòng chọn lộ trình khác.',
          ),
        );
      }

      const { done } = redisWorkoutTimes;

      const exercisesMap = {};
      Array.from(exerciseList).forEach((el) => {
        exercisesMap[el.id] = el;
      });

      const practices = redisWorkoutTimes.exerciseList.map(
        (id) => exercisesMap[id],
      );

      const nextExercise = (practices || []).find(
        (ex) => !(done || []).includes(ex.id),
      );

      return res.status(httpStatus.OK).json({
        practices,
        done,
        nextExercise,
        doctor: {
          id: doctor.id,
          fullName: doctor.full_name,
          avatar: doctor.avatar,
          gender: doctor.gender,
          hospitalName: doctor.hospital_name,
        },
      });
    } catch (e) {
      return next(e);
    }
  },
);

module.exports = router;
