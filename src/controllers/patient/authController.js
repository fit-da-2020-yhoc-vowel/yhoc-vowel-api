const router = require('express').Router();
const httpStatus = require('http-status');
const authService = require('@services/authService');
const userService = require('@services/userService');
const twilioService = require('@services/twilioService');
const {
  BadRequest,
  Forbidden,
  InternalServerError,
  Token,
} = require('@utils');
const { V } = require('@utils');
const {
  gender: genderEnum,
  role: systemRole,
} = require('@constants');
const { authenticate, authorize } = require('@middlewares/auth');

/**
 * body: phone_number
 */
router.post('/phone-checking', async (req, res, next) => {
  const { phoneNumber } = req.body;

  try {
    const validatedValues = V.string().phone().validate(phoneNumber);

    const { error, value } = validatedValues;

    if (error) {
      throw new BadRequest(new Error(error.details.message));
    }

    const parsedPhone = value.number;
    const result = await authService.phoneChecking(parsedPhone);

    return res.status(httpStatus.OK).json(result);
  } catch (e) {
    return next(e);
  }
});

/**
 * body: phone_number, code
 */
router.post('/register', async (req, res, next) => {
  try {
    const schema = V.object({
      phoneNumber: V.string().required().phone(),
      code: V.string().required().code(),
    });

    const validatedValues = schema.validate(req.body);

    const { error, value } = validatedValues;
    if (error) {
      throw new BadRequest(
        new Error(error.details[0].message),
        error.details[0].message,
      );
    }

    const {
      phoneNumber: { number },
      code,
    } = value;

    const result = await authService.verifyCodeAndCreatePatient(
      number,
      code,
    );

    return res.status(httpStatus.CREATED).json({
      created: true,
      ...result,
    });
  } catch (e) {
    return next(e);
  }
});
/**
 * body: password, confirm_password, access_token
 * at this step, we gona create accessToken for authentication
 */
router.put('/create-password', async (req, res, next) => {
  const { password, confirmPassword, accessToken } = req.body;
  let userData = null;
  // validate token
  try {
    userData = await Token.verifyAsync(accessToken);
  } catch (e) {
    return next(new Forbidden(e, 'Token không đúng.'));
  }

  const schema = V.object({
    password: V.string().required().password(),
    confirmPassword: V.string()
      .required()
      .confirmPassword(V.ref('password')),
  });

  try {
    const validatedValues = schema.validate({
      password,
      confirmPassword,
    });

    const { error } = validatedValues;

    if (error) {
      const detail = error.details[0];
      throw new BadRequest(new Error(detail.message), detail.message);
    }

    const user = await authService.createPatientPassword(
      userData.id,
      password,
    );

    const tokens = await authService.createTokens(user.id);

    return res.status(httpStatus.OK).json({
      user: {
        id: user.id,
        phone: user.phone,
        isFullfill: user.isFullfill,
        isCreated: user.isCreated,
      },
      ...tokens,
    });
  } catch (err) {
    return next(err);
  }
});
/**
 * body: id, full_name, birthday, gender
 */
router.put(
  '/update-info',
  authenticate(),
  authorize([systemRole.patient]),
  async (req, res, next) => {
    const { fullName, birthday, gender } = req.body;

    const { id } = req.user;

    try {
      const schema = V.object({
        id: V.string().guid({
          version: ['uuidv4'],
        }),
        fullName: V.string().fullName(),
        birthday: V.string().birthday(),
        gender: V.number().in(Object.values(genderEnum)),
      });

      const validatedValues = schema.validate({
        id,
        fullName,
        birthday,
        gender,
      });

      const { error, value } = validatedValues;

      if (error) {
        const detail = error.details[0];
        throw new BadRequest(
          new Error(detail.message),
          detail.message,
        );
      }

      const result = await userService.updateUserInfo({
        ...value,
        birthday: value.birthday.toDate(),
      });

      return res.status(httpStatus.OK).json({
        user: {
          id: result.id,
          fullName: result.fullName,
          gender: result.gender,
          birthday: result.birthday,
        },
      });
    } catch (err) {
      return next(err);
    }
  },
);

router.post('/login', authService.loginWithPhone);

router.post(
  '/forgot-password/request-code',
  async (req, res, next) => {
    const { phoneNumber, password, confirmPassword } = req.body;

    try {
      const schema = V.object({
        phoneNumber: V.string().required().phone(),
        password: V.string().required().password(),
        confirmPassword: V.string()
          .required()
          .confirmPassword(V.ref('password')),
      });
      const validatedValues = schema.validate({
        phoneNumber,
        password,
        confirmPassword,
      });

      const { value, error } = validatedValues;
      if (error) {
        const detail = error.details[0];
        throw new BadRequest(
          new Error(detail.message),
          detail.message,
        );
      }

      const verification = await twilioService.requestCode(
        value.phoneNumber.number,
      );

      return res.status(httpStatus.OK).json({
        verification: {
          phoneNumber: verification.to,
          status: verification.status,
          valid: verification.valid,
        },
      });
    } catch (e) {
      return next(e);
    }
  },
);

router.put('/forgot-password', async (req, res, next) => {
  const { phoneNumber, password, confirmPassword, code } = req.body;
  try {
    const schema = V.object({
      phoneNumber: V.string().required().phone(),
      password: V.string().required().password(),
      confirmPassword: V.string()
        .required()
        .confirmPassword(V.ref('password')),
      code: V.string().required().code(),
    });
    const validatedValues = schema.validate({
      phoneNumber,
      password,
      confirmPassword,
      code,
    });

    const { value, error } = validatedValues;
    if (error) {
      const detail = error.details[0];
      throw new BadRequest(new Error(detail.message), detail.message);
    }

    const [, user] = await Promise.all([
      twilioService.verifyCode(value.phoneNumber.number, code),
      userService.getUserByPhone(value.phoneNumber.number),
    ]);

    const isUpdated = await userService.changePassword(
      user.id,
      password,
    );

    if (!isUpdated) {
      throw new InternalServerError(
        new Error('Không thể cập nhật mật khẩu.'),
      );
    }

    return res.status(httpStatus.OK).json({
      isUpdated,
      user: {
        id: user.id,
        fullName: user.fullName,
        phone: user.phone,
      },
    });
  } catch (e) {
    return next(e);
  }
});

module.exports = router;
