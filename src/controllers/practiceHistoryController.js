const router = require('express').Router();
const httpStatus = require('http-status');
const { authenticate, authorize } = require('@middlewares/auth');
const {
  role: UserRole,
  practice: practiceConstant,
} = require('@constants');
const { V, BadRequest, Pagination } = require('@utils');
const { workoutType: systemWorkoutType } = require('@constants');
const workoutTimesService = require('@services/workoutTimesService');
const practiceAppreciationService = require('@services/practiceAppreciationService');
const exerciseService = require('@services/exercisesService');
const categoriesService = require('@services/categoriesService');
const practiceService = require('../services/practiceService');
const {
  appreciateResult,
} = require('../services/practiceAppreciationService');
const pathSuggestionService = require('../services/pathSuggestionService');

// get history of user in a month
router.get(
  '/',
  authenticate(),
  authorize([UserRole.patient]),
  async (req, res, next) => {
    try {
      const { id: patientId } = req.user;
      const { month, year } = req.query;

      const {
        error: paginationError,
        limit,
        page,
      } = Pagination.isValid(req.query.limit, req.query.page);

      if (paginationError) {
        throw new BadRequest(new Error(paginationError));
      }

      const type =
        req.query.type || practiceConstant.paginateType.ALL;

      const schema = V.object({
        month: V.number().integer().min(1).max(12).required(),
        year: V.number()
          .integer()
          .min(2020)
          .max(new Date().getFullYear())
          .required(),
        type: V.string()
          .in(Object.values(practiceConstant.paginateType))
          .required(),
      });

      const { error, value } = schema.validate({
        month,
        year,
        type,
      });

      if (error) {
        throw new BadRequest(new Error(error.details[0].message));
      }
      let histories = await workoutTimesService.findWorkoutTimesListInMonth(
        {
          limit,
          page,
          patientId,
          ...value,
          workoutTypes: [
            systemWorkoutType.PRACTICE,
            systemWorkoutType.CHECKING,
          ],
        },
      );

      histories = histories.map((val) => {
        return {
          ...val,
          ...practiceAppreciationService.appreciateResult(val.score),
        };
      });

      return res.status(httpStatus.OK).json({ histories });
    } catch (e) {
      return next(e);
    }
  },
);

router.get(
  '/lasted-checking',
  authenticate(),
  authorize(UserRole.patient),
  async (req, res, next) => {
    const { id: patientId } = req.user;

    try {
      const lastedWorkoutTimes = await workoutTimesService.getLastedWorkoutTimes(
        patientId,
        systemWorkoutType.CHECKING,
      );

      if (!lastedWorkoutTimes) {
        return res.status(httpStatus.OK).json({
          workoutTimes: null,
        });
      }

      const practicesInWorkout = await practiceService.getPracticesInWorkoutTimes(
        lastedWorkoutTimes.id,
      );

      const [
        checkedExercisesWithCategory,
        categories,
      ] = await Promise.all([
        exerciseService.filterExerciseWithCategory(
          practicesInWorkout.map((val) => val.exerciseId),
        ),
        categoriesService.getAllCategories(),
      ]);

      const categoriesMapping = {};
      (checkedExercisesWithCategory || []).forEach(
        (exerciseWithCate) => {
          categoriesMapping[
            exerciseWithCate.exerciseId
          ] = exerciseWithCate;
        },
      );

      const details = categories.map((cate) => {
        const { id, name } = cate.toJSON();
        return {
          id,
          name,
          practices: [],
        };
      });

      practicesInWorkout.forEach((practice) => {
        const { categoryId, exerciseName } = categoriesMapping[
          practice.exerciseId
        ];

        for (let i = 0; i < details.length; i += 1) {
          if (details[i].id === categoryId) {
            details[i].practices = [
              ...details[i].practices,
              { ...practice, exerciseName },
            ];
          }
        }
      });

      return res.status(httpStatus.OK).json({
        workoutTimes: {
          ...lastedWorkoutTimes,
          ...practiceAppreciationService.appreciateResult(
            lastedWorkoutTimes.score,
          ),
          details,
        },
      });
    } catch (e) {
      return next(e);
    }
  },
);

router.get(
  '/appreciation',
  authenticate(),
  authorize(UserRole.patient),
  async (req, res, next) => {
    const { categoryId } = req.query;
    const { id: patientId } = req.user;
    try {
      const { error } = V.string()
        .guid()
        .required()
        .validate(categoryId);
      if (error) {
        throw new BadRequest(
          error,
          'Id danh mục không đúng định dạng',
        );
      }

      const data = await workoutTimesService.calculateCategoryScore(
        patientId,
        categoryId,
      );

      if (!data) {
        return res.sendStatus(httpStatus.NO_CONTENT);
      }

      const { analyzedData, score } = data;

      return res.status(httpStatus.OK).json({
        categoryId,
        analyzedData,
        score,
        ...appreciateResult(score),
      });
    } catch (e) {
      return next(e);
    }
  },
);

router.get(
  '/progression',
  authenticate(),
  authorize(UserRole.patient),
  async (req, res, next) => {
    const { id: patientId } = req.user;

    try {
      // get current path
      const { doctor, workoutTimes } =
        (await pathSuggestionService.getSelectingPathWithDoctor(
          patientId,
        )) || {};
      // console.log('currentPaths', workoutTimes);

      if (!doctor || !workoutTimes) {
        return res.sendStatus(httpStatus.NO_CONTENT);
      }

      const totalPractices = (workoutTimes.exerciseList || []).length;

      const practices =
        (await practiceService.getPracticesInWorkoutTimes(
          workoutTimes.id,
        )) || [];

      let numOfPassed = 0;

      const appreciatedPractices = practices.map((p) => {
        const ptc = {
          ...p,
          ...practiceAppreciationService.appreciateResult(p.score),
        };
        if (ptc.isPassed) {
          numOfPassed += 1;
        }
        return ptc;
      });

      const notCompletedIds = workoutTimes.exerciseList.filter(
        (exId) =>
          (practices || []).every((p) => p.exerciseId !== exId),
      );
      const nextExerciseId =
        notCompletedIds && notCompletedIds.length > 0
          ? notCompletedIds[0]
          : null;

      let nextExercise = null;
      if (nextExerciseId) {
        nextExercise = await exerciseService.findExerciseById(
          nextExerciseId,
        );
      }

      return res.status(httpStatus.OK).json({
        doneNumber: practices.length,
        totalNumber: totalPractices,
        numOfPassed,
        donePractices: appreciatedPractices,
        nextExercise,
        doctor: {
          id: doctor.id,
          fullName: doctor.full_name,
          avatar: doctor.avatar,
          hospitalName: doctor.hospital_name,
        },
      });
    } catch (e) {
      return next(e);
    }
  },
);

module.exports = router;
