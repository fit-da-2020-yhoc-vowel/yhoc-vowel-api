const router = require('express').Router();
const httpStatus = require('http-status');
const exerTypesService = require('@services/exerciseTypesService');
const exerService = require('@services/exercisesService');
const { V, BadRequest } = require('@utils');
const { role: systemRole } = require('@constants');
const { authenticate, authorize } = require('@middlewares/auth');

// get exercises in type by ID
router.get(
  '/:id',
  authenticate(),
  authorize([
    systemRole.admin,
    systemRole.doctor,
    systemRole.patient,
  ]),
  async (req, res, next) => {
    const { id } = req.params;

    try {
      const { error } = V.string().guid().validate(id);

      if (error) {
        throw new BadRequest(
          new Error(error),
          'Id không đúng định dạng.',
        );
      }
      const result = await exerTypesService.findExerciseTypeById(id);
      return res.status(httpStatus.OK).json(result);
    } catch (e) {
      return next(e);
    }
  },
);

// get exercises by exerciseTypeId
router.get(
  '/:id/exercises',
  authenticate(),
  authorize([
    systemRole.admin,
    systemRole.doctor,
    systemRole.patient,
  ]),
  async (req, res, next) => {
    const { id } = req.params;

    try {
      const { error } = V.string().guid().validate(id);

      if (error) {
        throw new BadRequest(
          new Error(error),
          'Id không đúng định dạng.',
        );
      }
      const result = await exerService.findExercisesByTypeID(id);
      return res.status(httpStatus.OK).json({ exercises: result });
    } catch (e) {
      return next(e);
    }
  },
);

// create a exercise type
router.post(
  '/',
  authenticate(),
  authorize([systemRole.admin]),
  async (req, res, next) => {
    const { categoryId, name } = req.body;
    try {
      const schema = V.object({
        categoryId: V.string().required().guid(),
        name: V.string().required(),
      });

      const { error } = schema.validate({ categoryId, name });
      if (error) {
        throw new BadRequest(
          new Error(error.details[0].message),
          error.details[0].message,
        );
      }
      const result = await exerTypesService.createExerciseType({
        categoryId,
        name,
      });
      return res.status(httpStatus.OK).json(result);
    } catch (e) {
      return next(e);
    }
  },
);
module.exports = router;
