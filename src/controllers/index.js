const router = require('express').Router();

router.use('/patients', require('./patient'));
// router.use('/admin', require('./admin'));
// router.use('/doctor', require('./doctor'));
router.use('/auth', require('./authController'));
router.use('/hospital', require('./hospitalController'));
router.use('/doctor', require('./doctorController'));

router.use('/categories', require('./categoriesController'));
router.use('/exercises', require('./exercisesController'));
router.use('/exercise-types', require('./exerciseTypesController'));
router.use('/users', require('./userController'));
router.use('/practices', require('./practiceController'));
router.use('/connect', require('./connectDoctorController'));
router.use('/comment', require('./commentDoctorController'));
router.use('/histories', require('./practiceHistoryController'));
router.use('/conversations', require('./conversationController'));

module.exports = router;
