const router = require('express').Router();
const httpStatus = require('http-status');
const commentService = require('@services/commentService');
const { V, BadRequest } = require('@utils');
const { role: systemRole } = require('@constants');
const { authenticate, authorize } = require('@middlewares/auth');

// get comment in workout time
router.get(
  '/?',
  authenticate(),
  authorize([systemRole.patient, systemRole.doctor]),
  async (req, res, next) => {
    const { workoutTimeId } = req.query;
    try {
      const { error } = V.string().guid().validate(workoutTimeId);

      if (error) {
        throw new BadRequest(
          new Error(error),
          'Id không đúng định dạng.',
        );
      }
      const result = await commentService.getCommentByWorkoutTimeId(
        workoutTimeId,
      );
      return res.status(httpStatus.OK).json({ comments: result });
    } catch (e) {
      return next(e);
    }
  },
);

// create a new comment by doctor
router.post(
  '/create',
  authenticate(),
  authorize([systemRole.doctor]),
  async (req, res, next) => {
    const doctorId = req.user.id;
    const { workoutTimeId, content } = req.body;
    try {
      const schema = V.object({
        workoutTimeId: V.string()
          .guid()
          .required()
          .error(new Error('Id không đúng định dạng')),
        content: V.string()
          .min(1)
          .max(200)
          .required()
          .error(new Error(`Bình luận trong khoảng 1 đến 200`)),
      });

      const { error } = schema.validate({
        workoutTimeId,
        content,
      });

      if (error) {
        throw new BadRequest(new Error(error));
      }
      const result = await commentService.createNewComment(
        workoutTimeId,
        doctorId,
        content,
      );
      return res.status(httpStatus.OK).json({ comment: result });
    } catch (e) {
      return next(e);
    }
  },
);
// edit a comment by doctor
router.put(
  '/edit',
  authenticate(),
  authorize([systemRole.doctor]),
  async (req, res, next) => {
    const doctorId = req.user.id;
    const { commentId, content } = req.body;
    try {
      const schema = V.object({
        commentId: V.string()
          .guid()
          .required()
          .error(new Error('Id không đúng định dạng')),
        content: V.string()
          .min(1)
          .required()
          .error(new Error('Nội dung không được rỗng.')),
      });

      const { error } = schema.validate({
        commentId,
        content,
      });

      if (error) {
        throw new BadRequest(new Error(error));
      }

      const result = await commentService.editComment(
        commentId,
        content,
        doctorId,
      );
      return res.status(httpStatus.OK).json({ comment: result });
    } catch (e) {
      return next(e);
    }
  },
);
// delete a comment by doctor
router.delete(
  '/delete?',
  authenticate(),
  authorize([systemRole.doctor]),
  async (req, res, next) => {
    const { commentId } = req.query;
    const doctorId = req.user.id;
    try {
      const { error } = V.string()
        .guid()
        .require()
        .validate(commentId);

      if (error) {
        throw new BadRequest(
          new Error(error),
          'Id không đúng định dạng.',
        );
      }

      const result = await commentService.deleteComment(
        commentId,
        doctorId,
      );
      return res.status(httpStatus.OK).json(result);
    } catch (e) {
      return next(e);
    }
  },
);

module.exports = router;
