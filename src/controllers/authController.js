const router = require('express').Router();
const authService = require('@services/authService');
const userService = require('@services/userService');
const doctorService = require('@services/doctorService');
const httpStatus = require('http-status');
const {
  V,
  BadRequest,
  InternalServerError,
  Forbidden,
  NotExist,
} = require('@utils');
const redisService = require('../services/redisService');

router.post('/login', authService.loginWithEmail);

router.put('/forgot-password/request', async (req, res, next) => {
  try {
    const { email } = req.body;
    const { error } = V.string().email().required().validate(email);

    if (error) {
      throw new BadRequest(new Error(error));
    }
    const result = await doctorService.sendEmailToResetPassword(
      email,
    );
    return res.status(httpStatus.OK).json({
      sendResult: {
        success: result.success,
        message: result.message,
        token: result.token,
      },
    });
  } catch (e) {
    return next(e);
  }
});

router.put('/reset-password', async (req, res, next) => {
  const { newPassword, confirmNewPassword, token } = req.body;
  // verify password
  try {
    const schema = V.object({
      newPassword: V.string().required().password(),
      confirmNewPassword: V.string()
        .required()
        .confirmPassword(V.ref('newPassword')),
      token: V.string().guid().required(),
    });
    const validatedValues1 = schema.validate({
      newPassword,
      confirmNewPassword,
      token,
    });

    const { error } = validatedValues1;
    if (error) {
      throw new BadRequest(new Error(error));
    }

    const userId = await redisService.getForgotPasswordToken(token);
    if (!userId) {
      throw new Forbidden(
        new Error('Token không đúng hoặc đã hết hạn.'),
      );
    }

    const user = await userService.getUserById(userId);
    if (!user) {
      throw new NotExist(new Error('Người dùng không tồn tại.'));
    }

    const isUpdated = await userService.changePassword(
      user.id,
      newPassword,
    );

    if (!isUpdated) {
      throw new InternalServerError(
        new Error('Không thể thay đổi mật khẩu.'),
      );
    }

    Promise.resolve(redisService.removeForgotPasswordToken(token));
    return res.status(httpStatus.OK).json({
      isUpdated,
      // password: status,
    });
  } catch (e) {
    return next(e);
  }
});

module.exports = router;
