const router = require('express').Router();
const httpStatus = require('http-status');
const exerService = require('@services/exercisesService');
const { V, BadRequest } = require('@utils');
const { role: systemRole } = require('@constants');
const { authenticate, authorize } = require('@middlewares/auth');

router.get(
  '/',
  authenticate(),
  authorize([
    systemRole.patient,
    systemRole.doctor,
    systemRole.admin,
  ]),
  async (req, res, next) => {
    try {
      const exercises = await exerService.getAll();
      return res.status(httpStatus.OK).json({ exercises });
    } catch (e) {
      return next(e);
    }
  },
);

// get category by id
router.get(
  '/:id',
  authenticate(),
  authorize([
    systemRole.admin,
    systemRole.doctor,
    systemRole.patient,
  ]),
  async (req, res, next) => {
    const { id: exerciseId } = req.params;

    try {
      const { error } = V.string()
        .guid()
        .required()
        .validate(exerciseId);
      if (error) {
        throw new BadRequest(
          new Error(error),
          'Id không dúng định dạng.',
        );
      }
      const result = await exerService.findExerciseById(exerciseId);
      return res.status(httpStatus.OK).json(result);
    } catch (e) {
      return next(e);
    }
  },
);
// create a exercise
router.post(
  '/',
  authenticate(),
  authorize([systemRole.admin]),
  async (req, res, next) => {
    const { name, exerciseTypeId } = req.body;
    try {
      const schema = V.object({
        name: V.string().required(),
        exerciseTypeId: V.string().guid().required(),
      });

      const { error } = schema.validate({ name, exerciseTypeId });

      if (error) {
        throw new BadRequest(new Error(error));
      }
      const result = await exerService.createExercise({
        name,
        exerciseTypeId,
      });
      return res.status(httpStatus.OK).json(result);
    } catch (e) {
      return next(e);
    }
  },
);

module.exports = router;
