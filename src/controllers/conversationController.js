const router = require('express').Router();
const conversationService = require('@services/conversationService');
const httpStatus = require('http-status');
const { role: systemRole } = require('@constants');
const { authenticate, authorize } = require('@middlewares/auth');
const { V, BadRequest, Pagination } = require('@utils');
const { SocketInstance } = require('../socket');

// get all conversations of user
router.get(
  '/',
  authenticate(),
  authorize([systemRole.doctor, systemRole.patient]),
  async (req, res, next) => {
    const { id: userId } = req.user;
    try {
      const result =
        (await conversationService.findConversationsByUserId(
          userId,
        )) || [];
      return res
        .status(httpStatus.OK)
        .json({ conversations: result });
    } catch (e) {
      return next(e);
    }
  },
);

router.post(
  '/:conversationId/messages',
  authenticate(),
  authorize([systemRole.patient, systemRole.doctor]),
  async (req, res, next) => {
    const { content } = req.body;
    const { conversationId } = req.params;
    const { id: userId } = req.user;
    try {
      const schema = V.object({
        conversationId: V.string()
          .isObjectId()
          .required()
          .error(new Error('Id không đúng định dạng')),
        content: V.string()
          .min(1)
          .required()
          .error(new Error('Nội dung không được rỗng.')),
      });

      const { error } = schema.validate({
        conversationId,
        content,
      });

      if (error) {
        throw new BadRequest(new Error(error));
      }

      const result = await conversationService.addNewMessage(
        conversationId,
        userId,
        content,
      );

      if (result) {
        await SocketInstance.sendMessageToRoom({
          conversationId,
          senderId: userId,
          data: result,
        });
      }
      return res.status(httpStatus.OK).json(result);
    } catch (e) {
      return next(e);
    }
  },
);

router.get(
  '/:conversationId/messages',
  authenticate(),
  authorize([systemRole.doctor, systemRole.patient]),
  async (req, res, next) => {
    const { conversationId } = req.params;
    const { id: userId } = req.user;
    try {
      const { error, limit, page } = Pagination.isValid(
        req.query.limit,
        req.query.page,
      );

      if (error) {
        throw new BadRequest(new Error(error));
      }

      const {
        error: errorV,
      } = V.string().isObjectId().required().validate(conversationId);

      if (errorV) {
        throw new BadRequest(new Error(errorV));
      }

      const result = await conversationService.getMessagesInConversation(
        conversationId,
        userId,
        page,
        limit,
      );

      return res.status(httpStatus.OK).json({ conversation: result });
    } catch (e) {
      return next(e);
    }
  },
);

router.delete(
  '/:conversationId/message/:messageId',
  authenticate(),
  authorize([systemRole.patient, systemRole.doctor]),
  async (req, res, next) => {
    const { conversationId, messageId } = req.params;
    try {
      const schema = V.object({
        conversationId: V.string().isObjectId().required(),
        messageId: V.string().isObjectId().required(),
      });

      const { error } = schema.validate({
        conversationId,
        messageId,
      });

      if (error) {
        throw new BadRequest(new Error(error));
      }
      const result = await conversationService.deleteMessage(
        conversationId,
        messageId,
      );
      return res.status(httpStatus.OK).json({ message: result });
    } catch (e) {
      return next(e);
    }
  },
);

module.exports = router;
