const gender = require('./gender');
const role = require('./role');
const constants = require('./constants');
const signUpMethod = require('./signUpMethod');
const connectDoctor = require('./connectDoctor');
const practice = require('./practice');
const workoutType = require('./workoutType');

module.exports = {
  gender,
  role,
  signUpMethod,
  connectDoctor,
  practice,
  workoutType,
  ...constants,
};
