module.exports = {
  rating: {
    NORMAL: 'Bình thường',
    LOW: 'Rối loạn mức nhẹ',
    MEDIUM: 'Rối loạn mức vừa',
    HIGH: 'Rối loạn mức nặng',
  },
  practiceResult: {
    PASS: 'Đạt',
    FAIL: 'Chưa đạt',
  },
  paginateType: {
    ALL: 'all',
    PASS: 'pass',
    FAIL: 'fail',
  },
  range: {
    PASS: [0, 9],
    FAIL: [10, 15],
    ALL: [0, 15],
  },
};
