module.exports = {
  WAITING: 'WAITING',
  ACCEPTED: 'ACCEPTED',
  DELETE: 'DELETE',
  DOCTOR_INVITATION: 'DOCTOR_INVITATION',
  PATIENT_INVITATION: 'PATIENT_INVITATION',
};
