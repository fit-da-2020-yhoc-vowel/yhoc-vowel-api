require('module-alias/register');
const express = require('express');
const rootPath = require('app-root-path');
const httpStatus = require('http-status');
const morgan = require('morgan');
const path = require('path');
const cors = require('cors');
const { LoadConfiguration } = require('@config');
const { createMongoConnection } = require('@mongo-models');

LoadConfiguration();
const passport = require('passport');

const { logger } = require('@config/winston');
const {
  errorMiddleware,
  resourceNotFound,
  transformRequest,
  resSnakeCase,
} = require('@middlewares');
const { SocketInstance } = require('./socket');

const app = express();

createMongoConnection();

app.set('trust proxy', true);
app.use(cors());
app.use(express.static('public'));

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev', { stream: logger.stream }));
}

app.use(passport.initialize());
require('@config/passport');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(transformRequest);
app.use(resSnakeCase);

app.use('/api', require('./controllers'));

if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/'));
  app.get('/*', (_req, res, _next) => {
    return res.sendFile(
      path.join(rootPath.path, 'client/index.html'),
    );
  });
} else {
  app.get('/', (req, res) =>
    res.status(httpStatus.OK).json({
      time: new Date().toDateString(),
      message: 'Hello world!',
    }),
  );
}

app.use(resourceNotFound);

app.use(errorMiddleware);

const PORT = process.env.PORT || 5000;

const httpServer = app.listen(PORT, () => {
  logger.info(`App is running at port ${PORT}`);
});

SocketInstance.init(httpServer);
