const envalid = require('envalid');
const dotenv = require('dotenv');
const { logger } = require('@config/winston');
const { str, port, host, url } = require('envalid');

let path = '.env';
if (process.env.NODE_ENV === 'production') {
  path = '.env.staging';
}
dotenv.config({ path });

class Configuration {
  constructor(
    serverPort,
    twilioSID,
    twilioAuthToken,
    appSecret,
    twilioServiceId,
    tokenLivingTime,
    redisHost,
    redisPort,
    audioPath,
    praatExecuterPath,
    praatScriptPath,
    nodemailerEmail,
    nodemailerPassword,
    mailServerUrl,
  ) {
    this.serverPort = serverPort;
    this.twilioSID = twilioSID;
    this.twilioAuthToken = twilioAuthToken;
    this.appSecret = appSecret;
    this.twilioServiceId = twilioServiceId;
    this.tokenLivingTime = tokenLivingTime;
    this.redisHost = redisHost;
    this.redisPort = redisPort;
    this.audioPath = audioPath;
    this.praatExecuterPath = praatExecuterPath;
    this.praatScriptPath = praatScriptPath;
    this.nodemailerEmail = nodemailerEmail;
    this.nodemailerPassword = nodemailerPassword;
    this.mailServerUrl = mailServerUrl;
  }

  setEnvs(
    serverPort,
    twilioSID,
    twilioAuthToken,
    appSecret,
    twilioServiceId,
    tokenLivingTime,
    redisHost,
    redisPort,
    audioPath,
    praatExecuterPath,
    praatScriptPath,
    nodemailerEmail,
    nodemailerPassword,
    mailServerUrl,
  ) {
    this.serverPort = serverPort;
    this.twilioSID = twilioSID;
    this.twilioAuthToken = twilioAuthToken;
    this.appSecret = appSecret;
    this.twilioServiceId = twilioServiceId;
    this.tokenLivingTime = tokenLivingTime;
    this.redisHost = redisHost;
    this.redisPort = redisPort;
    this.audioPath = audioPath;
    this.praatExecuterPath = praatExecuterPath;
    this.praatScriptPath = praatScriptPath;
    this.nodemailerEmail = nodemailerEmail;
    this.nodemailerPassword = nodemailerPassword;
    this.mailServerUrl = mailServerUrl;
  }
}

const AppConfig = new Configuration();

function LoadConfiguration() {
  const env = envalid.cleanEnv(
    process.env,
    {
      PORT: port(),
      TWILIO_SID: str(),
      TWILIO_AUTH_TOKEN: str(),
      APP_SECRET: str(),
      TWILIO_SERVICE_ID: str(),
      REDIS_HOST: host(),
      REDIS_PORT: port(),
      AUDIO_PATH: str(),
      PRAAT_EXECUTER_PATH: str(),
      PRAAT_SCRIPT_PATH: str(),
      EMAIL: str(),
      PASSWORD: str(),
      MAIL_SERVER_URL: url(),
    },
    {
      reporter: (rp) => {
        if (
          Object.entries(rp.errors).length !== 0 ||
          rp.errors.constructor !== Object
        ) {
          logger.error(`Invalid env vars: ${Object.keys(rp.errors)}`);
          process.exit(1);
        }

        logger.info(`Loaded all env variables`);
      },
    },
  );

  AppConfig.setEnvs(
    env.PORT,
    env.TWILIO_SID,
    env.TWILIO_AUTH_TOKEN,
    env.APP_SECRET,
    env.TWILIO_SERVICE_ID,
    env.TOKEN_LIVING_TIME,
    env.REDIS_HOST,
    env.REDIS_PORT,
    env.AUDIO_PATH,
    env.PRAAT_EXECUTER_PATH,
    env.PRAAT_SCRIPT_PATH,
    env.EMAIL,
    env.PASSWORD,
    env.MAIL_SERVER_URL,
  );
}

module.exports = {
  AppConfig,
  LoadConfiguration,
};
