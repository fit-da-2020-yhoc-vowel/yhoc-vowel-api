const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const {
  V,
  NotExist,
  InternalServerError,
  BadRequest,
  Forbidden,
  comparePassword,
} = require('@utils');
const userRepo = require('@repositories/userRepo');
const { role } = require('@constants');
const { AppConfig } = require('@config');

passport.use(
  'phone',
  new LocalStrategy(
    {
      usernameField: 'phoneNumber',
      passwordField: 'password',
    },
    async (phoneNumber, password, done) => {
      try {
        // validate phoneNumber
        const schema = V.object({
          phoneNumber: V.string().phone().required(),
          password: V.string().password().required(),
        });
        const { error, value } = schema.validate({
          phoneNumber,
          password,
        });

        if (error) {
          return done(
            new BadRequest(new Error(error.details[0].message)),
            false,
          );
        }
        const user = await userRepo.findUserByPhone(
          value.phoneNumber.number,
        );

        if (!user) {
          return done(
            new NotExist(
              new Error('Số điện thoại hoặc mật khẩu không đúng.'),
            ),
            false,
          );
        }

        const isSame = await comparePassword(
          value.password,
          user.password,
        );

        if (!isSame) {
          return done(
            new NotExist(
              new Error('Số điện thoại hoặc mật khẩu không đúng.'),
            ),
            false,
          );
        }

        if (
          !Object.values(role).includes(user.role) ||
          user.role !== role.patient
        ) {
          return done(
            new Forbidden(
              new Error(
                'Bạn không thể đăng nhập bằng số điện thoại.',
              ),
            ),
            'Bạn không thể đăng nhập bằng số điện thoại.',
          );
        }
        return done(null, user);
      } catch (e) {
        return done(
          new InternalServerError(e, 'Không thể đăng nhập lúc này.'),
          false,
        );
      }
    },
  ),
);
// admin + doctor
passport.use(
  'email',
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
    },
    async (email, password, done) => {
      try {
        const schema = V.object({
          email: V.string().email().required(),
          password: V.string().password().required(),
        });
        const { error, value } = schema.validate({
          email,
          password,
        });

        if (error) {
          return done(
            new BadRequest(new Error(error.details[0].message)),
            false,
          );
        }
        const user = await userRepo.findUserByEmail(value.email);
        if (!user) {
          return done(
            new NotExist(
              new Error('Email hoặc mật khẩu không đúng.'),
            ),
            false,
          );
        }

        const isSame = await comparePassword(
          value.password,
          user.password,
        );

        if (!isSame) {
          return done(
            new NotExist(
              new Error('Email hoặc mật khẩu không đúng.'),
            ),
            false,
          );
        }

        if (
          !Object.values(role).includes(user.role) ||
          user.role === role.patient
        ) {
          return done(
            new Forbidden(
              new Error('Bạn không thể đăng nhập bằng email.'),
            ),
            'Bạn không thể đăng nhập bằng email.',
          );
        }
        return done(null, user);
      } catch (e) {
        return done(
          new InternalServerError(e, 'Không thể đăng nhập lúc này.'),
          false,
        );
      }
    },
  ),
);

passport.use(
  'phoneJWT',
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: AppConfig.appSecret,
    },
    async (payload, done) => {
      const { id } = payload;
      try {
        const { error } = V.string()
          .guid({
            version: ['uuidv4'],
          })
          .validate(id);
        if (error) {
          return done(
            new Forbidden(new Error('Token không hợp lệ')),
            false,
          );
        }
        const user = await userRepo.findUserById(id);
        if (!user) {
          return done(null, false);
        }

        return done(null, user);
      } catch (e) {
        return done(e, false);
      }
    },
  ),
);
