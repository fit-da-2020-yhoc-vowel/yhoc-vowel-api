const { Forbidden } = require('@utils');

const whitelist = ['http://165.22.104.29/'];

module.exports = {
  origin(origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Forbidden(new Error('Not allowed by CORS')));
    }
  },
};
