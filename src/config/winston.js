const winston = require('winston');
// const rootPath = require('app-root-path');

const { combine, timestamp, printf, colorize } = winston.format;

const consoleFormat = printf(({ level, message, timestamp: ts }) => {
  return `${ts} ${level}: ${message}`;
});

const options = {
  // file: {
  //   level: 'info',
  //   filename: `${rootPath}/logs/app.log`,
  //   handleExceptions: true,
  //   json: true,
  //   maxsize: 5242880, // 5MB
  //   maxFiles: 5,
  //   colorize: false,
  //   format: simple(),
  // },
  pm2: {
    level: 'info',
    handleExceptions: false,
    format: combine(colorize(), timestamp(), consoleFormat),
  },
  console: {
    level: 'debug',
    handleExceptions: false,
    format: combine(colorize(), timestamp(), consoleFormat),
  },
};

const logger = winston.createLogger({
  exitOnError: false,
});

if (process.env.NODE_ENV === 'production') {
  logger.add(new winston.transports.Console(options.pm2));
} else {
  logger.add(new winston.transports.Console(options.console));
}

logger.stream = {
  write: (message) => {
    logger.info(message);
  },
};

module.exports = {
  logger,
};
