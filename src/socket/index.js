/* eslint-disable camelcase */
/* eslint-disable class-methods-use-this */
/* eslint-disable no-param-reassign */
const socketServer = require('socket.io');
const { get } = require('lodash');
const { logger } = require('@config/winston');
const workoutType = require('@constants/workoutType');
const redisService = require('@services/redisService');
const conversationService = require('@services/conversationService');
const { Token } = require('@utils/token');
const { V, BadRequest } = require('@utils');
const {
  events: {
    CONNECTION_EVENT,
    AUTHENTICATE_EVENT,
    AUTHENTICATED_EVENT,
    // NEW_MESSAGE_EVENT,
    RECEIVE_NEW_MESSAGE_EVENT,
    DISCONNECT_EVENT,
    TIMES_SEEN_EVENT,
    SEEN_EVENT,
  },
} = require('./constants');
const SocketUtils = require('./utils');
const { getRoomName } = require('./utils');

const DISCONNECT_TIMEOUT = 5000;

class SocketServer {
  constructor() {
    this.io = null;
  }

  init(httpServer) {
    try {
      if (!this.io) {
        this.io = socketServer(httpServer, {
          serveClient: false,
        });
      }
      // authentication
      this.getChatNamespace().on(CONNECTION_EVENT, (client) => {
        client.isAuthenticated = false;

        this.turnOffSocketInAllNsp();

        client.on(AUTHENTICATE_EVENT, async ({ token } = {}) => {
          try {
            if (!token) {
              client.disconnect('unauthorized');
            } else {
              if (typeof token !== 'string' || token.length === 0) {
                throw new BadRequest(new Error('Wrong token type'));
              }
              const currentUser = await Token.verifyAsync(token);
              client.user = currentUser;

              if (!currentUser) {
                client.disconnect();
                logger.error(`Token verify fail: ${token}`);
              } else {
                this.turnOnSocketInAllNsp(client);
                // get all user conversation

                const conversations = await conversationService.findConversationsByUserId(
                  client.user.id,
                );
                (conversations || []).forEach((cv) => {
                  this.joinRoomFromClient(client, cv._id);
                });

                client.emit(AUTHENTICATED_EVENT);
              }
            }
          } catch (e) {
            logger.error(JSON.stringify(e));
          }
        });

        client.on(
          TIMES_SEEN_EVENT,
          async ({ conversation_id } = {}) => {
            try {
              this.validateMdw(client);
              const {
                error,
              } = V.string()
                .isObjectId()
                .required()
                .validate(conversation_id);
              if (error) {
                throw new BadRequest(new Error(error));
              }
              this.updateTimeSeen(
                {
                  conversationId: conversation_id,
                  senderId: client.user.id,
                },
                client,
              );
            } catch (e) {
              logger.error(JSON.stringify(e));
            }
          },
        );

        // client.on(
        //   NEW_MESSAGE_EVENT,
        //   async ({ content, conversationId } = {}) => {
        //     // save message to db
        //     const result = await conversationService.addNewMessage(
        //       conversationId,
        //       client.user.id,
        //       content,
        //     );

        //     if (result) {
        //       await this.sendMessageToRoom(
        //         {
        //           conversationId: result.conversationId,
        //           data: result,
        //           senderId: client.user.id,
        //         },
        //         client,
        //       );
        //     }
        //   },
        // );

        setTimeout(() => {
          if (!client.isAuthenticated) {
            client.disconnect('unauthorized');
          }
        }, DISCONNECT_TIMEOUT);

        client.on(DISCONNECT_EVENT, () => {
          try {
            this.validateMdw(client);
            Promise.resolve(
              redisService.removeWorkoutTimes(
                client.user.id,
                workoutType.PRACTICE,
              ),
            );
          } catch (e) {
            logger.error(e);
          }
        });
      });

      logger.info('Socket Server is running!');
    } catch (e) {
      logger.error("Can't initialize Socket Server");
      // return process.exit(1);
    }
  }

  getChatNamespace() {
    return this.io.of('/chat');
  }

  validateMdw(client) {
    const senderId = get(client, 'user.id');
    if (!senderId) {
      throw new Error('Client not contain user information.');
    }
  }

  getClientById(userId) {
    return Object.values(this.getChatNamespace().connected).find(
      (socket) => socket.user.id === userId,
    );
  }

  joinRoom(userId, conversationId) {
    const client = this.getClientById(userId);

    if (client) {
      this.joinRoomFromClient(client, conversationId);
    }
  }

  joinRoomFromClient(client, conversationId) {
    logger.info(
      `Client ${client.user.id} join room ${conversationId}`,
    );
    client.join(SocketUtils.getRoomName(conversationId));
  }

  async updateTimeSeen(
    { conversationId, senderId, timeSeen = new Date() },
    client = null,
  ) {
    if (!client) {
      client = this.getClientById(senderId);
    }
    const timeSeenResult = await conversationService.updateTimeSeen(
      conversationId,
      client.user.id,
      timeSeen,
    );

    if (timeSeenResult) {
      client.to(getRoomName(conversationId)).emit(SEEN_EVENT, {
        conversation_id: conversationId,
        ...timeSeenResult,
      });
    }
  }

  async sendMessageToRoom(
    { conversationId, senderId, data },
    client = null,
  ) {
    // get sender socket
    try {
      if (!client) {
        client = this.getClientById(senderId);
      }

      const isInConversation = Object.keys(
        client.rooms,
      ).some((roomName) => SocketUtils.getConversationId(roomName));

      if (isInConversation) {
        client
          .to(getRoomName(conversationId))
          .emit(RECEIVE_NEW_MESSAGE_EVENT, data);
      }
    } catch (e) {
      logger.error(JSON.stringify(e));
    }
  }

  turnOffSocketInAllNsp() {
    Array.from(this.io.nsps).map((nsp) => {
      return nsp.on('connect', (socket) => {
        if (!socket.isAuthenticated) {
          delete nsp.connected[socket.id];
        }
      });
    });
  }

  turnOnSocketInAllNsp(socket) {
    socket.isAuthenticated = true;
    Array.from(this.io.nsps).forEach((nsp) => {
      if (Object.keys(nsp.connected).find((id) => id === socket.id)) {
        nsp.connected[socket.id] = socket;
      }
    });
  }
}

const instance = new SocketServer();

module.exports = {
  SocketInstance: instance,
};
