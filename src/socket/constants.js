module.exports = {
  events: {
    CONNECTION_EVENT: 'connection',
    AUTHENTICATE_EVENT: 'authenticate',
    AUTHENTICATED_EVENT: 'authenticated',
    NEW_MESSAGE_EVENT: 'newMessage',
    RECEIVE_NEW_MESSAGE_EVENT: 'receiveNewMessage',
    DISCONNECT_EVENT: 'disconnect',
    TIMES_SEEN_EVENT: 'timesSeen',
    SEEN_EVENT: 'seen',
  },
};
