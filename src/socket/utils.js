class SocketUtils {
  static getRoomName(conversationId) {
    return `private_chat_${conversationId}`;
  }

  static getConversationId(roomName) {
    return roomName.replace('private_chat_', '');
  }
}

module.exports = SocketUtils;
