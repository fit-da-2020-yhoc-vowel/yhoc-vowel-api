const medicalRecordRepo = require('@repositories/medicalRecordRepo');
const connectDoctorRepo = require('@repositories/connectDoctorRepo');
const {
  InternalServerError,
  NotExist,
  ErrorBase,
  Unauthorized,
} = require('@utils');
const { connectDoctor } = require('@constants');

async function findMedicalRecordByPatientId(patientId) {
  const medicalRecord = await medicalRecordRepo.findMedicalRecordByPatientId(
    patientId,
  );

  return medicalRecord;
}

/**
 * flow: kiểm tra hồ sơ người dùng: chưa tồn tại -> tạo mới
 * get array images, add new images to array->update
 * params: patientId: string, images: array new images
 */
async function addImages(patientId, images) {
  try {
    let medicalRecord = await medicalRecordRepo.findMedicalRecordByPatientId(
      patientId,
    );
    // medical is not created
    if (!medicalRecord) {
      medicalRecord = await medicalRecordRepo.createMedicalRecord({
        patientId,
        images,
      });
      return medicalRecord;
    }

    // update images
    const arrImages = medicalRecord.images
      ? [...(medicalRecord.images || []), ...images]
      : [...images];
    const nAffect = await medicalRecordRepo.updateImages({
      id: medicalRecord.id,
      images: arrImages,
    });
    if (nAffect !== 1) {
      throw new InternalServerError(
        new Error('Không thể thêm ảnh vào hồ sơ lúc này'),
      );
    }
    medicalRecord = await medicalRecordRepo.findMedicalRecordById(
      medicalRecord.id,
    );
    return medicalRecord;
  } catch (err) {
    throw new InternalServerError(
      err,
      'Không thể cập nhật hồ sơ bệnh án lúc này.',
    );
  }
}

async function getMedicalRecordByPatientId(patientId, doctorId = '') {
  if (doctorId) {
    const connection = await connectDoctorRepo.findConnectionByPatientIdAndDoctorId(
      patientId,
      doctorId,
    );
    if (!connection || connection.status !== connectDoctor.ACCEPTED) {
      throw new Unauthorized(
        null,
        'Người dùng không có quyền xem hồ sơ bệnh án này',
      );
    }
  }
  const medicalRecord = await medicalRecordRepo.findMedicalRecordByPatientId(
    patientId,
  );
  return medicalRecord;
}

/**
 * flow: check Medical Record is exists -> check image is exists -> delete
 */
async function deleteImage(patientId, image) {
  try {
    const medicalRecord = await medicalRecordRepo.findMedicalRecordByPatientId(
      patientId,
    );
    // medical is not created
    if (!medicalRecord) {
      throw new NotExist(null, 'Hồ sơ người dùng không tồn tại.');
    }
    const { images } = medicalRecord;
    // image is not in record
    if (!images.includes(image)) {
      throw new NotExist(null, 'Ảnh không có trong hồ sơ.');
    }

    // delete image from array
    const index = images.indexOf(image);
    if (index > -1) {
      images.splice(index, 1);
    }

    const nAffect = await medicalRecordRepo.updateImages({
      id: medicalRecord.id,
      images,
    });
    if (nAffect !== 1) {
      throw new InternalServerError(
        new Error('Không thể xóa ảnh ra khỏi hồ sơ lúc này'),
      );
    }
    return { is_deleted: true };
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

module.exports = {
  addImages,
  getMedicalRecordByPatientId,
  deleteImage,
  findMedicalRecordByPatientId,
};
