const conversationRepo = require('@repositories/conversationRepo');
// const userRepo = require('@repositories/userRepo');
const {
  ErrorBase,
  InternalServerError,
  Unauthorized,
  Forbidden,
  NotExist,
} = require('@utils');
const userService = require('@services/userService');
const connectDoctorService = require('./connectDoctorService');

/**
 * flow: check conversation is exists? -> check connection is following -> create
 */
async function createConversation(patientId, doctorId) {
  try {
    const [conversation, connectionChecking] = await Promise.all([
      conversationRepo.findConversationByUserIds([
        patientId,
        doctorId,
      ]),
      connectDoctorService.isUsersFollowing(patientId, doctorId),
    ]);

    if (conversation) {
      return conversation;
    }
    if (!connectionChecking.isFollowing) {
      throw new Forbidden(
        new Error('Chưa có kết nối giữa bác sĩ và bệnh nhân.'),
      );
    }
    const createdConversation = await conversationRepo.createConversation(
      [patientId, doctorId],
    );
    return createdConversation;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(
      new Error(e),
      'Không thể tạo được cuộc hội thoại.',
    );
  }
}

async function findConversationByUserIds(userIds) {
  try {
    const conversation = await conversationRepo.findConversationByUserIds(
      userIds,
    );
    return conversation;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(
      new Error(e),
      'Internal Server Error',
    );
  }
}

async function findConversationsByUserId(userId) {
  try {
    const conversations = await conversationRepo.findConversationsByUserId(
      userId,
    );
    const userIds = conversations.map((cv) => cv.user.user_id);
    const users = (await userService.getUsersByIds(userIds)) || [];

    const usersMap = {};
    Array.from(users).forEach((u) => {
      usersMap[u.id] = u;
    });

    const newConversations = conversations.map((cv) => {
      const { id, fullName, avatar } =
        usersMap[cv.user.user_id] || {};
      return {
        ...cv,
        user: {
          id,
          fullName,
          avatar,
        },
      };
    });
    return newConversations;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

async function updateConversationStatus(id, isDeleted) {
  try {
    const conversation = await conversationRepo.updateStatusConversation(
      id,
      { is_deleted: isDeleted },
    );
    return conversation;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

async function updateTimeSeen(conversationId, userId, timeSeen) {
  try {
    const conversation = await conversationRepo.findConversation(
      conversationId,
    );
    if (!conversation) {
      throw new NotExist(new Error('Cuộc trò chuyện không tồn tại.'));
    }
    let check = false;
    conversation.users.forEach((user) => {
      if (userId === user.user_id) {
        check = true;
      }
    });
    if (!check) {
      throw new Unauthorized(
        new Error('Người dùng không ở trong cuộc trò chuyện này.'),
      );
    }
    await conversationRepo.updateTimeSeen(
      conversationId,
      userId,
      timeSeen,
    );
    const result = await conversationRepo.getTimeSeen(
      conversationId,
      userId,
    );
    return result;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

async function findConversationById(conversationId) {
  const conversation = await conversationRepo.findConversation(
    conversationId,
  );
  return conversation || {};
}

async function addNewMessage(conversationId, sendId, content) {
  try {
    // find connection and conversation
    const conversation = await findConversationById(conversationId);
    if (!conversation || conversation.is_deleted) {
      throw new InternalServerError(
        new Error('Cuộc trò chuyện không tồn tại.'),
      );
    }

    const message = await conversationRepo.addNewMessage(
      conversationId,
      sendId,
      content,
    );
    return {
      conversation_id: conversation._id,
      ...(message ? message.toObject() : {}),
    };
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

async function getMessagesInConversation(
  conversationId,
  userId,
  page,
  limit,
) {
  try {
    let conversation = await conversationRepo.findConversation(
      conversationId,
    );
    if (!conversation) {
      throw new NotExist(null, 'Cuộc trò chuyện không tồn tại.');
    }
    let check = false;
    conversation.users.forEach((user) => {
      if (userId === user.user_id) {
        check = true;
      }
    });
    if (!check) {
      throw new Unauthorized(
        null,
        'Bạn không có quyền xem tin nhắn của cuộc trò chuyện này',
      );
    }
    conversation = await conversationRepo.getMessagesByConversationId(
      conversationId,
      page,
      limit,
    );
    return conversation;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

async function deleteMessage(conversationId, messageId) {
  try {
    let conversation = await conversationRepo.getMessage(
      conversationId,
      messageId,
    );
    conversation = await conversationRepo.deleteMessage(
      conversationId,
      messageId,
    );
    return conversation;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

module.exports = {
  createConversation,
  findConversationsByUserId,
  findConversationByUserIds,
  updateConversationStatus,
  addNewMessage,
  getMessagesInConversation,
  deleteMessage,
  updateTimeSeen,
};
