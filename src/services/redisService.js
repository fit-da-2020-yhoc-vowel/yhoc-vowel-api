const {
  setRedisValue,
  getRedisValue,
  removeRedisValue,
} = require('@utils');

const REFRESH_TOKEN = 'refresh_token';
const FORGOT_PASSWORD = 'forgot_password';

const WORKOUT_TIMES_EXPIRATION = 60 * 60 * 24 * 1;
const REFRESH_TOKEN_EXPIRATION = 60 * 60 * 24 * 7;
const FORGOT_PASSWORD_EXPIRATION = 60 * 5;

function getKey(param1, param2) {
  return `${param1}_${param2}`;
}

async function saveRefreshToken(id, token) {
  await setRedisValue(
    getKey(REFRESH_TOKEN, id),
    token,
    REFRESH_TOKEN_EXPIRATION,
  );
}

async function findWorkoutTimes(userId, workoutType) {
  const result = await getRedisValue(getKey(workoutType, userId));
  return result;
}

async function removeWorkoutTimes(userId, workoutType) {
  const isDeleted = await removeRedisValue(
    getKey(workoutType, userId),
  );
  return isDeleted;
}

async function createWorkoutTimes(
  userId,
  payload,
  workoutType,
  expireAt = WORKOUT_TIMES_EXPIRATION,
) {
  await removeWorkoutTimes(userId, workoutType);
  const key = getKey(workoutType, userId);
  await setRedisValue(key, payload, expireAt);

  return findWorkoutTimes(userId, workoutType);
}

async function getForgotPasswordToken(token) {
  return getRedisValue(getKey(FORGOT_PASSWORD, token));
}

async function saveForgotPasswordToken(userId, token) {
  const key = getKey(FORGOT_PASSWORD, token);
  await setRedisValue(key, userId, FORGOT_PASSWORD_EXPIRATION);

  const retToken = await getForgotPasswordToken(userId);
  return retToken;
}

async function removeForgotPasswordToken(token) {
  await removeRedisValue(getKey(FORGOT_PASSWORD, token));
}

module.exports = {
  saveRefreshToken,
  findWorkoutTimes,
  createWorkoutTimes,
  removeWorkoutTimes,
  getForgotPasswordToken,
  saveForgotPasswordToken,
  removeForgotPasswordToken,
};
