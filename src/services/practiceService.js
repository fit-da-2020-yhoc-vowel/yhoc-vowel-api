const { isArray } = require('lodash');
const rootPath = require('app-root-path');
const { InternalServerError } = require('@utils');
const audioAnalysisService = require('@services/audioAnalysisService');
const practiceRepo = require('@repositories/practiceRepo');
const { getAudioPath } = require('@utils');
const {
  getAvgAnalyzedData,
} = require('./practiceAppreciationService');

async function getPracticesInWorkoutTimes(workoutTimesId) {
  const practices = await practiceRepo.getPracticesInWorkoutTimes(
    workoutTimesId,
  );
  return practices || [];
}

async function getPracticeById(practiceId) {
  const practice = await practiceRepo.getPracticeById(practiceId);
  return practice || null;
}

async function createPractice(workoutTimesId, exerciseId, audio) {
  // tìm kiếm bài tập mà có workoutTimesId và exerciseId.
  // nếu chưa có: tạo mới
  // nếu có rồi: update lại dữ liệu (điểm, chỉ số)

  try {
    const audioStats = await audioAnalysisService.analyzeAudio(
      `${rootPath}/${audio.path}`,
    );

    if (
      !audioStats ||
      audioStats.jitter === null ||
      audioStats.shimmer === null ||
      audioStats.hnr === null
    ) {
      throw new Error('Some analyzed data is null.');
    }

    // calculate score
    const { total } = audioAnalysisService.calculateScore(audioStats);
    // tao practice trong workout
    let practice = await practiceRepo.findPracticeByWorkoutTimesAndExercise(
      workoutTimesId,
      exerciseId,
    );

    if (!practice) {
      practice = await practiceRepo.createNewPractice({
        workoutTimesId,
        exerciseId,
        score: total,
        audioPath: getAudioPath(audio.filename),
        analyzedData: audioStats,
      });
    } else {
      await practiceRepo.updatePractice({
        workoutTimesId,
        exerciseId,
        score: total,
        audioPath: getAudioPath(audio.filename),
        analyzedData: audioStats,
      });

      practice = await getPracticeById(practice.id);
    }

    return practice;
  } catch (e) {
    throw new InternalServerError(
      new Error(e),
      'Không thể phân tích giọng nói.',
    );
  }
}

async function calculateWorkoutTimesScore(workoutTimesId) {
  const practices = await getPracticesInWorkoutTimes(workoutTimesId);

  if (!isArray(practices) || practices.length === 0) {
    return null;
  }

  return getAvgAnalyzedData(practices.map((ptc) => ptc.analyzedData));
}

module.exports = {
  createPractice,
  getPracticesInWorkoutTimes,
  getPracticeById,
  calculateWorkoutTimesScore,
};
