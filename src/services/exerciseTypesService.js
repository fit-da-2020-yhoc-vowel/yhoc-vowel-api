const {
  Conflict,
  NotExist,
  InternalServerError,
  ErrorBase,
} = require('@utils');
const exerTypesRepo = require('@repositories/exercisesTypesRepo');
const cateRepo = require('@repositories/categoriesRepo');

async function findExerciseTypesByCateID(id) {
  try {
    const exerciseTypes = await exerTypesRepo.findExerciseTypesByCateID(
      id,
    );
    return exerciseTypes;
  } catch (e) {
    throw new InternalServerError(
      new Error(e),
      'Internal Server Error',
    );
  }
}
async function findExerciseTypeById(exerciseTypeId) {
  try {
    const exerciseType = await exerTypesRepo.findById(exerciseTypeId);
    return exerciseType;
  } catch (e) {
    throw new InternalServerError(
      new Error(e),
      'Internal Server Error',
    );
  }
}
async function createExerciseType({ categoryId, name }) {
  try {
    const category = await cateRepo.findCategoryByID(categoryId);
    if (category.isDeleted === true) {
      throw new NotExist(null, 'Mục đã bị xóa.');
    }
    const foundExerciseTypes = await exerTypesRepo.findExerciseTypesByName(
      categoryId,
      name,
    );
    if (
      foundExerciseTypes &&
      foundExerciseTypes.length > 0 &&
      foundExerciseTypes.find((el) => el.name === name)
    ) {
      throw new Conflict(null, 'Tên mục đã bị trùng.');
    }
    const exercise = await exerTypesRepo.createExerciseType({
      categoryId,
      name,
    });

    return exercise;
  } catch (e) {
    if (e instanceof ErrorBase) {
      throw e;
    }
    throw new InternalServerError(
      new Error(e),
      'Không tạo được mục.',
    );
  }
}
module.exports = {
  findExerciseTypesByCateID,
  createExerciseType,
  findExerciseTypeById,
};
