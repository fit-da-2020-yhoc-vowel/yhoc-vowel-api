const multer = require('multer');
const { AppConfig } = require('@config');
const {
  Forbidden,
  InternalServerError,
  ErrorBase,
} = require('@utils');

const uploadAudioOptions = {
  limits: {
    fileSize: 2 * 1024 * 1024,
  },
  fileFilter(_req, file, cb) {
    const acceptedExtensions = ['.wav'];
    if (file.originalname) {
      const fileExt = file.originalname.slice(
        file.originalname.lastIndexOf('.'),
      );

      if (!fileExt || !acceptedExtensions.includes(fileExt)) {
        cb(
          new Forbidden(new Error('Audio không đúng định dạng.')),
          false,
        );
      }
      cb(null, true);
    }
    cb(null, false);
  },
  storage: multer.diskStorage({
    destination: (_req, _file, cb) => {
      cb(null, AppConfig.audioPath);
    },
    filename: (req, file, cb) => {
      const fileExt = file.originalname.slice(
        file.originalname.lastIndexOf('.'),
      );
      const userId = req.user.id;
      cb(null, `${userId}-${Date.now()}${fileExt}`);
    },
  }),
};

const multerUploadAudio = multer({
  limits: uploadAudioOptions.limits,
  fileFilter: uploadAudioOptions.fileFilter,
  storage: uploadAudioOptions.storage,
});

function uploadSingleAudio(req, res, next) {
  multerUploadAudio.single('audio')(req, res, (err) => {
    if (err instanceof ErrorBase) {
      return next(err);
    }
    if (err) {
      return next(
        new InternalServerError(new Error('Internal Server Error')),
      );
    }
    if (!req.file) {
      return next(new Forbidden(new Error('Need an audio file')));
    }
    return next();
  });
}

module.exports = { uploadSingleAudio, uploadAudioOptions };
