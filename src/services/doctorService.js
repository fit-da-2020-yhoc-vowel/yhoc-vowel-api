const { v4: uuidV4 } = require('uuid');
const { InternalServerError } = require('@utils');
const { role: systemRole } = require('@constants');
const userRepo = require('@repositories/userRepo');
const { NotExist, Forbidden } = require('@utils');
const { AppConfig } = require('@config');
const transport = require('../utils/sendEmail');
const redisService = require('./redisService');

async function sendEmailToResetPassword(email) {
  try {
    const user = await userRepo.findUserByEmail(email);

    if (!user) {
      throw new NotExist(new Error('Người dùng không tồn tại.'));
    }

    if (
      !(
        Object.values(systemRole).includes(user.role) &&
        user.role !== systemRole.patient
      )
    ) {
      throw new Forbidden(
        new Error('Người dùng không có quyền gửi quên mật khẩu.'),
      );
    }

    const token = uuidV4();
    if (!token) {
      throw new InternalServerError(
        new Error('Không thể tạo được token để lấy lại mật khẩu.'),
      );
    }

    // save token to redis
    await redisService.saveForgotPasswordToken(user.id, token);

    const htmlOutput = `
    <p>(Do not reply)</p>
    <h3>Link reset password: <a href="${AppConfig.mailServerUrl}/forgot-password/${token}">${AppConfig.mailServerUrl}/forgot-password/${token}</a></h3>`;
    const mailOption = {
      from: AppConfig.nodemailerEmail,
      to: email,
      subject: 'Reset Password',
      html: htmlOutput,
    };

    const result = await transport.sendMail(mailOption);
    if (result) {
      return {
        success: true,
        message: 'Gửi email thành công',
        token,
      };
    }
    return {
      success: false,
      message: 'Gửi email thất bại',
      token: '',
    };
  } catch (err) {
    throw new InternalServerError(
      err,
      'Không thể gửi mail quên mật khẩu.',
    );
  }
}

module.exports = {
  sendEmailToResetPassword,
};
