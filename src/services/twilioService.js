const { AppConfig } = require('@config');
const client = require('twilio')(
  AppConfig.twilioSID,
  AppConfig.twilioAuthToken,
);
const { InternalServerError, BadRequest } = require('@utils');
const httpStatus = require('http-status');
const { TWILIO_DEFAULT_CHANNEL } = require('@constants');
const { ErrorBase } = require('../utils/errorHandler');

async function requestCode(phoneNumber) {
  try {
    const verification = await client.verify
      .services(AppConfig.twilioServiceId)
      .verifications.create({
        to: phoneNumber,
        channel: TWILIO_DEFAULT_CHANNEL,
      });
    return verification;
  } catch (err) {
    throw new InternalServerError(
      err,
      'Không thể lấy mã xác nhận bây giờ.',
    );
  }
}

async function verifyCode(phoneNumber, receiveCode) {
  try {
    const verificationCheck = await client.verify
      .services(AppConfig.twilioServiceId)
      .verificationChecks.create({
        to: phoneNumber,
        code: receiveCode,
      });

    if (
      !(
        verificationCheck.status === 'approved' &&
        verificationCheck.valid
      )
    ) {
      throw new BadRequest(
        new Error('Mã xác nhận không được chấp nhận.'),
      );
    }

    return verificationCheck;
  } catch (err) {
    if (err.status === httpStatus.NOT_FOUND) {
      throw new InternalServerError(
        new Error(err),
        'Mã xác nhận đã hết hạn.',
      );
    }
    if (err instanceof ErrorBase) {
      throw err;
    } else {
      throw new InternalServerError(
        new Error(err),
        'Không thể xác nhận bây giờ.',
      );
    }
  }
}

module.exports = {
  requestCode,
  verifyCode,
};
