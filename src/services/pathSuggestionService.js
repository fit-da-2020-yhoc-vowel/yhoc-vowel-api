const userService = require('@services/userService');
const pathSuggestionRepo = require('@repositories/pathSuggestionRepo');

async function findPathSuggestion({ patientId, doctorId }) {
  const paths = await pathSuggestionRepo.findPathSuggestion({
    patientId,
    doctorId,
  });

  return paths;
}

async function findPathsOfPatient(patientId, isCompleted) {
  const paths = await pathSuggestionRepo.findPathsOfPatient(
    patientId,
    isCompleted,
  );

  return paths;
}

async function createNewPathSuggestion({
  patientId,
  doctorId,
  workoutTimesId,
}) {
  const newPath = await pathSuggestionRepo.createNewPathSuggestion({
    patientId,
    doctorId,
    workoutTimesId,
  });

  return newPath;
}

async function getSelectingPathWithDoctor(patientId) {
  const selectingPath = await pathSuggestionRepo.getSelectingPath(
    patientId,
  );
  if (!selectingPath) {
    return null;
  }

  const doctor = await userService.getUserInfoById(
    selectingPath.doctorId,
  );

  return {
    workoutTimes: selectingPath,
    doctor,
  };
}

module.exports = {
  findPathSuggestion,
  createNewPathSuggestion,
  findPathsOfPatient,
  getSelectingPathWithDoctor,
};
