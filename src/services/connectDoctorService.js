const {
  InternalServerError,
  Conflict,
  Unauthorized,
  NotExist,
  ErrorBase,
} = require('@utils');
const { connectDoctor } = require('@constants');
const connectDoctorRepo = require('@repositories/connectDoctorRepo');
const { role } = require('@constants');
const conversationRepo = require('@repositories/conversationRepo');
const db = require('@models');
const workoutTimesService = require('./workoutTimesService');
const userService = require('./userService');

async function getConnectionsByUserId(
  userId,
  userRole,
  status,
  invitationOrigin,
) {
  try {
    const connections = await connectDoctorRepo.findConnectionsByUserId(
      userId,
      userRole,
      status,
      invitationOrigin,
    );
    return connections;
  } catch (e) {
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

async function isUsersFollowing(patientId, doctorId) {
  try {
    const connection = await connectDoctorRepo.findConnectionByPatientIdAndDoctorId(
      patientId,
      doctorId,
    );
    if (!connection || connection.status !== connectDoctor.ACCEPTED) {
      return { isFollowing: false };
    }
    return { isFollowing: true };
  } catch (e) {
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

/**
 * flow: check request: exists->status is delete-> update status, origin
 *                      not exists-> create
 */
async function sendRequestConnection(
  patientId,
  doctorId,
  invitationOrigin,
) {
  try {
    let connection = await connectDoctorRepo.findConnectionByPatientIdAndDoctorId(
      patientId,
      doctorId,
    );
    if (connection) {
      if (connection.status === connectDoctor.DELETE) {
        const nAffect = await connectDoctorRepo.updateConnection(
          connection.id,
          connectDoctor.WAITING,
          { invitationOrigin },
        );
        if (nAffect !== 1) {
          throw new InternalServerError(
            new Error('Không thể gửi yêu cầu theo dõi lúc này'),
          );
        }
        connection = await connectDoctorRepo.findConnectionById(
          connection.id,
        );
        return connection;
      }
      throw new Conflict(
        null,
        'Hai tài khoản đang theo dõi hoặc yêu cầu đã được gửi trước đó',
      );
    }
    // tạo connection
    connection = await connectDoctorRepo.createConnection(
      patientId,
      doctorId,
      invitationOrigin,
    );
    return connection;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

/**
 * flow: connection->exists? -> connection status is waiting ? ->check user is receive user?
 */
async function acceptConnection(connectionId, userId, userRole) {
  try {
    const connection = await connectDoctorRepo.findConnectionById(
      connectionId,
    );
    if (!connection || connection.status !== connectDoctor.WAITING) {
      throw new NotExist(null, 'Yêu cầu theo dõi không tồn tại');
    }
    if (userRole === role.patient) {
      if (
        connection.patientId !== userId ||
        connection.invitationOrigin !==
          connectDoctor.DOCTOR_INVITATION
      ) {
        throw new Unauthorized(
          null,
          'Người dùng không có quyền chấp nhận lời đề nghị này',
        );
      }
    } else if (
      connection.doctorId !== userId ||
      connection.invitationOrigin !== connectDoctor.PATIENT_INVITATION
    ) {
      throw new Unauthorized(
        null,
        'Người dùng không có quyền chấp nhận lời đề nghị này',
      );
    }
    let result = null;
    await db.withTransaction(async () => {
      const nAffect = await connectDoctorRepo.updateConnection(
        connection.id,
        connectDoctor.ACCEPTED,
      );
      if (nAffect !== 1) {
        throw new InternalServerError(
          new Error('Không thể chấp nhận lời đề nghị này.'),
        );
      }
      result = await connectDoctorRepo.findConnectionById(
        connectionId,
      );
      // check phòng chat tồn tại => set is_deleted = false else create
      let conversation = await conversationRepo.findConversationByUserIds(
        [result.patientId, result.doctorId],
      );
      if (conversation) {
        const { _id } = conversation;
        conversation = await conversationRepo.updateStatusConversation(
          _id,
          { is_deleted: false },
        );
      } else {
        conversation = await conversationRepo.createConversation([
          result.patientId,
          result.doctorId,
        ]);
      }
    });

    return result;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

/**
 * flow: connection->exists?->check userId = patientId or doctorId?
 */
async function deleteConnection(connectionId, userId) {
  try {
    const connection = await connectDoctorRepo.findConnectionById(
      connectionId,
    );
    if (!connection) {
      throw new NotExist(null, 'Yêu cầu theo dõi không tồn tại');
    }
    if (
      connection.patientId !== userId &&
      connection.doctorId !== userId
    ) {
      throw new Unauthorized(
        null,
        'Người dùng không có quyền từ chối lời đề nghị này',
      );
    }

    let result = null;

    await db.withTransaction(async () => {
      const nAffect = await connectDoctorRepo.updateConnection(
        connection.id,
        connectDoctor.DELETE,
      );
      if (nAffect !== 1) {
        throw new InternalServerError(
          new Error('Không thể từ chối lời đề nghị này.'),
          'Không thể từ chối lời đề nghị này.',
        );
      }
      result = await connectDoctorRepo.findConnectionById(
        connectionId,
      );
      // xóa phòng chat
      let conversation = await conversationRepo.findConversationByUserIds(
        [result.patientId, result.doctorId],
      );
      if (conversation) {
        const { _id } = conversation;
        conversation = await conversationRepo.updateStatusConversation(
          _id,
          { is_deleted: true },
        );
      }
    });

    return result;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

async function searchDoctorByName(patientId, name) {
  try {
    const result = await connectDoctorRepo.findDoctorByName(
      patientId,
      name,
    );
    return result;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

async function searchPatientByName(patientId, name, page, limit) {
  try {
    const result = await connectDoctorRepo.findPatientByName(
      patientId,
      name,
      page,
      limit,
    );
    return result;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

async function getPatientProfile({ doctorId, patientId }) {
  const patient = await connectDoctorRepo.getPatientProfile({
    doctorId,
    patientId,
  });
  return patient;
}

async function getSingleConnection({ doctorId, patientId }) {
  const connection = connectDoctorRepo.getSingleConnection({
    doctorId,
    patientId,
  });

  return connection;
}

async function countConnections({
  userId,
  userRole,
  connectionStatus,
  invitationOrigin,
}) {
  if (!Object.values(role).includes(userRole)) {
    throw new InternalServerError(new Error('Role is not exists.'));
  }

  const connections = connectDoctorRepo.countConnections({
    userId,
    userRole,
    connectionStatus,
    invitationOrigin,
  });

  return connections;
}

async function get5LastedPatientWorkouts(doctorId) {
  const connections = await getConnectionsByUserId(
    doctorId,
    role.doctor,
    connectDoctor.ACCEPTED,
  );

  let workouts = await Promise.all(
    connections.map((cnt) =>
      workoutTimesService.getLastedWorkoutTimes(
        cnt.patient_id,
        null,
        null,
      ),
    ),
  );

  let patients = await Promise.all(
    connections.map((cnt) => userService.getUserById(cnt.patient_id)),
  );

  patients = patients.map((p) => ({
    id: p.id,
    fullName: p.fullName,
    phone: p.phone,
    avatar: p.avatar,
    birthday: p.birthday,
  }));

  workouts = workouts.filter((wk) => wk !== null);

  const combineData = patients.map((p) => ({
    ...p,
    lastedWorkout: workouts.find((wk) => wk.patientId === p.id),
  }));

  return combineData;
}

module.exports = {
  getConnectionsByUserId,
  sendRequestConnection,
  acceptConnection,
  deleteConnection,
  searchDoctorByName,
  searchPatientByName,
  getPatientProfile,
  getSingleConnection,
  isUsersFollowing,
  countConnections,
  get5LastedPatientWorkouts,
};
