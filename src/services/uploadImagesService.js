const multer = require('multer');
const {
  Forbidden,
  InternalServerError,
  ErrorBase,
} = require('@utils');

// --------------------------Images Record -------------------------------

const uploadImageRecordOptions = {
  limits: {
    fileSize: 30 * 1024 * 1024,
  },
  fileFilter(_req, file, cb) {
    const acceptedExtensions = ['.png', '.jpg'];
    if (file.originalname) {
      const fileExt = file.originalname.slice(
        file.originalname.lastIndexOf('.'),
      );

      if (!fileExt || !acceptedExtensions.includes(fileExt)) {
        return cb(
          new Forbidden(new Error('Hình không đúng định dạng.')),
          false,
        );
      }
      return cb(null, true);
    }
    return cb(null, false);
  },
  storage: multer.diskStorage({
    destination: (req, file, cb) => {
      // cb(null, AppConfig.audioPath);
      cb(null, 'public/medical-record/');
    },
    filename: (req, file, cb) => {
      const fileExt = file.originalname.slice(
        file.originalname.lastIndexOf('.'),
      );
      const userId = req.user.id;
      // cb(null, `${userId}-${Date.now()}${fileExt}`);
      cb(null, `record-${userId}_${Date.now()}${fileExt}`);
    },
  }),
};

const multerUploadImagesRecord = multer({
  limits: uploadImageRecordOptions.limits,
  fileFilter: uploadImageRecordOptions.fileFilter,
  storage: uploadImageRecordOptions.storage,
});

function uploadImagesRecord(req, res, next) {
  multerUploadImagesRecord.array('images', 12)(req, res, (err) => {
    if (err instanceof ErrorBase) {
      return next(err);
    }
    if (err) {
      return next(
        new InternalServerError(new Error('Internal Server Error')),
      );
    }
    if (!req.files) {
      return next(new Forbidden(new Error('Need image files')));
    }
    return next();
  });
}

// ------------------------------------------ Avatar-------------------------------------

const uploadAvatarOptions = {
  limits: {
    fileSize: 30 * 1024 * 1024,
  },
  fileFilter(_req, file, cb) {
    const acceptedExtensions = ['.png', '.jpg'];
    if (file.originalname) {
      const fileExt = file.originalname.slice(
        file.originalname.lastIndexOf('.'),
      );

      if (!fileExt || !acceptedExtensions.includes(fileExt)) {
        return cb(
          new Forbidden(new Error('Hình không đúng định dạng.')),
          false,
        );
      }
      return cb(null, true);
    }
    return cb(null, false);
  },
  storage: multer.diskStorage({
    destination: (req, file, cb) => {
      // cb(null, AppConfig.audioPath);
      cb(null, 'public/avatars/');
    },
    filename: (req, file, cb) => {
      const fileExt = file.originalname.slice(
        file.originalname.lastIndexOf('.'),
      );
      const userId = req.user.id;
      // cb(null, `${userId}-${Date.now()}${fileExt}`);
      cb(null, `avatar-${userId}${fileExt}`);
    },
  }),
};

const multerUploadAvatar = multer({
  limits: uploadAvatarOptions.limits,
  fileFilter: uploadAvatarOptions.fileFilter,
  storage: uploadAvatarOptions.storage,
});

function uploadAvatar(req, res, next) {
  multerUploadAvatar.single('avatar')(req, res, (err) => {
    if (err instanceof ErrorBase) {
      return next(err);
    }
    if (err) {
      return next(
        new InternalServerError(new Error('Internal Server Error')),
      );
    }
    if (!req.file) {
      return next(new Forbidden(new Error('Need an image file')));
    }
    return next();
  });
}

module.exports = {
  uploadAvatar,
  uploadImagesRecord,
  multerUploadImagesRecord,
};
