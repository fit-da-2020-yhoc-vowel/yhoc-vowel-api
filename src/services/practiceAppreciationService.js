const { isArray } = require('lodash');
const { InternalServerError } = require('@utils');
const { practice: practiceConstants } = require('@constants');
const audioAnalysisService = require('./audioAnalysisService');

function appreciateResult(score) {
  const { range, practiceResult, rating } = practiceConstants;
  if (!score) {
    return {};
  }
  if (score >= range.PASS[0] && score <= range.PASS[1]) {
    return {
      rating: score <= 3 ? rating.NORMAL : rating.LOW,
      practiceResult: practiceResult.PASS,
      isPassed: true,
    };
  }

  if (score >= range.FAIL[0] && score <= range.FAIL[1]) {
    return {
      rating: score <= 12 ? rating.MEDIUM : rating.HIGH,
      practiceResult: practiceResult.FAIL,
      isPassed: false,
    };
  }
  throw new InternalServerError(new Error('Score not valid.'));
}

async function getAvgAnalyzedData(listAnalyzedData = []) {
  if (!isArray(listAnalyzedData) || listAnalyzedData.length === 0) {
    return null;
  }

  const wkAnalysisData = (listAnalyzedData || []).reduce(
    (accumulator, current) => {
      accumulator.jitter += current.jitter;
      accumulator.shimmer += current.shimmer;
      accumulator.hnr += current.hnr;
      return accumulator;
    },
    {
      jitter: 0,
      shimmer: 0,
      hnr: 0,
    },
  );

  Object.keys(wkAnalysisData).forEach((key) => {
    wkAnalysisData[key] = Number(
      (wkAnalysisData[key] / listAnalyzedData.length).toFixed(3),
    );
  });

  const { total } = audioAnalysisService.calculateScore(
    wkAnalysisData,
  );

  return {
    score: total,
    analyzedData: wkAnalysisData,
  };
}

module.exports = {
  appreciateResult,
  getAvgAnalyzedData,
};
