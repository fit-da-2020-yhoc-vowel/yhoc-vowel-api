const { isArray } = require('lodash');
const { InternalServerError } = require('@utils');
const workoutTimesRepo = require('@repositories/workoutTimesRepo');
const exerciseServices = require('@services/exercisesService');
const {
  practice: practiceConstant,
  NUMBER_OF_CHECKING_EXERCISE,
} = require('@constants');
const { BadRequest } = require('@utils');
const { workoutType: systemWorkoutType } = require('@constants');
const redisService = require('./redisService');
const {
  getAvgAnalyzedData,
} = require('./practiceAppreciationService');
const practiceService = require('./practiceService');

async function findWorkoutTimesById(workoutTimesId) {
  const workoutTimes = await workoutTimesRepo.findWorkoutTimesById(
    workoutTimesId,
  );
  return workoutTimes;
}

async function createPracticeWorkoutTimes(patientId, exerciseTypeId) {
  const exercisesInType = await exerciseServices.findExercisesByTypeID(
    exerciseTypeId,
  );
  if (!exercisesInType) {
    throw new BadRequest(new Error('Không có bài tập.'));
  }
  const newWorkoutTimes = await workoutTimesRepo.createWorkoutTimes({
    patientId,
    exerciseTypeId,
    exerciseList: exercisesInType.map((val) => val.id),
    workoutType: systemWorkoutType.PRACTICE,
  });

  if (!newWorkoutTimes) {
    throw new BadRequest(new Error('Không tạo được lần tập.'));
  }

  // save workout_times to redis
  const payload = {
    ...newWorkoutTimes.toJSON(),
    done: [],
  };
  await redisService.createWorkoutTimes(
    patientId,
    payload,
    systemWorkoutType.PRACTICE,
  );
  return newWorkoutTimes;
}

async function updateWorkoutTimesScore(workoutTimesId) {
  const { score = null, analyzedData = null } =
    (await practiceService.calculateWorkoutTimesScore(
      workoutTimesId,
    )) || {};

  const isSuccess = await workoutTimesRepo.updateWorkoutTimesScore(
    workoutTimesId,
    score,
    analyzedData,
  );

  let workoutTimes = null;

  if (isSuccess) {
    workoutTimes = await findWorkoutTimesById(workoutTimesId);
  }

  return workoutTimes;
}

async function findWorkoutTimesListInMonth({
  patientId,
  month,
  year,
  type = practiceConstant.paginateType.ALL,
  page = 1,
  limit = 10,
  workoutTypes = [
    systemWorkoutType.PRACTICE,
    systemWorkoutType.CHECKING,
  ],
}) {
  const { range, paginateType } = practiceConstant;
  const payload = {
    patientId,
    month,
    year,
    range: paginateType.ALL,
    page,
    limit,
    workoutTypes,
  };

  switch (type) {
    case paginateType.PASS:
      payload.range = range.PASS;
      break;
    case paginateType.FAIL:
      payload.range = range.FAIL;
      break;
    default:
      payload.range = range.ALL;
  }
  const workoutTimesList = await workoutTimesRepo.findWorkoutTimesListInMonth(
    payload,
  );

  return workoutTimesList || [];
}

async function createCheckingWorkoutTimes(patientId) {
  // generate exercises
  const exercises = await exerciseServices.generateRandomExercise(
    NUMBER_OF_CHECKING_EXERCISE,
  );

  if (!exercises || exercises.length === 0) {
    throw InternalServerError(
      new Error('Không lấy được danh sách bài tập.'),
    );
  }
  const newWorkoutTimes = await workoutTimesRepo.createWorkoutTimes({
    patientId,
    exerciseList: exercises.map((val) => val.id),
    workoutType: systemWorkoutType.CHECKING,
  });
  if (!newWorkoutTimes) {
    throw InternalServerError(new Error('Không tạo được lần tập.'));
  }

  const payload = {
    ...newWorkoutTimes.toJSON(),
    done: [],
  };
  await redisService.createWorkoutTimes(
    patientId,
    payload,
    systemWorkoutType.CHECKING,
  );
  return newWorkoutTimes;
}

async function findWorkoutTimesInDate({
  patientId,
  date = new Date(),
  type = practiceConstant.paginateType.ALL,
  page = 1,
  limit = 10,
  workoutType = systemWorkoutType.PRACTICE,
}) {
  const { range, paginateType } = practiceConstant;
  const payload = {
    patientId,
    date,
    range: paginateType.ALL,
    page,
    limit,
  };

  switch (type) {
    case paginateType.PASS:
      payload.range = range.PASS;
      break;
    case paginateType.FAIL:
      payload.range = range.FAIL;
      break;
    default:
      payload.range = range.ALL;
  }

  const workoutTimesList = await workoutTimesRepo.findWorkoutTimesInDate(
    {
      patientId,
      date,
      type,
      page,
      limit,
      workoutType,
    },
  );

  return workoutTimesList;
}

async function getLastedWorkoutTimes(
  patientId,
  workoutType = systemWorkoutType.PRACTICE,
  isFinished = true,
) {
  const lastedWorkoutTimes = await workoutTimesRepo.getLastedWorkoutTimes(
    patientId,
    workoutType,
    isFinished,
  );

  return lastedWorkoutTimes;
}

async function findLastedFinishedWorkoutsByCategoryId(
  patientId,
  categoryId,
) {
  const workoutTimesList = await workoutTimesRepo.findLastedFinishedWorkoutsByCategoryId(
    patientId,
    categoryId,
  );

  return workoutTimesList;
}

async function calculateCategoryScore(patientId, categoryId) {
  // get all workout times with type = PRACTICE and score not NULL
  const workoutTimesList = await findLastedFinishedWorkoutsByCategoryId(
    patientId,
    categoryId,
  );

  if (!isArray(workoutTimesList) || workoutTimesList.length === 0) {
    return null;
  }

  return getAvgAnalyzedData(
    workoutTimesList.map((wk) => wk.analyzedData),
  );
}

async function getWorkoutTimesList({
  patientId,
  limit = 10,
  page = 1,
  isFinished = null,
}) {
  if (page < 1)
    throw new BadRequest(new Error('Page number is not vaild'));
  const workoutTimesList = await workoutTimesRepo.getWorkoutTimesList(
    {
      patientId,
      limit,
      page,
      isFinished,
    },
  );

  return workoutTimesList;
}

async function createPathWorkoutTimes({
  patientId,
  exerciseList = [],
}) {
  const newWorkoutTimes = await workoutTimesRepo.createWorkoutTimes({
    patientId,
    exerciseList,
    workoutType: systemWorkoutType.PATHS,
  });

  return newWorkoutTimes;
}

async function getCurrentWorkoutPath(patientId) {
  const workout = await workoutTimesRepo.getCurrentWorkoutPath(
    patientId,
  );

  return workout;
}

async function countWorkout({ patientId, isFinished = true }) {
  const total = await workoutTimesRepo.countWorkout({
    patientId,
    isFinished,
  });

  return total;
}

async function getAllWorkout({ patientId, limit, page }) {
  const list = await workoutTimesRepo.getAllWorkout({
    patientId,
    limit,
    page,
  });
  return list;
}

module.exports = {
  createPracticeWorkoutTimes,
  createCheckingWorkoutTimes,
  updateWorkoutTimesScore,
  findWorkoutTimesById,
  findWorkoutTimesListInMonth,
  findWorkoutTimesInDate,
  getLastedWorkoutTimes,
  findLastedFinishedWorkoutsByCategoryId,
  calculateCategoryScore,
  getWorkoutTimesList,
  createPathWorkoutTimes,
  getCurrentWorkoutPath,
  countWorkout,
  getAllWorkout,
};
