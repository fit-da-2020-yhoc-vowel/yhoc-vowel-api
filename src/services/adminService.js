const hospitalRepo = require('@repositories/hospitalRepo');
const userRepo = require('@repositories/userRepo');
const doctorInforRepo = require('@repositories/doctorInforRepo');
const db = require('@models');

const {
  Conflict,
  InternalServerError,
  hashPassword,
  ErrorBase,
} = require('@utils');

async function getAllHospitals() {
  try {
    const hospitals = await hospitalRepo.getAllHospitals();
    return hospitals;
  } catch (e) {
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

async function createNewHospital(name, address) {
  try {
    // find user by phone
    let hospital = await hospitalRepo.findHospitalByName(name);

    if (hospital) {
      throw new Conflict(null, 'Bệnh viện đã tồn tại.');
    }

    if (!hospital) {
      hospital = await hospitalRepo.createNewHospital(name, address);
    }

    return hospital;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}
async function createAccountDoctor(
  email,
  fullName,
  phone,
  password,
  hospitalId,
) {
  try {
    // find user by phone
    let user = await userRepo.findUserByEmail(email);
    if (user && user.isCreated) {
      throw new Conflict(null, 'Email này đã được sử dụng.');
    }
    user = await userRepo.findUserByPhone(phone);
    if (user && user.isCreated) {
      throw new Conflict(null, 'Số điện thoại này đã được sử dụng.');
    }
    const hashed = await hashPassword(password);
    // if not `user` -> create
    await db.withTransaction(async () => {
      if (!user) {
        user = await userRepo.createAccountDoctor(
          email,
          fullName,
          phone,
          hashed,
        );
        if (user.id) {
          await doctorInforRepo.addInformation(user.id, hospitalId);
        }
      }
    });

    return { user };
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}
module.exports = {
  createNewHospital,
  createAccountDoctor,
  getAllHospitals,
};
