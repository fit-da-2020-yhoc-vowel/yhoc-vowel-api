const passport = require('passport');
const httpStatus = require('http-status');
const {
  Token,
  InternalServerError,
  NotExist,
  Forbidden,
} = require('@utils');
const db = require('@models');
const userRepo = require('@repositories/userRepo');
const medicalRecordRepo = require('@repositories/medicalRecordRepo');
const userService = require('./userService');
const twilioService = require('./twilioService');
const redisService = require('./redisService');
const selectingPathService = require('./selectingPathService');

async function generateIdentificationToken(payload) {
  const token = await Token.signAsync(payload, {
    expiresIn: 60 * 10,
  });
  return token;
}

async function verifyCodeAndCreatePatient(phoneNumber, receiveCode) {
  let user = null;
  let token = null;
  await db.withTransaction(async () => {
    const verificationCheck = await twilioService.verifyCode(
      phoneNumber,
      receiveCode,
    );

    // TODO: save phoneNumber to db -> create new account
    user = await userService.createAccountWithPhone(
      verificationCheck.to,
    );

    // create medical record

    const [medicalRecord, selectingPath] = await Promise.all([
      medicalRecordRepo.createMedicalRecord({
        patientId: user.id,
      }),
      selectingPathService.createSelectingPath(user.id),
    ]);

    if (!medicalRecord) {
      throw new InternalServerError(
        new Error('Không thể tạo hồ sơ y tế người dùng'),
      );
    }

    if (!selectingPath) {
      throw new InternalServerError(
        new Error("Can't create SelectingPath"),
      );
    }

    // generate a token -> for the next tine create password.
    token = await generateIdentificationToken({
      id: user.id,
      phoneNumber: user.phone,
    });
  });

  if (!user || !token) {
    throw new InternalServerError(
      new Error('Không thể tạo người dùng lúc này.'),
    );
  }
  return {
    user: { id: user.id, phone: user.phone },
    token,
  };
}

async function phoneChecking(phoneNumber) {
  const user = await userService.getUserByPhone(phoneNumber);

  if (!user || (user && !user.isCreated && !user.password)) {
    // request code.
    const verification = await twilioService.requestCode(phoneNumber);
    return {
      isExist: false,
      phone: verification.to,
      verification: {
        phoneNumber: verification.to,
        status: verification.status,
        valid: verification.valid,
      },
    };
  }
  return {
    isExist: true,
    phone: user.phone,
    user: {
      id: user.id,
      phone: user.phone,
      name: user.name ? user.name : null,
    },
  };
}

async function createPatientPassword(id, password) {
  // find user by id in db
  const user = await userRepo.findUserById(id);
  if (!user) {
    throw new NotExist(new Error('Người dùng không tồn tại.'), null);
  }

  // if have user, check isFullfill & password field
  if (user.password && user.isCreated) {
    throw new Forbidden(
      new Error('Người dùng đã cập nhật thông tin trước đó.'),
      null,
    );
  }

  try {
    const isUpdated = await userService.changePassword(id, password);

    if (!isUpdated) {
      throw new InternalServerError(
        new Error('Không thể cập nhật mật khẩu.'),
      );
    }

    return { id: user.id, phone: user.phone, isCreated: true };
  } catch (err) {
    throw new InternalServerError(
      err,
      'Không thể tạo mật khẩu lúc này.',
    );
  }
}

async function createTokens(userId) {
  try {
    const user = await userService.getUserById(userId);
    if (!user) {
      throw new NotExist(
        new Error('Không tồn tại người dùng.'),
        null,
      );
    }
    const jsonUser = user.toJSON();
    delete jsonUser.password;

    // generate token
    const accessToken = await Token.signAsync(jsonUser);
    const refreshToken = await Token.signAsync(
      { id: user.id },
      { expiresIn: '7d' },
    );

    // save refresh token to redis
    await redisService.saveRefreshToken(user.id, refreshToken);

    return {
      accessToken,
      refreshToken,
    };
  } catch (e) {
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

async function loginWithPhone(req, res, next) {
  passport.authenticate(
    'phone',
    { session: false },
    async (error, account, _info) => {
      if (error) {
        return next(error);
      }

      try {
        const userTemp = account.toJSON();
        delete userTemp.password;

        // generate token
        const tokens = await createTokens(account.id);

        return res.status(httpStatus.OK).json(tokens);
      } catch (e) {
        return next(e);
      }
    },
  )(req, res, next);
}

async function loginWithEmail(req, res, next) {
  passport.authenticate(
    'email',
    { session: false },
    async (error, account, _info) => {
      if (error) {
        return next(error);
      }

      try {
        const userTemp = account.toJSON();
        delete userTemp.password;

        // generate token
        const tokens = await createTokens(account.id);

        return res.status(httpStatus.OK).json(tokens);
      } catch (e) {
        return next(e);
      }
    },
  )(req, res, next);
}

module.exports = {
  verifyCodeAndCreatePatient,
  phoneChecking,
  createPatientPassword,
  loginWithPhone,
  loginWithEmail,
  createTokens,
  generateIdentificationToken,
};
