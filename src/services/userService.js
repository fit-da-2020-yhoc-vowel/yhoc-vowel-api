const userRepo = require('@repositories/userRepo');
const doctorInforRepo = require('@repositories/doctorInforRepo');

const {
  Conflict,
  InternalServerError,
  NotExist,
  Forbidden,
  hashPassword,
  comparePassword,
  ErrorBase,
} = require('@utils');
const { signUpMethod } = require('@constants');
const hospitalRepo = require('../repositories/hospitalRepo');

async function createAccountWithPhone(phoneNumber) {
  try {
    // find user by phone
    let user = await userRepo.findUserByPhone(phoneNumber);

    if (user && user.isCreated) {
      throw new Conflict(null, 'Số điện thoại này đã được sử dụng.');
    }

    // if not `user` -> create
    if (!user) {
      user = await userRepo.createUserWithPhone(phoneNumber);
    }

    return user;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

async function updateUserInfo({
  id,
  phone,
  fullName,
  birthday,
  gender,
  email,
}) {
  try {
    let user = await userRepo.findUserById(id);

    if (!user) {
      throw new NotExist(null, 'Người dùng không tồn tại.');
    }

    switch (user.signUpMethod) {
      case signUpMethod.phone:
        if (phone) {
          throw new Forbidden(
            null,
            'Bạn không thể thay đổi số điện thoại.',
          );
        }
        break;
      case signUpMethod.email:
        if (email) {
          throw new Forbidden(null, 'Bạn không thể thay đổi email.');
        }
        break;
      default:
        break;
    }

    if (phone) {
      user = await userRepo.findUserByPhone(phone.number);

      if (user && user.id !== id) {
        throw new Conflict(
          null,
          'Số điện thoại này đã được sử dụng.',
        );
      }
    }

    const nAffect = await userRepo.updateUserInfo({
      id,
      fullName,
      phone,
      birthday,
      gender,
      email,
      extraFields: { isFullFilled: true },
    });
    if (nAffect !== 1) {
      throw new InternalServerError(
        new Error('Không thể cập nhật thông tin người dùng lúc này.'),
        'Không thể cập nhật thông tin người dùng lúc này.',
      );
    }
    user = await userRepo.findUserById(id);
    return user;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

async function getUserByPhone(phoneNumber) {
  const user = await userRepo.findUserByPhone(phoneNumber);
  return user;
}

async function getUserById(id) {
  const user = await userRepo.findUserById(id);
  return user;
}

async function changePassword(id, password) {
  try {
    const hashed = await hashPassword(password);

    const nAffect = await userRepo.updatePassword(id, hashed);

    if (nAffect === 0) {
      return false;
    }
    return true;
  } catch (err) {
    throw new InternalServerError(
      err,
      'Không thể cập nhật mật khẩu lúc này.',
    );
  }
}

async function checkPassword(id, password) {
  const user = await userRepo.findUserById(id);
  const isSame = await comparePassword(password, user.password);
  return isSame;
}
/*
 * flow: findUserbyID-> set Avatar
 */
async function setAvatar(id, avatar) {
  try {
    let user = await userRepo.findUserById(id);

    if (!user) {
      throw new NotExist(null, 'Người dùng không tồn tại.');
    }

    const nAffect = await userRepo.updateUserInfo({
      id,
      avatar,
    });
    if (nAffect !== 1) {
      throw new InternalServerError(
        new Error('Không thể cập nhật avatar người dùng lúc này.'),
        'Không thể cập nhật avatar người dùng lúc này.',
      );
    }
    user = await userRepo.findUserById(id);
    return user;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

async function getUserInfoById(userId) {
  const user = await userRepo.getUserInfoById(userId);
  return user;
}

async function updateDoctorInfo(doctorId, hospitalId, address) {
  try {
    if (hospitalId) {
      const hospital = await hospitalRepo.findHospitalById(
        hospitalId,
      );
      if (!hospital) {
        throw new NotExist(null, 'Bênh viện không tồn tại.');
      }
    }
    const nAffect = await doctorInforRepo.updateInformation(
      doctorId,
      hospitalId,
      address,
    );
    if (nAffect !== 1) {
      throw new InternalServerError(
        new Error('Không thể cập nhật thông tin bác sĩ lúc này.'),
        'Không thể cập nhật thông tin bác sĩ lúc này.',
      );
    }
    return { is_updated: true };
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

async function getUsers(role, page, limit) {
  const users = await userRepo.getUsers(role, page, limit);
  return users;
}

async function countUsers(role) {
  const result = await userRepo.countUsers(role);
  return result;
}

async function searchUserByName(name, role, page, limit) {
  const result = await userRepo.searchUserByName(
    name,
    role,
    page,
    limit,
  );
  return result;
}

async function countSearchUser(name, role) {
  const result = await userRepo.countSearchUserResult(name, role);
  return result;
}

async function getUsersByIds(userIds = []) {
  const users = await userRepo.getUsersByIds(userIds);

  return users;
}

module.exports = {
  createAccountWithPhone,
  updateUserInfo,
  getUserByPhone,
  getUserById,
  changePassword,
  checkPassword,
  setAvatar,
  getUserInfoById,
  updateDoctorInfo,
  countSearchUser,
  searchUserByName,
  countUsers,
  getUsers,
  getUsersByIds,
};
