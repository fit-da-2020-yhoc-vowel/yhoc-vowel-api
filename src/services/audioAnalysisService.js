const { exec } = require('child_process');
const { AppConfig } = require('@config');
const { InternalServerError } = require('@utils');

function getJitterScore(jitter) {
  if (jitter <= 0.484) return 1;
  if (jitter <= 0.798) return 3;
  return 5;
}

function getShimmerScore(shimmer) {
  if (shimmer <= 3.439) return 1;
  if (shimmer <= 6.501) return 3;
  return 5;
}

function getHNRScore(hnr) {
  if (hnr >= 24.816) return 1;
  if (hnr >= 20.149) return 3;
  return 5;
}

// 0.767%,6.407%,13.604
function formatStdout(stdout) {
  const [jitter, shimmer, hnr] = stdout.split(',');

  return {
    jitter: Number(Number.parseFloat(jitter).toFixed(3) * 100),
    shimmer: Number(Number.parseFloat(shimmer).toFixed(3) * 100),
    hnr: Number(Number.parseFloat(hnr).toFixed(3)),
  };
}

function analyzeAudio(audioPath) {
  return new Promise((resolve, reject) => {
    exec(
      `${AppConfig.praatExecuterPath} --run ${AppConfig.praatScriptPath} ${audioPath}`,
      function (err, stdout, _sdterr) {
        if (err) {
          return reject(
            new InternalServerError(err, 'Internal Server Error'),
          );
        }
        return resolve(formatStdout(stdout));
      },
    );
  });
}

function calculateScore({ jitter, shimmer, hnr }) {
  const score = {
    jitter: getJitterScore(jitter),
    shimmer: getShimmerScore(shimmer),
    hnr: getHNRScore(hnr),
  };
  return {
    score,
    total: score.jitter + score.shimmer + score.hnr,
  };
}

module.exports = {
  analyzeAudio,
  calculateScore,
};
