const userService = require('@services/userService');
const selectingPathRepo = require('@repositories/selectingPathRepo');
const { role: systemRole } = require('@constants');
const { Forbidden } = require('@utils');

async function getSelectingPathOfPatient(patientId) {
  const selectingPath = await selectingPathRepo.getSelectingPathOfPatient(
    patientId,
  );
  return selectingPath;
}

async function createSelectingPath(patientId) {
  const [patient, selectingPath] = await Promise.all([
    userService.getUserById(patientId),
    getSelectingPathOfPatient(patientId),
  ]);

  if (!patient || patient.role !== systemRole.patient) {
    throw new Forbidden(
      new Error('Người này không phải là bệnh nhân.'),
    );
  }

  if (selectingPath) {
    throw new Forbidden(
      new Error('Duplicate patient selecting path.'),
    );
  }

  const result = await selectingPathRepo.createSelectingPath(
    patientId,
  );
  return result;
}

async function updateCurrentPath(patientId, currentPathId) {
  await selectingPathRepo.updateCurrentPath(patientId, currentPathId);

  const selectingPath = await getSelectingPathOfPatient(patientId);

  return selectingPath;
}

module.exports = {
  createSelectingPath,
  getSelectingPathOfPatient,
  updateCurrentPath,
};
