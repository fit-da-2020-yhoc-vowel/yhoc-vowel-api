const {
  InternalServerError,
  NotExist,
  Unauthorized,
  ErrorBase,
} = require('@utils');
const commentRepo = require('@repositories/commentRepo');
const workoutTimesRepo = require('@repositories/workoutTimesRepo');
const connectDoctorRepo = require('@repositories/connectDoctorRepo');
const { connectDoctor } = require('@constants');

/**
 * flow: check doctor is following patient
 */
async function createNewComment(workoutTimeId, doctorId, content) {
  try {
    const workoutTime = await workoutTimesRepo.findWorkoutTimesById(
      workoutTimeId,
    );
    if (!workoutTime) {
      throw new NotExist(null, 'Lịch sử luyện tập không tồn tại');
    }
    const connection = await connectDoctorRepo.findConnectionByPatientIdAndDoctorId(
      workoutTime.patientId,
      doctorId,
    );
    if (!connection || connection.status !== connectDoctor.ACCEPTED) {
      throw new Unauthorized(
        null,
        'Người dùng không có quyền bình luận lịch sử luyện tập này',
      );
    }
    const comment = await commentRepo.createNewComment(
      workoutTimeId,
      doctorId,
      content,
    );
    return comment;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}
async function getCommentByWorkoutTimeId(workoutTimeId) {
  try {
    const comment = await commentRepo.getCommentByWorkoutTimeId(
      workoutTimeId,
    );
    return comment;
  } catch (e) {
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

/**
 * flow: check doctorId = comment.doctorId ? -> delete
 */
async function deleteComment(commentId, doctorId) {
  try {
    const comment = await commentRepo.findCommentById(commentId);
    if (!comment) {
      throw new NotExist(null, 'Bình luận không tồn tại');
    }
    if (comment.doctorId !== doctorId) {
      throw new Unauthorized(
        null,
        'Người dùng không có quyền xóa bình luận này',
      );
    }
    const nAffect = await commentRepo.deleteComment(commentId);
    if (nAffect !== 1) {
      throw new InternalServerError(
        new Error('Không thể xóa comment này.'),
        'Không thể xóa comment này..',
      );
    }
    return { is_deleted: true };
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}

/**
 * flow: check doctorId = comment.doctorId ? -> edit
 */
async function editComment(commentId, content, doctorId) {
  try {
    let comment = await commentRepo.findCommentById(commentId);
    if (!comment) {
      throw new NotExist(null, 'Bình luận không tồn tại');
    }
    if (comment.doctorId !== doctorId) {
      throw new Unauthorized(
        null,
        'Người dùng không có quyền chỉnh sửa bình luận này',
      );
    }
    const nAffect = await commentRepo.editComment(commentId, content);
    if (nAffect !== 1) {
      throw new InternalServerError(
        new Error('Không thể sửa comment này.'),
        'Không thể sửa comment này.',
      );
    }
    comment = await commentRepo.findCommentById(commentId);
    return comment;
  } catch (e) {
    if (e instanceof ErrorBase) throw e;
    throw new InternalServerError(e, 'Internal Server Error');
  }
}
module.exports = {
  createNewComment,
  deleteComment,
  editComment,
  getCommentByWorkoutTimeId,
};
