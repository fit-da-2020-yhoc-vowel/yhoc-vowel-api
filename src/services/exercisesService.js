const { isArray } = require('lodash');
const {
  Conflict,
  NotExist,
  InternalServerError,
  ErrorBase,
  BadRequest,
} = require('@utils');
const exerRepo = require('@repositories/exercisesRepo');
const exerTypeService = require('@services/exerciseTypesService');
const { NUMBER_OF_CHECKING_EXERCISE } = require('@constants');
const exercisesRepo = require('../repositories/exercisesRepo');

async function getAll() {
  const exercises = await exercisesRepo.getAll();
  return exercises;
}

async function findExerciseById(exerciseId) {
  const exercise = await exerRepo.findExerciseByID(exerciseId);
  return exercise;
}

async function findExercisesByTypeID(exerciseTypeId) {
  const exercise = await exerRepo.findExercisesByTypeID(
    exerciseTypeId,
  );
  return exercise;
}
async function createExercise({ exerciseTypeId, name }) {
  try {
    const exerciseType = await exerTypeService.findExerciseTypeById(
      exerciseTypeId,
    );

    if (!exerciseType) {
      throw new NotExist(null, 'Loại bài tập không tồn tại.');
    }

    if (exerciseType.isDeleted) {
      throw new NotExist(null, 'Loại bài tập đã bị xoá.');
    }
    const foundExercises = await exerRepo.findExercisesByName(
      exerciseTypeId,
      name,
    );
    if (
      foundExercises &&
      foundExercises.length > 0 &&
      foundExercises.find((el) => el.name === name)
    ) {
      throw new Conflict(null, 'Tên bài tập đã bị trùng.');
    }

    const exercise = await exerRepo.createExercise({
      exerciseTypeId,
      name,
    });

    return exercise;
  } catch (err) {
    if (err instanceof ErrorBase) {
      throw err;
    }
    throw new InternalServerError(err);
  }
}

async function generateRandomExercise(
  numOfRecord = NUMBER_OF_CHECKING_EXERCISE,
) {
  const exercises = await exerRepo.generateRandomExercise(
    numOfRecord,
  );
  return exercises;
}

async function filterExerciseWithCategory(exerciseIds = []) {
  const exerciseWithCate = await exercisesRepo.getCategoryByExerciseIds(
    exerciseIds,
  );
  return exerciseWithCate;
}

async function findExercisesWithListId(exerciseIds = []) {
  if (!isArray(exerciseIds) || exerciseIds.length === 0) {
    throw new BadRequest(new Error('Exercise must be a list of Id'));
  }
  const list = await exerRepo.findExercisesWithListId(exerciseIds);

  return list;
}

module.exports = {
  findExerciseById,
  findExercisesByTypeID,
  createExercise,
  generateRandomExercise,
  filterExerciseWithCategory,
  findExercisesWithListId,
  getAll,
};
