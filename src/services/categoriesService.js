const { Conflict, NotExist, InternalServerError } = require('@utils');
const cateRepo = require('@repositories/categoriesRepo');

async function getAllCategories() {
  try {
    const categories = await cateRepo.findAllCategories();
    return categories;
  } catch (e) {
    throw new InternalServerError(e, 'Internal Server Error');
  }
}
async function createCategory(Cate) {
  try {
    let category = await cateRepo.findCategoryByName(Cate.name);
    if (category !== null) {
      throw new Conflict(null, 'Tên mục đã bị trùng.');
    }
    if (category === null) {
      category = await cateRepo.createCategory(Cate);
    }
    return category;
  } catch (err) {
    throw new InternalServerError(err, null);
  }
}
async function deleteCategory(idCategory) {
  try {
    const findCategory = await cateRepo.findCategoryByID(idCategory);
    if (!findCategory) {
      throw new NotExist(null, 'Không tìm thấy mục cần xóa.');
    }
    const nAffect = await cateRepo.deleteCategory(idCategory);
    if (nAffect !== 1) {
      throw new InternalServerError(
        new Error('Không thể xóa mục này.'),
        'Không thể xóa mục này.',
      );
    }
    const category = await cateRepo.findCategoryByID(idCategory);
    return category;
  } catch (err) {
    throw new InternalServerError(err, 'Không thể xóa mục này.');
  }
}
async function getCategoryById(cateId) {
  try {
    const category = await cateRepo.findCategoryByID(cateId);

    if (!category) {
      throw new NotExist(new Error('Danh mục không tồn tại.'));
    }
    return category;
  } catch (e) {
    throw new InternalServerError(e, 'Internal Server Error');
  }
}
module.exports = {
  getAllCategories,
  createCategory,
  deleteCategory,
  getCategoryById,
};
