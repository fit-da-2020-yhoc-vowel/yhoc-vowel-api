const { Model, DataTypes } = require('sequelize');
const PathSuggestion = require('./pathSuggestion');
const User = require('./user');

module.exports = (sequelize) => {
  class SelectingPath extends Model {}
  SelectingPath.init(
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
      },
      patientId: {
        type: DataTypes.UUID,
        allowNull: false,
        unique: true,
        field: 'patient_id',
        references: {
          key: 'id',
          model: User(sequelize),
        },
      },
      currentPath: {
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: true,
        field: 'current_path',
        references: {
          key: 'id',
          model: PathSuggestion(sequelize),
        },
      },
    },
    {
      modelName: 'SelectingPath',
      tableName: 'selecting_path',
      sequelize,
      updatedAt: 'updated_at',
      createdAt: 'created_at',
    },
  );
  return SelectingPath;
};
