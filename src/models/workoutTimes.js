const { Model, DataTypes } = require('sequelize');
const { workoutType } = require('@constants');
const ExerciseType = require('./exerciseType');
const User = require('./user');

module.exports = (sequelize) => {
  class WorkoutTimes extends Model {}
  WorkoutTimes.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      patientId: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'patient_id',
        references: {
          model: User(sequelize),
          key: 'id',
        },
      },
      exerciseTypeId: {
        type: DataTypes.UUID,
        allowNull: true,
        field: 'exercise_type_id',
        references: {
          model: ExerciseType(sequelize),
          key: 'id',
        },
      },
      exerciseList: {
        type: DataTypes.JSON,
        allowNull: false,
        field: 'exercise_list',
      },
      score: {
        type: DataTypes.INTEGER,
        defaultValue: null,
        validate: {
          min: 0,
          max: 15,
        },
      },
      analyzedData: {
        type: DataTypes.JSON,
        allowNull: true,
        field: 'analyzed_data',
      },
      workoutType: {
        type: DataTypes.STRING(25),
        defaultValue: workoutType.PRACTICE,
        field: 'workout_type',
        allowNull: true,
      },
    },
    {
      modelName: 'WorkoutTimes',
      tableName: 'workout_times',
      sequelize,
      updatedAt: 'updated_at',
      createdAt: 'created_at',
    },
  );
  return WorkoutTimes;
};
