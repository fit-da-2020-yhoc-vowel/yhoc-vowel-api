const { Model, DataTypes, Deferrable } = require('sequelize');
const Category = require('./category');

module.exports = (sequelize) => {
  class ExercisesTypes extends Model {}
  ExercisesTypes.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      categoryId: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'category_id',
        references: {
          model: Category(sequelize),
          key: 'id',
          deferrable: Deferrable.INITIALLY_IMMEDIATE,
        },
      },
      name: {
        type: DataTypes.TEXT('tiny'),
        allowNull: false,
      },
      isDeleted: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false,
        field: 'is_deleted',
      },
      status: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
    },
    {
      modelName: 'ExerciseType',
      tableName: 'exercise_type',
      sequelize,
      updatedAt: 'updated_at',
      createdAt: 'created_at',
    },
  );
  return ExercisesTypes;
};
