const { Model, DataTypes } = require('sequelize');
const User = require('./user');
const Hospital = require('./hospital');

module.exports = (sequelize) => {
  class DoctorInformation extends Model {}
  DoctorInformation.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      doctorId: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'doctor_id',
        references: {
          model: User(sequelize),
          key: 'id',
        },
      },
      address: {
        type: DataTypes.TEXT('tiny'),
        defaultValue: null,
      },
      hospitalId: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'hospital_id',
        references: {
          model: Hospital(sequelize),
          key: 'id',
        },
      },
      status: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
    },
    {
      modelName: 'DoctorInformation',
      tableName: 'doctor_information',
      sequelize,
      updatedAt: 'updated_at',
      createdAt: 'created_at',
    },
  );
  return DoctorInformation;
};
