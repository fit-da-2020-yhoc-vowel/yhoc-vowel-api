const { Model, DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  class Hospital extends Model {}
  Hospital.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: DataTypes.TEXT('tiny'),
        defaultValue: null,
      },
      address: {
        type: DataTypes.TEXT('tiny'),
        defaultValue: null,
      },
      status: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
    },
    {
      modelName: 'Hospital',
      tableName: 'hospital',
      sequelize,
      updatedAt: 'updated_at',
      createdAt: 'created_at',
    },
  );
  return Hospital;
};
