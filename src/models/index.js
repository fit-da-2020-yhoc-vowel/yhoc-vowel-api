const fs = require('fs');
const cls = require('cls-hooked');
const path = require('path');
const Sequelize = require('sequelize');
const { logger } = require('@config/winston');

const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require('@config/config.json')[env];

const db = {};

const sequelizeNamespace = cls.createNamespace('sequelize-namespace');
Sequelize.Sequelize.useCLS(sequelizeNamespace);

let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(
    process.env[config.use_env_variable],
    config,
  );
} else {
  sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    {
      ...config,
      logging: (msg) => logger.log({ level: 'debug', message: msg }),
    },
  );
}

fs.readdirSync(__dirname)
  .filter((file) => {
    return (
      file.indexOf('.') !== 0 &&
      file !== basename &&
      file.slice(-3) === '.js'
    );
  })
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

sequelize
  .sync()
  // .sync({ force: true })
  .then(() => {
    logger.info('All models were synchronized successfully');
  })
  .catch((err) => {
    logger.error(err.message);
    process.exit();
  });

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.withTransaction = async (fn = (_t) => {}) => {
  await db.sequelize.transaction(fn);
};

db.namespace = sequelizeNamespace;

module.exports = db;
