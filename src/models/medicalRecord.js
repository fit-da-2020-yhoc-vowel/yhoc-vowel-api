const { Model, DataTypes } = require('sequelize');
const User = require('./user');

module.exports = (sequelize) => {
  class MedicalRecord extends Model {}
  MedicalRecord.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      patientId: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'patient_id',
        references: {
          model: User(sequelize),
          key: 'id',
        },
      },
      images: {
        type: DataTypes.JSON,
        allowNull: true,
      },
    },
    {
      modelName: 'MedicalRecord',
      tableName: 'medical_record',
      sequelize,
      updatedAt: 'updated_at',
      createdAt: 'created_at',
    },
  );
  return MedicalRecord;
};
