const { Model, DataTypes } = require('sequelize');
const { connectDoctor } = require('@constants');
const User = require('./user');

module.exports = (sequelize) => {
  class ConnectDoctor extends Model {}
  ConnectDoctor.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      patientId: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'patient_id',
        references: {
          model: User(sequelize),
          key: 'id',
        },
      },
      doctorId: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'doctor_id',
        references: {
          model: User(sequelize),
          key: 'id',
        },
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: connectDoctor.WAITING,
        field: 'status',
      },
      invitationOrigin: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: connectDoctor.PATIENT_INVITATION,
        field: 'invitation_origin',
      },
    },
    {
      modelName: 'ConnectDoctor',
      tableName: 'connect_doctor',
      sequelize,
      updatedAt: 'updated_at',
      createdAt: 'created_at',
    },
  );
  return ConnectDoctor;
};
