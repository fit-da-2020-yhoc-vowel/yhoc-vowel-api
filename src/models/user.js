const { DataTypes, Model } = require('sequelize');

module.exports = (sequelize) => {
  class User extends Model {}
  User.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      fullName: {
        type: DataTypes.TEXT('tiny'),
        defaultValue: null,
        field: 'full_name',
      },
      password: {
        type: DataTypes.STRING,
        defaultValue: null,
      },
      email: {
        type: DataTypes.STRING,
        defaultValue: null,
        validate: {
          isEmail: true,
        },
      },
      phone: {
        type: DataTypes.STRING(15),
        allowNull: false,
        unique: true,
        validate: {
          isNumeric: true,
        },
      },
      avatar: {
        type: DataTypes.STRING,
        defaultValue: null,
      },
      birthday: {
        type: DataTypes.DATE,
        defaultValue: null,
      },
      gender: {
        type: DataTypes.INTEGER,
        defaultValue: null,
      },
      role: {
        type: DataTypes.STRING(10),
        defaultValue: null,
      },
      isFullfilled: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        field: 'is_full_filled',
      },
      isCreated: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        field: 'is_created',
      },
      signUpMethod: {
        type: DataTypes.STRING(10),
        defaultValue: null,
        field: 'sign_up_method',
      },
    },
    {
      modelName: 'User',
      tableName: 'user',
      sequelize,
      updatedAt: 'updated_at',
      createdAt: 'created_at',
    },
  );
  return User;
};
