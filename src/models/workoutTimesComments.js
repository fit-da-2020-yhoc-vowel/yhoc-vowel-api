const { Model, DataTypes } = require('sequelize');
const WorkoutTimes = require('./workoutTimes');
const User = require('./user');

module.exports = (sequelize) => {
  class WorkoutTimesComments extends Model {}
  WorkoutTimesComments.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      doctorId: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'doctor_id',
        references: {
          model: User(sequelize),
          key: 'id',
        },
      },
      workoutTimeId: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'workout_time_id',
        references: {
          model: WorkoutTimes(sequelize),
          key: 'id',
        },
      },
      content: {
        type: DataTypes.TEXT,
        defaultValue: null,
        field: 'content',
      },
      isDeleted: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
        field: 'is_deleted',
      },
    },
    {
      modelName: 'WorkoutTimesComments',
      tableName: 'workout_times_comments',
      sequelize,
      updatedAt: 'updated_at',
      createdAt: 'created_at',
    },
  );
  return WorkoutTimesComments;
};
