const { Model, DataTypes } = require('sequelize');
const User = require('./user');
const WorkoutTimes = require('./workoutTimes');

module.exports = (sequelize) => {
  class PathSuggestion extends Model {}
  PathSuggestion.init(
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      patientId: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'patient_id',
        references: {
          model: User(sequelize),
          key: 'id',
        },
      },
      doctorId: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'doctor_id',
        references: {
          model: User(sequelize),
          key: 'id',
        },
      },
      workoutTimesId: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'workout_times_id',
        unique: true,
        references: {
          model: WorkoutTimes(sequelize),
          key: 'id',
        },
      },
      isDeleted: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
        field: 'is_deleted',
      },
    },
    {
      modelName: 'PathSuggestion',
      tableName: 'path_suggestion',
      sequelize,
      updatedAt: 'updated_at',
      createdAt: 'created_at',
    },
  );
  return PathSuggestion;
};
