const { Model, DataTypes } = require('sequelize');
const WorkoutTimes = require('./workoutTimes');
const Exercise = require('./exercise');

module.exports = (sequelize) => {
  class Practice extends Model {}
  Practice.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      workoutTimesId: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'workout_times_id',
        references: {
          model: WorkoutTimes(sequelize),
          key: 'id',
        },
      },
      exerciseId: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'exercise_id',
        references: {
          model: Exercise(sequelize),
          key: 'id',
        },
      },
      score: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          min: 0,
          max: 15,
        },
      },
      audioPath: {
        type: DataTypes.STRING,
        allowNull: false,
        field: 'audio_path',
      },
      analyzedData: {
        type: DataTypes.JSON,
        allowNull: false,
        field: 'analyzed_data',
      },
    },
    {
      modelName: 'Practice',
      tableName: 'practice',
      sequelize,
      updatedAt: 'updated_at',
      createdAt: 'created_at',
    },
  );
  return Practice;
};
