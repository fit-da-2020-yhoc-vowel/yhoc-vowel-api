const { Model, DataTypes, Deferrable } = require('sequelize');
const ExerciseType = require('./exerciseType');

module.exports = (sequelize) => {
  class Exercises extends Model {}
  Exercises.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      exerciseTypeId: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'exercise_type_id',
        references: {
          model: ExerciseType(sequelize),
          key: 'id',
          deferrable: Deferrable.INITIALLY_IMMEDIATE,
        },
      },
      name: {
        type: DataTypes.TEXT('tiny'),
        allowNull: false,
      },
      isDeleted: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false,
        field: 'is_deleted',
      },
    },
    {
      modelName: 'Exercise',
      tableName: 'exercise',
      sequelize,
      updatedAt: 'updated_at',
      createdAt: 'created_at',
    },
  );
  return Exercises;
};
