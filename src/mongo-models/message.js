const mongoose = require('mongoose');

const { Schema } = mongoose;

const Message = new Schema(
  {
    send_id: {
      type: String,
      alias: 'sendId',
    },
    content: {
      type: String,
    },
    is_deleted: {
      type: Boolean,
      default: false,
      alias: 'isDeleted',
    },
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  },
);

module.exports = mongoose.model('Message', Message);
