const mongoose = require('mongoose');

const { Schema } = mongoose;

const User = new Schema({
  user_id: {
    type: String,
    alias: 'userId',
  },
  time_seen: {
    type: Date,
    default: new Date(),
    alias: 'timeSeen',
  },
});

module.exports = mongoose.model('User', User);
