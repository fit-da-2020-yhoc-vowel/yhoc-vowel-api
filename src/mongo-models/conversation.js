const mongoose = require('mongoose');

const { Schema } = mongoose;

const Conversation = new Schema(
  {
    users: [{ type: Object, ref: 'User', default: [] }],
    is_deleted: {
      type: Boolean,
      default: false,
      alias: 'isDeleted',
    },
    // time_seen: {
    //   type: Date,
    //   default: new Date(),
    //   alias: 'timeSeen',
    // },
    messages: [{ type: Object, ref: 'Message', default: [] }],
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  },
);

module.exports = mongoose.model('Conversation', Conversation);
