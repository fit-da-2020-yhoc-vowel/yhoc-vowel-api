const env = process.env.NODE_ENV || 'development';
const config = require('@config/config.json')[env];
const { logger } = require('@config/winston');

// --------------------- MongoDB ---------------------------------------

// const { MongoClient } = require('mongodb');
const mongoose = require('mongoose');

// Replace the following with your Atlas connection string
// const client = new MongoClient(config.mongoUrl);
module.exports.createMongoConnection = function () {
  mongoose
    .connect(config.mongoUrl, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true,
    })
    .then(() => logger.info('Mongoose connected!'))
    .catch((err) => {
      logger.info(err);
      process.exit();
    });
};
