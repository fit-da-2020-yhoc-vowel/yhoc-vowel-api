const { MedicalRecord } = require('@models');
const { QueryTypes } = require('sequelize');
const db = require('@models');
const { role } = require('@constants');

async function findMedicalRecordByPatientId(patientId) {
  const medicalRecord = await db.sequelize.query(
    `SELECT medical_record.* FROM user, medical_record WHERE user.id = '${patientId}' AND user.role = '${role.patient}' AND user.id = medical_record.patient_id`,
    {
      type: QueryTypes.SELECT,
    },
  );
  return medicalRecord ? medicalRecord[0] : medicalRecord;
}

async function findMedicalRecordById(medicalRecordId) {
  const medicalRecord = await MedicalRecord.findByPk(medicalRecordId);
  return medicalRecord;
}

async function createMedicalRecord({ patientId, images = null }) {
  const medicalRecord = await MedicalRecord.create({
    patientId,
    images,
  });
  return medicalRecord;
}

async function updateImages({ id, images }) {
  const [nAffect] = await MedicalRecord.update(
    { images },
    {
      where: {
        id,
      },
    },
  );
  return nAffect;
}

module.exports = {
  findMedicalRecordByPatientId,
  findMedicalRecordById,
  createMedicalRecord,
  updateImages,
};
