const Conversation = require('@mongo-models/conversation');
const Message = require('@mongo-models/message');
const User = require('@mongo-models/user');
const { ObjectId } = require('mongodb');

// get conversation by id,
async function findConversation(id) {
  const conversation = await Conversation.findById(
    {
      _id: ObjectId(id),
    },
    { _id: 1, users: 1, created_at: 1, updated_at: 1, is_deleted: 1 },
  );
  return conversation;
}

async function findConversationByUserIds(userIds) {
  const conversation = await Conversation.aggregate([
    {
      $match: {
        users: { $size: userIds.length },
      },
    },
    {
      $unwind: '$users',
    },
    {
      $match: {
        'users.user_id': { $in: userIds },
      },
    },
    {
      $group: {
        _id: '$_id',
        users: { $push: '$users' },
        created_at: { $first: '$created_at' },
        updated_at: { $first: '$updated_at' },
        is_deleted: { $first: '$is_deleted' },
      },
    },
    {
      $match: {
        users: { $size: userIds.length },
      },
    },
    {
      $project: {
        _id: 1,
        users: 1,
        created_at: 1,
        updated_at: 1,
        is_deleted: 1,
      },
    },
  ]);
  return conversation[0] || null;
}

// async function getTimeSeen(conversationId, userId) {
//   const result = await Conversation.aggregate([
//     {
//       $match: {
//         _id: ObjectId(conversationId),
//       },
//     },
//     {
//       $unwind: '$messages',
//     },
//     {
//       $match: {
//         'messages._id': ObjectId(messageId),
//       },
//     },
//     {
//       $project: {
//         _id: '$messages._id',
//         send_id: '$messages.send_id',
//         content: '$messages.content',
//         is_deleted: '$messages.is_deleted',
//         created_at: '$messages.created_at',
//         updated_at: '$messages.updated_at',
//       },
//     },
//   ]);
//   return result[0] || result;
// }

// get conversations by userId,
async function findConversationsByUserId(userId) {
  const conversations = await Conversation.aggregate([
    {
      $match: {
        'users.user_id': userId,
      },
    },
    {
      $project: {
        _id: 1,
        // users: 1,
        user: {
          $arrayElemAt: [
            {
              $filter: {
                input: '$users',
                as: 'user',
                cond: {
                  $not: { $eq: ['$$user.user_id', userId] },
                },
              },
            },
            0,
          ],
        },
        numberOfNewMessages: {
          $let: {
            vars: {
              user: {
                $arrayElemAt: [
                  {
                    $filter: {
                      input: '$users',
                      as: 'user',
                      cond: {
                        $eq: ['$$user.user_id', userId],
                      },
                    },
                  },
                  0,
                ],
              },
            },
            in: {
              $size: {
                $filter: {
                  input: '$messages',
                  as: 'message',
                  cond: {
                    $and: [
                      {
                        $gt: [
                          '$$message.created_at',

                          '$$user.time_seen',
                        ],
                      },
                      {
                        $not: {
                          $eq: ['$$message.send_id', userId],
                        },
                      },
                      { $eq: ['$$message.is_deleted', false] },
                    ],
                  },
                },
              },
            },
          },
        },
        created_at: 1,
        updated_at: 1,
        is_deleted: 1,
        last_message: {
          $ifNull: [
            {
              $arrayElemAt: [
                {
                  $filter: {
                    input: '$messages',
                    as: 'message',
                    cond: { $eq: ['$$message.is_deleted', false] },
                  },
                },
                -1,
              ],
            },
            null,
          ],
        },
      },
    },
    {
      $sort: {
        // updated_at: -1,
        'last_message.updated_at': -1,
      },
    },
  ]);
  return conversations;
}
// create conversation by array id users
async function createConversation(userIds) {
  const users = userIds.map(
    (userId) => new User({ user_id: userId }),
  );
  const conversation = new Conversation({ users });
  await conversation.save();
  return conversation;
}
// update status is_deleted for conversation
async function updateStatusConversation(id, updateObj) {
  const conversation = await Conversation.updateOne(
    { _id: id },
    { ...updateObj },
  );
  return conversation;
}

async function addNewMessage(id, sendId, content) {
  const message = new Message({
    send_id: sendId,
    content,
    created_at: new Date(),
    updated_at: new Date(),
  });
  await Conversation.updateOne(
    { _id: id },
    { $push: { messages: message } },
  );
  return message;
}

async function deleteMessage(conversationId, messageId) {
  const result = await Conversation.updateOne(
    {
      _id: ObjectId(conversationId),
      'messages._id': ObjectId(messageId),
    },
    {
      $set: {
        'messages.$.is_deleted': true,
      },
    },
  );
  return result;
}

async function updateTimeSeen(conversationId, userId, timeSeen) {
  const result = await Conversation.updateOne(
    {
      _id: ObjectId(conversationId),
      'users.user_id': userId,
    },
    {
      $set: {
        'users.$.time_seen': new Date(timeSeen),
      },
    },
  );
  return result;
}

async function getMessage(conversationId, messageId) {
  const result = await Conversation.aggregate([
    {
      $match: {
        _id: ObjectId(conversationId),
      },
    },
    {
      $unwind: '$messages',
    },
    {
      $match: {
        'messages._id': ObjectId(messageId),
      },
    },
    {
      $project: {
        _id: '$messages._id',
        send_id: '$messages.send_id',
        content: '$messages.content',
        is_deleted: '$messages.is_deleted',
        created_at: '$messages.created_at',
        updated_at: '$messages.updated_at',
      },
    },
  ]);
  return result[0] || null;
}

async function getTimeSeen(conversationId, userId) {
  const result = await Conversation.aggregate([
    {
      $match: {
        _id: ObjectId(conversationId),
      },
    },
    {
      $unwind: '$users',
    },
    {
      $match: {
        'users.user_id': userId,
      },
    },
    {
      $project: {
        user_id: '$users.user_id',
        time_seen: '$users.time_seen',
      },
    },
  ]);
  return result[0] || null;
}

async function getMessagesByConversationId(id, page, limit) {
  const conversation = await Conversation.aggregate([
    {
      $match: {
        _id: ObjectId(id),
      },
    },
    {
      $unwind: {
        path: '$messages',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $match: {
        $or: [
          { 'messages.is_deleted': null },
          { 'messages.is_deleted': false },
        ],
      },
    },
    {
      $sort: { 'messages.created_at': -1 },
    },
    {
      $skip: (page - 1) * limit,
    },
    {
      $limit: limit,
    },
    {
      $group: {
        _id: '$_id',
        users: { $first: '$users' },
        created_at: { $first: '$created_at' },
        updated_at: { $first: '$updated_at' },
        messages: { $push: '$messages' },
      },
    },
    {
      $project: {
        _id: 1,
        users: 1,
        messages: 1,
        created_at: 1,
        updated_at: 1,
      },
    },
  ]);
  return conversation[0] || null;
}

// async function getMessagesByUserIds(userIds, page, limit) {
//   const conversation = await Conversation.aggregate([
//     {
//       $match: {
//         users: { $size: userIds.length },
//       },
//     },
//     {
//       $unwind: '$users',
//     },
//     {
//       $match: {
//         'users.user_id': { $in: userIds },
//       },
//     },
//     {
//       $group: {
//         _id: '$_id',
//         users: { $push: '$users' },
//         created_at: { $first: '$created_at' },
//         updated_at: { $first: '$updated_at' },
//         messages: { $first: '$messages' },
//       },
//     },
//     {
//       $match: {
//         users: { $size: userIds.length },
//       },
//     },
//     {
//       $unwind: {
//         path: '$messages',
//         preserveNullAndEmptyArrays: true,
//       },
//     },
//     {
//       $match: {
//         $or: [
//           { 'messages.is_deleted': null },
//           { 'messages.is_deleted': false },
//         ],
//       },
//     },
//     {
//       $sort: { 'messages.created_at': -1 },
//     },
//     {
//       $group: {
//         _id: '$_id',
//         users: { $first: '$users' },
//         created_at: { $first: '$created_at' },
//         updated_at: { $first: '$updated_at' },
//         messages: { $push: '$messages' },
//       },
//     },
//     {
//       $project: {
//         _id: 1,
//         users: 1,
//         messages: {
//           $slice: ['$messages', (page - 1) * limit, limit * 1],
//         },
//         created_at: 1,
//         updated_at: 1,
//       },
//     },
//   ]);
//   return conversation[0] || null;
// }

module.exports = {
  findConversationsByUserId,
  createConversation,
  updateStatusConversation,
  findConversation,
  addNewMessage,
  getMessagesByConversationId,
  deleteMessage,
  getMessage,
  updateTimeSeen,
  getTimeSeen,
  findConversationByUserIds,
};
