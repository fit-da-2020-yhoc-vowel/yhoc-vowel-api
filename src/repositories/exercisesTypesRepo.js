// const { status } = require('@constants');
const { ExerciseType } = require('@models');
const { QueryTypes } = require('sequelize');
const db = require('@models');

async function findById(exerciseTypeId) {
  const exerciseType = await ExerciseType.findByPk(exerciseTypeId);
  return exerciseType;
}
async function findExerciseTypesByCateID(cateId) {
  const exerciseTypes = await db.sequelize.query(
    'SELECT exType.* FROM `exercise_type` as exType, `category` as cate WHERE exType.category_id= ? and exType.category_id=cate.id and exType.is_deleted= 0 and cate.is_deleted=0 order by created_at asc',
    {
      replacements: [cateId],
      type: QueryTypes.SELECT,
    },
  );
  return exerciseTypes;
}
async function createExerciseType({ categoryId, name }) {
  const exerciseTypes = await ExerciseType.create({
    categoryId,
    name,
  });
  return exerciseTypes;
}
async function findExerciseTypesByName(categoryId, name) {
  const exerciseTypes = await ExerciseType.findAll({
    where: {
      categoryId,
      name,
      isDeleted: false,
    },
  });
  return exerciseTypes;
}
module.exports = {
  findExerciseTypesByCateID,
  createExerciseType,
  findExerciseTypesByName,
  findById,
};
