const { DoctorInformation } = require('@models');

async function addInformation(doctorId, hospitalId) {
  const infor = await DoctorInformation.create({
    doctorId,
    hospitalId,
  });
  return infor;
}

async function updateInformation(doctorId, hospitalId, address) {
  const updateObj = {};
  if (hospitalId) {
    updateObj.hospitalId = hospitalId;
  }
  if (address != null) {
    updateObj.address = address;
  }
  const [nAffect] = await DoctorInformation.update(updateObj, {
    where: {
      doctorId,
    },
  });

  return nAffect;
}

module.exports = {
  addInformation,
  updateInformation,
};
