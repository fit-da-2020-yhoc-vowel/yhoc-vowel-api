const { isArray } = require('lodash');
const camelcaseKeys = require('camelcase-keys');
const { QueryTypes, Op } = require('sequelize');
const { WorkoutTimes } = require('@models');
const db = require('@models');
const {
  workoutType: systemWorkoutType,
  role: systemRole,
} = require('@constants');
const { range: scoreRange } = require('@constants/practice');

async function createWorkoutTimes({
  patientId,
  exerciseTypeId = null,
  exerciseList = [],
  workoutType = systemWorkoutType.PRACTICE,
}) {
  const workoutTimes = await WorkoutTimes.create({
    patientId,
    exerciseTypeId,
    exerciseList,
    workoutType,
  });
  return workoutTimes;
}

async function updateWorkoutTimesScore(
  workoutTimesId,
  score,
  analyzedData,
) {
  const [nAffect] = await WorkoutTimes.update(
    {
      score,
      analyzedData,
    },
    {
      where: {
        id: workoutTimesId,
      },
    },
  );
  return nAffect >= 1;
}

async function findWorkoutTimesById(workoutTimesId) {
  const workoutTimes = await WorkoutTimes.findByPk(workoutTimesId);
  return workoutTimes;
}

async function findWorkoutTimesListInMonth({
  patientId,
  month,
  year,
  range,
  page = 1,
  limit = 10,
  workoutTypes = [
    systemWorkoutType.PRACTICE,
    systemWorkoutType.CHECKING,
  ],
}) {
  const firstDate = new Date(year, month - 1, 1);
  // console.log('firstDate', firstDate.toDateString());
  const lastDate = new Date(
    firstDate.getFullYear(),
    firstDate.getMonth() + 1,
    0,
  );

  let workoutTypeQuery = workoutTypes.reduce((query, wk, idx) => {
    let ret = query;
    if (idx === 0) {
      ret += `workout_type = '${wk}'`;
    } else {
      ret += ` OR workout_type = '${wk}'`;
    }
    return ret;
  }, '');

  if (workoutTypeQuery) {
    workoutTypeQuery = `AND (${workoutTypeQuery})`;
  }

  let workoutTimesList = await db.sequelize.query(
    `SELECT wk.*, et.name, (SELECT COUNT(wk_comment.id) FROM workout_times_comments wk_comment WHERE wk_comment.workout_time_id = wk.id ) AS num_of_comments FROM workout_times AS wk LEFT JOIN exercise_type AS et ON wk.exercise_type_id = et.id LEFT JOIN user ON wk.patient_id = user.id WHERE user.id = '${patientId}' AND user.role = '${
      systemRole.patient
    }' AND wk.score IS NOT NULL AND wk.score BETWEEN ${
      range[0]
    } AND ${
      range[1]
    } AND wk.updated_at between '${year}/${month}/${firstDate.getDate()}' AND '${year}/${month}/${lastDate.getDate()}' ${workoutTypeQuery} ORDER BY wk.updated_at DESC LIMIT ${limit} OFFSET ${
      (page - 1) * limit
    } `,
    {
      type: QueryTypes.SELECT,
    },
  );

  workoutTimesList = camelcaseKeys(workoutTimesList, { deep: true });
  return workoutTimesList;
}

async function findWorkoutTimesInDate({
  patientId,
  date,
  range = scoreRange.ALL,
  page,
  limit,
  workoutType = systemWorkoutType.PRACTICE,
}) {
  const beginOfDate = new Date(date);
  const endOfDate = new Date(date);
  beginOfDate.setHours(0, 0, 0, 0);
  endOfDate.setHours(23, 59, 59, 999);
  const workoutTimes = await WorkoutTimes.findAll({
    where: {
      patientId,
      score: {
        [Op.between]: [range[0], range[1]],
        // [Op.or]: {
        //   [Op.eq]: null,
        // },
      },
      workoutType,
      created_at: {
        [Op.between]: [beginOfDate, endOfDate],
      },
    },
    limit,
    offset: (page - 1) * limit,
  });

  return workoutTimes;
}

async function getWorkoutTimesList({
  patientId,
  limit = 10,
  page = 1,
  workoutType = null,
  isFinished = null,
}) {
  const rest = {};
  if (Object.values(systemWorkoutType).includes(workoutType)) {
    rest.workoutType = workoutType;
  }
  if (isFinished === true) {
    rest.score = { [Op.not]: null };
  }
  if (isFinished === false) {
    rest.score = { [Op.is]: null };
  }
  const workoutTimesList = await WorkoutTimes.findAll({
    where: {
      patientId,
      ...rest,
    },
    order: [['updated_at', 'DESC']],
    limit,
    offset: (page - 1) * limit,
  });

  return workoutTimesList.map((val) => val.toJSON());
}

async function getLastedWorkoutTimes(
  patientId,
  workoutType = systemWorkoutType.PRACTICE,
  isFinished = true,
) {
  const lastedWorkoutTimes = await getWorkoutTimesList({
    patientId,
    limit: 1,
    workoutType,
    isFinished,
  });

  if (lastedWorkoutTimes && lastedWorkoutTimes.length === 0) {
    return null;
  }

  return lastedWorkoutTimes[0];
}

async function findLastedFinishedWorkoutsByCategoryId(
  patientId,
  categoryId,
) {
  let workoutTimesList = await db.sequelize.query(
    `
  SELECT wk.* FROM workout_times AS wk
  LEFT JOIN exercise_type et ON wk.exercise_type_id = et.id
  WHERE wk.id = (SELECT wk2.id
                 FROM workout_times wk2
                 WHERE wk2.exercise_type_id = wk.exercise_type_id
                 ORDER BY wk2.updated_at DESC
                 LIMIT 1 OFFSET 0)
    AND et.category_id = '${categoryId}'
    AND wk.score IS NOT NULL
    AND wk.workout_type = '${systemWorkoutType.PRACTICE}'
    AND wk.patient_id = '${patientId}'
  `,
    { type: QueryTypes.SELECT },
  );

  workoutTimesList = camelcaseKeys(workoutTimesList);

  return workoutTimesList;
}

async function getCurrentWorkoutPath(patientId) {
  const workout = await db.sequelize.query(
    `
  SELECT wk.*
  FROM (selecting_path AS sp INNER JOIN path_suggestion ps ON sp.current_path = ps.id)
          INNER JOIN workout_times AS wk ON wk.id = ps.workout_times_id
  WHERE sp.patient_id = '${patientId}'
    AND wk.workout_type = '${systemWorkoutType.PATHS}'
  `,
    { type: QueryTypes.SELECT },
  );

  if (!isArray(workout)) {
    return null;
  }

  return camelcaseKeys(workout[0], { deep: true });
}

async function countWorkout({ patientId, isFinished = true }) {
  const rest = {};
  if (isFinished) {
    rest.score = {
      [Op.not]: null,
    };
  }
  const total = await WorkoutTimes.count({
    where: {
      patientId,
      ...rest,
    },
  });

  return total;
}

async function getAllWorkout({ patientId, limit, page }) {
  const list = await db.sequelize.query(
    `
    SELECT wk.*, et.name AS exercise_type_name, u.id AS doctor_id, u.full_name AS doctor_name
    FROM (workout_times AS wk LEFT JOIN exercise_type et ON wk.exercise_type_id = et.id)
            LEFT JOIN path_suggestion ps ON wk.id = ps.workout_times_id
            LEFT JOIN user u ON ps.doctor_id = u.id
    WHERE wk.patient_id = '${patientId}'
    ORDER BY wk.updated_at DESC
    LIMIT ${limit}
    OFFSET ${(page - 1) * limit}
  `,
    {
      type: QueryTypes.SELECT,
    },
  );

  return camelcaseKeys(list, { deep: true });
}

module.exports = {
  createWorkoutTimes,
  updateWorkoutTimesScore,
  findWorkoutTimesById,
  findWorkoutTimesListInMonth,
  findWorkoutTimesInDate,
  getLastedWorkoutTimes,
  findLastedFinishedWorkoutsByCategoryId,
  getWorkoutTimesList,
  getCurrentWorkoutPath,
  countWorkout,
  getAllWorkout,
};
