const { Hospital } = require('@models');

async function getAllHospitals() {
  const hospitals = await Hospital.findAll({
    where: { status: false },
    order: [['name', 'ASC']],
  });
  return hospitals;
}

async function createNewHospital(name, address) {
  const hospital = await Hospital.create({
    name,
    address,
  });
  return hospital;
}

async function findHospitalByName(name) {
  const hospital = await Hospital.findOne({ where: { name } });
  return hospital;
}
async function findHospitalById(id) {
  const hospital = await Hospital.findByPk(id);
  return hospital;
}
module.exports = {
  createNewHospital,
  findHospitalByName,
  findHospitalById,
  getAllHospitals,
};
