const { User } = require('@models');
const { signUpMethod, role } = require('@constants');
const { QueryTypes, Op } = require('sequelize');
const db = require('@models');

async function findUserByPhone(phoneNumber) {
  const user = await User.findOne({ where: { phone: phoneNumber } });
  return user;
}
async function findUserByEmail(emailUser) {
  const user = await User.findOne({ where: { email: emailUser } });
  return user;
}
async function createUserWithPhone(phoneNumber) {
  const user = await User.create({
    phone: phoneNumber,
    role: role.patient,
    signUpMethod: signUpMethod.phone,
  });
  return user;
}
async function createAccountDoctor(email, fullName, phone, password) {
  const user = await User.create({
    email,
    fullName,
    password,
    phone,
    role: role.doctor,
    signUpMethod: signUpMethod.email,
    isCreated: true,
  });
  return user;
}

async function findUserById(id) {
  const user = await User.findByPk(id);
  return user;
}

async function findUserByIdAndPhone(id, phoneNumber) {
  const user = await User.findOne({
    where: { id, phone: phoneNumber },
  });
  return user;
}

async function updatePassword(id, password) {
  const [nAffect] = await User.update(
    { password, isCreated: true },
    {
      where: {
        id,
      },
    },
  );

  return nAffect;
}

async function updateUserInfo({
  id,
  fullName,
  birthday,
  phone,
  email,
  gender,
  avatar,
  extraFields = {},
}) {
  const updateObj = {};
  if (fullName) {
    updateObj.fullName = fullName;
  }
  if (birthday) {
    updateObj.birthday = birthday;
  }
  if (phone) {
    updateObj.phone = phone.number;
  }
  if (email) {
    updateObj.email = email;
  }
  if (Number.isInteger(gender)) {
    updateObj.gender = gender;
  }
  if (avatar) {
    updateObj.avatar = avatar;
  }

  const [nAffect] = await User.update(
    { ...updateObj, ...extraFields },
    {
      where: {
        id,
      },
    },
  );
  return nAffect;
}

async function getUserInfoById(userId) {
  const user = await db.sequelize.query(
    `SELECT user.id, user.full_name, user.avatar, user.phone, user.email, user.birthday, user.gender, user.role, doctor_information.address, 
    hospital.id AS hospital_id, hospital.name AS hospital_name FROM user LEFT JOIN doctor_information 
    ON user.id = doctor_information.doctor_id LEFT JOIN hospital ON doctor_information.hospital_id = hospital.id 
    WHERE user.id = ?`,
    { replacements: [userId], type: QueryTypes.SELECT },
  );
  return user ? user[0] : user;
}

async function getUsers(userRole = '', page, limit) {
  let filterRole = '';
  if (userRole) {
    filterRole = `AND user.role='${userRole}'`;
  }
  const result = await db.sequelize.query(
    `SELECT user.id, user.avatar, user.full_name, user.birthday, user.phone, user.email, user.gender, user.role, doctor_information.hospital_id, hospital.name AS hospital_name, doctor_information.address
    FROM user LEFT JOIN doctor_information ON user.id = doctor_information.doctor_id LEFT JOIN hospital ON doctor_information.hospital_id = hospital.id 
    WHERE user.role!='${
      role.admin
    }' ${filterRole} ORDER BY user.created_at DESC LIMIT ${limit} OFFSET ${
      limit * (page - 1)
    }`,
    {
      type: QueryTypes.SELECT,
    },
  );
  return result;
}

async function countUsers(userRole = '') {
  let filterRole = '';
  if (userRole) {
    filterRole = `AND user.role='${userRole}'`;
  }
  const result = await db.sequelize.query(
    `SELECT count(user.id) as total_user
    FROM user 
    WHERE user.role!='${role.admin}' ${filterRole}`,
    {
      type: QueryTypes.SELECT,
    },
  );
  return result ? result[0] : result;
}

async function searchUserByName(name, userRole = '', page, limit) {
  let filterRole = '';
  if (userRole) {
    filterRole = `AND user.role='${userRole}' `;
  }
  const result = await db.sequelize.query(
    `SELECT user.id, user.avatar, user.full_name, user.birthday, user.phone, user.email, user.gender, user.role, doctor_information.hospital_id, hospital.name AS hospital_name, doctor_information.address
    FROM user LEFT JOIN doctor_information ON user.id = doctor_information.doctor_id LEFT JOIN hospital ON doctor_information.hospital_id = hospital.id 
    WHERE user.role!='${
      role.admin
    }' ${filterRole}AND user.full_name LIKE '%${name}%' LIMIT ${limit} OFFSET ${
      limit * (page - 1)
    }`,
    {
      type: QueryTypes.SELECT,
    },
  );
  return result;
}

async function countSearchUserResult(name, userRole = '') {
  let filterRole = '';
  if (userRole) {
    filterRole = `user.role='${userRole}' AND `;
  }
  const result = await db.sequelize.query(
    `SELECT count(user.id) as total_user
    FROM user 
    WHERE user.role!='${role.admin}' AND ${filterRole}user.full_name LIKE '%${name}%'`,
    {
      type: QueryTypes.SELECT,
    },
  );
  return result ? result[0] : result;
}

async function getUsersByIds(userIds = []) {
  const users = await User.findAll({
    where: {
      id: {
        [Op.in]: userIds,
      },
    },
  });

  return users;
}

module.exports = {
  findUserByPhone,
  findUserByEmail,
  createUserWithPhone,
  createAccountDoctor,
  findUserById,
  findUserByIdAndPhone,
  updatePassword,
  updateUserInfo,
  getUserInfoById,
  getUsers,
  countUsers,
  searchUserByName,
  countSearchUserResult,
  getUsersByIds,
};
