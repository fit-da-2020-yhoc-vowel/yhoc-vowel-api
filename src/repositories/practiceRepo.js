const db = require('@models');
const { Practice } = require('@models');
const { QueryTypes } = require('sequelize');
const camelcaseKeys = require('camelcase-keys');

async function getPracticeById(practiceId) {
  const practice = await Practice.findByPk(practiceId);
  return practice;
}

async function createNewPractice({
  workoutTimesId,
  exerciseId,
  score,
  analyzedData,
  audioPath,
}) {
  const practice = await Practice.create({
    workoutTimesId,
    exerciseId,
    score,
    audioPath,
    analyzedData,
  });

  return practice;
}

async function findPracticeByWorkoutTimesAndExercise(
  workoutTimesId,
  exerciseId,
) {
  const practice = await Practice.findOne({
    where: {
      workoutTimesId,
      exerciseId,
    },
  });
  return practice;
}

async function updatePractice(practice) {
  const [, nAffect] = await Practice.update(
    {
      score: practice.score,
      audioPath: practice.audioPath,
      analyzedData: practice.analyzedData,
    },
    {
      where: {
        workoutTimesId: practice.workoutTimesId,
        exerciseId: practice.exerciseId,
      },
    },
  );
  return nAffect >= 1;
}

async function getPracticesInWorkoutTimes(workoutTimesId) {
  // const practices = await Practice.findAll({
  //   where: {
  //     workoutTimesId,
  //   },
  // });

  const practices = await db.sequelize.query(
    `
  SELECT ptc.*, e.name AS exercise_name FROM practice AS ptc LEFT JOIN exercise e on ptc.exercise_id = e.id WHERE ptc.workout_times_id='${workoutTimesId}'
  `,
    {
      type: QueryTypes.SELECT,
    },
  );

  return camelcaseKeys(practices, { deep: true });
}

module.exports = {
  getPracticeById,
  createNewPractice,
  findPracticeByWorkoutTimesAndExercise,
  updatePractice,
  getPracticesInWorkoutTimes,
};
