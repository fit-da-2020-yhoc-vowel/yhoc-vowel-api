const { QueryTypes } = require('sequelize');
const camelCase = require('camelcase-keys');
const { ConnectDoctor } = require('@models');
const db = require('@models');
const { role } = require('@constants');
// const userRepo = require('./userRepo');

async function findConnectionById(connectionId) {
  const result = await ConnectDoctor.findByPk(connectionId);
  return result;
}

async function findConnectionByPatientIdAndDoctorId(
  patientId,
  doctorId,
) {
  const result = await ConnectDoctor.findOne({
    where: { patientId, doctorId },
  });
  return result;
}

async function findConnectionsByUserId(
  userId,
  userRole,
  status = '',
  invitationOrigin = '',
) {
  let filterStatus = '';
  let filterInvitationOrigin = '';
  let query;
  if (status) {
    filterStatus = `and conn.status='${status}' `;
  }
  if (invitationOrigin) {
    filterInvitationOrigin = `and conn.invitation_origin='${invitationOrigin}' `;
  }
  if (userRole === role.doctor) {
    query = `SELECT conn.id, conn.patient_id, conn.doctor_id, user.full_name, user.avatar, user.birthday, user.gender, conn.status, 
    conn.invitation_origin, conn.created_at, conn.updated_at FROM user , connect_doctor as conn 
    WHERE conn.doctor_id= ? ${filterStatus}${filterInvitationOrigin}and conn.patient_id = user.id 
    order by conn.updated_at desc`;
  } else {
    query = `SELECT conn.id, conn.patient_id, conn.doctor_id, user.full_name, user.avatar, user.birthday, user.gender,
    doctor_information.hospital_id, hospital.name AS hospital_name, doctor_information.address, conn.status, conn.invitation_origin, 
    conn.created_at, conn.updated_at FROM user LEFT JOIN doctor_information ON user.id = doctor_information.doctor_id 
    LEFT JOIN hospital ON doctor_information.hospital_id = hospital.id , connect_doctor as conn 
    WHERE conn.patient_id= ? ${filterStatus}${filterInvitationOrigin}and conn.doctor_id = user.id 
    order by conn.updated_at desc`;
  }
  const result = await db.sequelize.query(query, {
    replacements: [userId],
    type: QueryTypes.SELECT,
  });
  return result;
}

async function updateConnection(
  connectionId,
  status,
  extraFields = {},
) {
  const [nAffect] = await ConnectDoctor.update(
    { status, ...extraFields },
    {
      where: {
        id: connectionId,
      },
    },
  );
  return nAffect;
}

async function createConnection(
  patientId,
  doctorId,
  invitationOrigin,
) {
  const result = await ConnectDoctor.create({
    invitationOrigin,
    patientId,
    doctorId,
  });
  return result;
}

async function findDoctorByName(patientId, doctorName) {
  const result = await db.sequelize.query(
    `SELECT user.id as doctor_id, user.avatar, user.full_name, user.birthday, doctor_information.hospital_id, hospital.name AS hospital_name, 
    doctor_information.address, connect_doctor.id, connect_doctor.status, connect_doctor.invitation_origin 
    FROM user LEFT JOIN connect_doctor ON user.id = connect_doctor.doctor_id AND connect_doctor.patient_id = '${patientId}' 
    LEFT JOIN doctor_information ON user.id = doctor_information.doctor_id LEFT JOIN hospital ON doctor_information.hospital_id = hospital.id 
    WHERE user.role = '${role.doctor}' AND user.full_name LIKE '%${doctorName}%'`,
    {
      type: QueryTypes.SELECT,
    },
  );
  return result;
}

async function findPatientByName(
  doctorId,
  patientName,
  page = 1,
  limit = 20,
) {
  const result = await db.sequelize.query(
    `SELECT user.id as patient_id, user.avatar, user.full_name, user.birthday, user.gender, connect_doctor.id, connect_doctor.status, connect_doctor.invitation_origin 
      FROM user LEFT JOIN connect_doctor ON user.id = connect_doctor.patient_id AND connect_doctor.doctor_id = '${doctorId}' 
      WHERE user.role = '${
        role.patient
      }' AND user.full_name LIKE '%${patientName}%' LIMIT ${limit} OFFSET ${
      limit * (page - 1)
    }`,
    {
      type: QueryTypes.SELECT,
    },
  );
  return result;
}

async function getPatientProfile({
  doctorId,
  patientId,
  // connectionStatus = connectDoctor.ACCEPTED,
}) {
  const patient = await db.sequelize.query(
    `
  SELECT u2.*, mr.images AS medical_record
  FROM (SELECT u.*
        FROM connect_doctor AS cd
                 LEFT JOIN user AS u ON cd.patient_id = u.id
        WHERE cd.doctor_id = '${doctorId}'
          AND u.role = '${role.patient}'
          AND u.id = '${patientId}'
        LIMIT 1 OFFSET 0) AS u2
           LEFT JOIN medical_record AS mr ON u2.id = mr.patient_id
  `,
    {
      type: QueryTypes.SELECT,
    },
  );

  if (patient && patient.length > 0) {
    let tmp = camelCase(patient, { deep: true });
    [tmp] = tmp;

    return tmp;
  }
  return null;
}

async function getSingleConnection({ doctorId, patientId }) {
  const connection = await ConnectDoctor.findOne({
    where: {
      doctorId,
      patientId,
    },
  });

  if (!connection) {
    return null;
  }

  return connection.toJSON();
}

async function countConnections({
  userId,
  userRole,
  connectionStatus,
  invitationOrigin,
}) {
  const queryObject = {};
  switch (userRole) {
    case role.patient:
      queryObject.patientId = userId;
      break;
    case role.doctor:
      queryObject.doctorId = userId;
      break;
    default:
  }

  if (connectionStatus) {
    queryObject.status = connectionStatus;
  }

  if (invitationOrigin) {
    queryObject.invitationOrigin = invitationOrigin;
  }

  const connections = await ConnectDoctor.count({
    where: queryObject,
    order: [['updated_at', 'DESC']],
  });

  return connections;
}

module.exports = {
  findConnectionById,
  findConnectionsByUserId,
  updateConnection,
  createConnection,
  findDoctorByName,
  findConnectionByPatientIdAndDoctorId,
  findPatientByName,
  getPatientProfile,
  getSingleConnection,
  countConnections,
};
