const { QueryTypes } = require('sequelize');
const camelcaseKeys = require('camelcase-keys');
const db = require('@models');
const { SelectingPath } = require('@models');
const { isArray } = require('lodash');

async function createSelectingPath(patientId) {
  const selectingPath = await SelectingPath.create({
    patientId,
  });

  return selectingPath;
}

async function getSelectingPathOfPatient(patientId) {
  const path = await db.sequelize.query(
    `
    SELECT ps.*, sp.id AS selecting_path_id
    FROM selecting_path AS sp
             LEFT JOIN path_suggestion ps ON sp.current_path = ps.id
    WHERE sp.patient_id = '${patientId}'
  `,
    {
      type: QueryTypes.SELECT,
    },
  );
  const temp = camelcaseKeys(path, { deep: true });

  if (!isArray(temp)) return null;

  return temp.length > 0 ? temp[0] : null;
}

async function updateCurrentPath(patientId, currentPathId) {
  const [, nAffected] = await SelectingPath.update(
    { currentPath: currentPathId },
    {
      where: {
        patientId,
      },
    },
  );
  return nAffected > 0;
}

module.exports = {
  createSelectingPath,
  getSelectingPathOfPatient,
  updateCurrentPath,
};
