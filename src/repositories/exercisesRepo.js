const { Exercise } = require('@models');
const { QueryTypes, Op } = require('sequelize');
const db = require('@models');
const { NUMBER_OF_CHECKING_EXERCISE } = require('@constants');

async function getAll() {
  const exercises = await Exercise.findAll({
    where: {
      isDeleted: false,
    },
    order: [['created_at', 'ASC']],
  });

  return exercises;
}

async function findExercisesByTypeID(exerciseTypeId) {
  const exercise = await db.sequelize.query(
    'SELECT ex.* FROM `exercise` as ex,`exercise_type` as exType, `category` as cate WHERE ex.is_deleted=0 and ex.exercise_type_id = ? and ex.exercise_type_id = exType.id and exType.category_id=cate.id and exType.is_deleted= 0 and cate.is_deleted=0 order by created_at asc',
    {
      replacements: [exerciseTypeId],
      type: QueryTypes.SELECT,
    },
  );
  return exercise;
}
async function findExerciseByID(exerciseId) {
  const exercise = await Exercise.findByPk(exerciseId);
  return exercise;
}
async function createExercise({ name, exerciseTypeId }) {
  const exerciseTypes = await Exercise.create({
    exerciseTypeId,
    name,
  });
  return exerciseTypes;
}
async function findExercisesByName(exerciseTypeId, name) {
  const exercises = await Exercise.findAll({
    where: {
      exerciseTypeId,
      name: {
        [Op.like]: name,
      },
      isDeleted: false,
    },
  });
  return exercises;
}

async function generateRandomExercise(
  limit = NUMBER_OF_CHECKING_EXERCISE,
) {
  const exercises = await Exercise.findAll({
    order: db.sequelize.random(),
    limit,
  });
  return exercises;
}

async function getCategoryByExerciseIds(exerciseIds) {
  const query = exerciseIds.reduce((acc, val, idx) => {
    let tmp = acc;
    if (idx === 0) {
      tmp += `ex.id = '${val}'`;
    } else {
      tmp += ` OR ex.id = '${val}'`;
    }
    return tmp;
  }, '');

  const exerciseWithCategory = await db.sequelize.query(
    `
    SELECT ex.id AS exerciseId, ex.name as exerciseName, cate.id AS categoryId, cate.name AS categoryName FROM exercise AS ex LEFT JOIN exercise_type AS et ON ex.exercise_type_id = et.id  LEFT JOIN category AS cate ON et.category_id = cate.id WHERE ${query}
  `,
    {
      type: QueryTypes.SELECT,
    },
  );

  return exerciseWithCategory;
}

async function findExercisesWithListId(exerciseIds = []) {
  const list = await Exercise.findAll({
    where: {
      id: {
        [Op.in]: exerciseIds,
      },
      isDeleted: false,
    },
  });

  return list;
}

module.exports = {
  findExercisesByTypeID,
  findExerciseByID,
  createExercise,
  findExercisesByName,
  generateRandomExercise,
  getCategoryByExerciseIds,
  findExercisesWithListId,
  getAll,
};
