const { isArray } = require('lodash');
const { QueryTypes } = require('sequelize');
const camelcaseKeys = require('camelcase-keys');
const db = require('@models');
const { PathSuggestion } = require('@models');
const {
  workoutType: systemWorkoutType,
  role: systemRole,
} = require('@constants');

async function findPathSuggestion({ patientId, doctorId }) {
  const paths = await db.sequelize.query(
    `
    SELECT ps.id AS path_id, ps.doctor_id AS doctor_id, wk.* FROM path_suggestion AS ps LEFT JOIN workout_times AS wk ON ps.workout_times_id = wk.id WHERE ps.doctor_id = ? AND ps.patient_id = ? AND wk.workout_type = '${systemWorkoutType.PATHS}' ORDER BY ps.updated_at DESC
  `,
    {
      type: QueryTypes.SELECT,
      replacements: [doctorId, patientId],
    },
  );

  return camelcaseKeys(paths, { deep: true });
}

async function findPathsOfPatient(patientId, isCompleted = null) {
  let query = '';
  switch (isCompleted) {
    case true:
      query = 'AND wk.score IS NOT NULL';
      break;
    case false:
      query = 'AND wk.score IS NULL';
      break;
    default:
      query = '';
  }
  const paths = await db.sequelize.query(
    `
  SELECT ps.id AS path_id, ps.doctor_id AS doctor_id, u.full_name AS doctor_name, 
    wk.*, IF (sp.current_path IS NULL, FALSE, TRUE) AS is_selecting
  FROM (
    (path_suggestion AS ps LEFT JOIN workout_times AS wk ON ps.workout_times_id = wk.id) 
    LEFT JOIN user AS u ON u.id = ps.doctor_id) 
    LEFT JOIN selecting_path AS sp ON sp.current_path = ps.id
  WHERE ps.patient_id = '${patientId}'
    AND wk.workout_type = '${systemWorkoutType.PATHS}' 
    AND u.role = '${systemRole.doctor}'
    ${query} ORDER BY ps.updated_at DESC
  `,
    {
      type: QueryTypes.SELECT,
    },
  );

  return camelcaseKeys(paths, { deep: true });
}

async function createNewPathSuggestion({
  patientId,
  doctorId,
  workoutTimesId,
}) {
  const newPath = await PathSuggestion.create({
    patientId,
    doctorId,
    workoutTimesId,
  });

  return newPath;
}

async function getSelectingPath(patientId) {
  const paths = await db.sequelize.query(
    `
  SELECT wk.*, ps.id AS path_id, ps.doctor_id AS doctor_id
  FROM (selecting_path AS sp JOIN path_suggestion AS ps ON sp.current_path = ps.id)
           JOIN workout_times AS wk ON ps.workout_times_id = wk.id
           WHERE sp.patient_id = '${patientId}'
  `,
    {
      type: QueryTypes.SELECT,
    },
  );

  if (!isArray(paths)) {
    return null;
  }

  return camelcaseKeys(paths[0], { deep: true });
}

module.exports = {
  findPathSuggestion,
  createNewPathSuggestion,
  findPathsOfPatient,
  getSelectingPath,
};
