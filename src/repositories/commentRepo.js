const { WorkoutTimesComments } = require('@models');
const { QueryTypes } = require('sequelize');
const db = require('@models');

async function findCommentById(id) {
  const comment = await WorkoutTimesComments.findOne({
    where: { id, isDeleted: false },
  });
  return comment;
}

async function createNewComment(workoutTimeId, doctorId, content) {
  const comment = await WorkoutTimesComments.create({
    workoutTimeId,
    doctorId,
    content,
  });
  return comment;
}
async function deleteComment(commentId) {
  const [nAffect] = await WorkoutTimesComments.update(
    { isDeleted: true },
    {
      where: {
        id: commentId,
      },
    },
  );
  return nAffect;
}
async function editComment(commentId, content) {
  const [nAffect] = await WorkoutTimesComments.update(
    { content },
    {
      where: {
        id: commentId,
      },
    },
  );
  return nAffect;
}
async function getCommentByWorkoutTimeId(workoutTimeId) {
  const comment = await db.sequelize.query(
    `SELECT user.id, user.full_name, user.avatar, comment.id as commentId, comment.created_at, comment.content
     FROM user, workout_times_comments as comment WHERE comment.workout_time_id= ? and comment.doctor_id = user.id
      and comment.is_deleted = '0' order by comment.updated_at desc`,
    {
      replacements: [workoutTimeId],
      type: QueryTypes.SELECT,
    },
  );
  return comment;
}
module.exports = {
  createNewComment,
  deleteComment,
  editComment,
  findCommentById,
  getCommentByWorkoutTimeId,
};
