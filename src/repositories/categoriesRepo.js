/* eslint-disable prettier/prettier */
const { Category } = require('@models');

async function findAllCategories() {
  const categories = await Category.findAll({
    where: { isDeleted: false },
    order: [['created_at', 'ASC']],
  });
  return categories;
}
async function findCategoryByID(id) {
  const categories = await Category.findOne({
    where: {
      id,
      isDeleted: false,
    },
  });
  return categories;
}
async function createCategory(Cate) {
  const category = await Category.create({
    name: Cate.name,
    thumbnail: Cate.thumbnail,
  });
  return category;
}
async function findCategoryByName(nameCate) {
  let category = null;
  category = await Category.findOne({
    where: {
      name: nameCate,
      isDeleted: false,
    },
  });
  return category;
}
async function deleteCategory(idCategory) {
  const updateObj = {};
  updateObj.isDeleted = true;
  const [nAffect] = await Category.update(
    { ...updateObj },
    {
      where: {
        id: idCategory,
      },
    },
  );
  return nAffect;
}
module.exports = {
  findAllCategories,
  createCategory,
  findCategoryByName,
  deleteCategory,
  findCategoryByID,
};
