const errorHandler = require('./errorHandler');
const validators = require('./validator');
const helpers = require('./helpers');
const token = require('./token');
const redis = require('./redis');
const customJoi = require('./customJoi');
const sendEmail = require('./sendEmail');
const pagination = require('./pagination');

module.exports = {
  ...errorHandler,
  ...validators,
  ...helpers,
  ...token,
  ...redis,
  ...sendEmail,
  V: customJoi,
  Pagination: pagination,
};
