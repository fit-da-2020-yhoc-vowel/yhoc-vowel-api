const V = require('./customJoi');

class Pagination {
  static isValid(limit, page) {
    const defaultValue = Pagination.default();

    try {
      const newLimit = !limit ? defaultValue.limit : +limit;
      const newPage = !page ? defaultValue.page : +page;
      const schema = V.object({
        limit: V.number().min(defaultValue.limit).required(),
        page: V.number().min(defaultValue.page).required(),
      });

      const { error } = schema.validate({
        limit: newLimit,
        page: newPage,
      });
      if (error) {
        return {
          isValid: false,
          error,
        };
      }

      return {
        isValid: true,
        limit: newLimit,
        page: newPage,
        offset: (newPage - 1) * newLimit,
      };
    } catch (e) {
      return {
        error: e,
        isValid: false,
      };
    }
  }

  static default() {
    return {
      limit: 10,
      page: 1,
    };
  }
}

module.exports = Pagination;
