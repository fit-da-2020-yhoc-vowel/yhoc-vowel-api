const Joi = require('@hapi/joi');
const { isValidObjectId } = require('mongoose');
const libphonenumber = require('libphonenumber-js');
const moment = require('moment');
const {
  TWILIO_CODE_LENGTH,
  DEFAULT_COUNTRY_CODE,
  MIN_LEN_PW,
  MAX_LEN_PW,
  MIN_LEN_FULLNAME,
  MAX_LEN_FULLNAME,
} = require('@constants');
const { normalizeHumanName } = require('./helpers');

function removeAscent(str) {
  let tmp = str;
  if (tmp === null || tmp === undefined) return tmp;
  tmp = tmp
    .normalize('NFC')
    .toLowerCase()
    .replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
    .replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
    .replace(/ì|í|ị|ỉ|ĩ/g, 'i')
    .replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
    .replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
    .replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
    .replace(/đ/g, 'd');
  return tmp;
}

const customJoi = Joi.extend(
  (joi) => ({
    type: 'string',
    base: joi.string(),
    messages: {
      'string.phone.cant_parse':
        'Không thể chuyển đổi số điện thoại.',
      'string.code.invalid': 'Mã xác nhận không đúng định dạng.',
      'string.password.length': `Độ dài mật khẩu trong khoảng từ ${MIN_LEN_PW} đến ${MAX_LEN_PW}.`,
      'string.password.invalid': 'Mật khẩu phải gồm chữ và số.',
      'string.confirm_password.not_match':
        'Mật khẩu xác nhận không khớp.',
      'string.fullname.invalid': `Tên chỉ chứa kí tự chữ và độ dài trong khoảng ${MIN_LEN_FULLNAME} đến ${MAX_LEN_FULLNAME}`,
      'string.date.invalid': 'Ngày sai định dạng DD-MM-YYYY.',
      'string.birthday.invalid':
        'Ngày sinh phải trước ngày hiện tại.',
      'string.in.invalid': 'Value is not contain in array',
      'string.objectId.invalid': 'ObjectId is not valid',
      'string.searchtext.length':
        'Độ dài chuỗi tìm kiếm không quá 35 ký tự',
      'string.searchtext.invalid':
        'Chuỗi tìm kiếm không được có ký tự số hoặc ký tự đặc biệt',
    },
    rules: {
      phone: {
        validate(value, helpers, _args, _options) {
          // validate phone number
          try {
            // validate phone number
            const phoneNumber = libphonenumber.parsePhoneNumber(
              value,
              DEFAULT_COUNTRY_CODE,
            );
            return phoneNumber;
          } catch (err) {
            return helpers.error('string.phone.cant_parse');
          }
        },
      },
      code: {
        validate(value, helpers, _args, _options) {
          const pattern = new RegExp(
            `^[0-9]{${TWILIO_CODE_LENGTH}}$`,
          );
          if (pattern.test(value)) {
            return value;
          }
          return helpers.error('string.code.invalid');
        },
      },

      password: {
        validate(value, helpers, _args, _options) {
          if (
            value.length < MIN_LEN_FULLNAME ||
            value.length > MAX_LEN_FULLNAME
          ) {
            return helpers.error('string.password.length');
          }
          const pattern = new RegExp(
            `^((?=.*[a-zA-Z])(?=.*[0-9]))\\w{${MIN_LEN_PW},${MAX_LEN_PW}}$`,
            'g',
          );
          if (pattern.test(value)) {
            return value;
          }
          return helpers.error('string.password.invalid');
        },
      },
      confirmPassword: {
        multi: true, // Rule supports multiple invocations
        method(candidate) {
          return this.$_addRule({
            name: 'confirmPassword',
            args: { candidate },
          });
        },
        args: [
          {
            name: 'candidate',
            ref: true,
            assert: (value) => typeof value === 'string',
            message: 'must be a number',
          },
        ],
        validate(value, helpers, args, _options) {
          if (value === args.candidate) {
            return value;
          }
          return helpers.error('string.confirm_password.not_match');
        },
      },
      fullName: {
        validate(value, helpers, _args, _options) {
          const fullName = normalizeHumanName(value);

          const pattern = new RegExp(
            `^[A-Za-z\\s]{${MIN_LEN_FULLNAME},${MAX_LEN_FULLNAME}}$`,
          );

          if (pattern.test(removeAscent(fullName))) {
            return fullName;
          }
          return helpers.error('string.fullname.invalid');
        },
      },
      birthday: {
        validate(value, helpers, _args, _options) {
          try {
            const temp = moment(value, 'DD-MM-YYYY');
            if (temp.isAfter(moment(new Date()))) {
              return helpers.error('string.birthday.invalid');
            }
            return temp;
          } catch (e) {
            return helpers.error('string.date.invalid', e);
          }
        },
      },
      in: {
        method(array) {
          return this.$_addRule({
            name: 'in',
            args: { array },
          });
        },
        args: [
          {
            name: 'array',
            // ref: true,
            assert: (value) => typeof value === 'object',
            message: 'must be an array',
          },
        ],
        validate(value, helpers, args, _options) {
          if ((args.array || []).includes(value)) {
            return value;
          }
          return helpers.error('string.in.invalid');
        },
      },
      isObjectId: {
        validate(value, helpers, _args, _options) {
          if (isValidObjectId(value)) {
            return value;
          }
          return helpers.error('string.objectId.invalid');
        },
      },
      searchText: {
        validate(value, helpers, _args, _options) {
          const fullName = normalizeHumanName(value);
          if (
            fullName.length < 0 ||
            fullName.length > MAX_LEN_FULLNAME
          ) {
            return helpers.error('string.searchtext.length');
          }
          const pattern = new RegExp(
            `^[A-Za-z\\s]{${0},${MAX_LEN_FULLNAME}}$`,
          );
          if (pattern.test(removeAscent(fullName))) {
            return value;
          }
          return helpers.error('string.searchtext.invalid');
        },
      },
    },
  }),
  (joi) => ({
    type: 'number',
    base: joi.number(),
    messages: {
      'number.in.invalid': 'Value is not contain in array',
    },
    rules: {
      in: {
        method(array) {
          return this.$_addRule({
            name: 'in',
            args: { array },
          });
        },
        args: [
          {
            name: 'array',
            // ref: true,
            assert: (value) => typeof value === 'object',
            message: 'must be an array',
          },
        ],
        validate(value, helpers, args, _options) {
          if ((args.array || []).includes(value)) {
            return value;
          }
          return helpers.error('number.in.invalid');
        },
      },
    },
  }),
);

module.exports = customJoi;
