const nodemailer = require('nodemailer');
const { AppConfig } = require('@config');

const transport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: AppConfig.nodemailerEmail,
    pass: AppConfig.nodemailerPassword,
  },
});
module.exports = transport;
