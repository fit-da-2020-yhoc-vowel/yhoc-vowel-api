const redis = require('redis');
const { AppConfig } = require('@config');
const { logger } = require('@config/winston');
const { InternalServerError } = require('@utils');
const { promisify } = require('util');

let client = null;
try {
  client = redis.createClient({
    host: AppConfig.redisHost,
    port: AppConfig.redisPort,
  });

  logger.info(
    `Redis server is connected at port ${AppConfig.redisPort}`,
  );
} catch (e) {
  logger.error("Cant't connect to Redis server.");
  process.exit();
}

const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const expireAsync = promisify(client.expire).bind(client);
const delAsync = promisify(client.del).bind(client);

async function setRedisValue(key, value, expireAt = 0) {
  let temp = value;
  temp = JSON.stringify(temp);
  try {
    await setAsync(key, temp);
    if (expireAt > 0) {
      await expireAsync(key, expireAt);
    }
  } catch (e) {
    throw new InternalServerError(
      new Error(e),
      'Internal Server Error',
    );
  }
}

async function getRedisValue(key) {
  try {
    const value = await getAsync(key);
    let temp = value;
    try {
      temp = JSON.parse(temp);
    } catch (e) {
      temp = value;
    }
    return temp;
  } catch (e) {
    throw new InternalServerError(
      new Error(e),
      'Internal Server Error',
    );
  }
}

async function removeRedisValue(key) {
  try {
    const isDeleted = await delAsync(key);
    return isDeleted;
  } catch (e) {
    throw new InternalServerError(
      new Error(e),
      'Internal Server Error',
    );
  }
}

module.exports = {
  setRedisValue,
  getRedisValue,
  removeRedisValue,
};
