const jwt = require('jsonwebtoken');
const { promisify } = require('util');
const { AppConfig } = require('@config');

const tokenOptions = {
  expiresIn: AppConfig.tokenLivingTime,
};

class Token {
  static async signAsync(payload, options = {}) {
    const sign = promisify(jwt.sign);
    const token = await sign(payload, AppConfig.appSecret, {
      ...tokenOptions,
      ...options,
    });
    return token;
  }

  static async verifyAsync(token, options = {}) {
    const verify = promisify(jwt.verify);
    const data = await verify(token, AppConfig.appSecret, options);
    return data;
  }

  static async sign(payload, options = {}) {
    const token = jwt.sign(payload, AppConfig.appSecret, {
      ...tokenOptions,
      ...options,
    });
    return token;
  }

  static async verify(token, options = {}) {
    const data = jwt.verify(token, AppConfig.appSecret, options);
    return data;
  }
}

module.exports = { Token };
