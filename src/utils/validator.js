const checkTypes = require('check-types');
const libphonenumber = require('libphonenumber-js');
const validator = require('validator');
const moment = require('moment');
const lodash = require('lodash');
const {
  TWILIO_CODE_LENGTH,
  DEFAULT_COUNTRY_CODE,
  MIN_LEN_PW,
  MAX_LEN_PW,
  MIN_LEN_FULLNAME,
  MAX_LEN_FULLNAME,
  gender,
} = require('@constants');
const { BadRequest } = require('./errorHandler');

/**
 *
 * @param {string} userPhoneNumber
 * @param {string} countryCode
 */
function validatePhoneNumber(
  userPhoneNumber,
  countryCode = DEFAULT_COUNTRY_CODE,
) {
  // convert contry code
  const ctCode = countryCode.toUpperCase();
  // validate types
  if (
    !(
      checkTypes.nonEmptyString(userPhoneNumber) &&
      /^([A-Z]){2}$/.test(ctCode)
    )
  ) {
    throw new BadRequest(null, 'Tham số không đúng định dạng.');
  }

  try {
    // validate phone number
    const phoneNumber = libphonenumber.parsePhoneNumber(
      userPhoneNumber,
      ctCode,
    );
    return phoneNumber;
  } catch (err) {
    throw new BadRequest(err, 'Không thể chuyển đổi số điện thoại.');
  }
}

function validateReceiveCode(code) {
  const pattern = new RegExp(`^[0-9]{${TWILIO_CODE_LENGTH}}$`);
  return pattern.test(code);
}

function validatePassword(password) {
  // password must be at least 8 characters and contains both characters and numbers

  const pattern = new RegExp(
    `^((?=.*[a-zA-Z])(?=.*[0-9]))\\w{${MIN_LEN_PW},${MAX_LEN_PW}}$`,
    'g',
  );

  return pattern.test(password);
}

function isUUID(id, uuidVersion = 4) {
  if (!validator.isUUID(id, uuidVersion)) {
    throw new BadRequest(null, 'ID không đúng định dạng.');
  }
  return true;
}

function validateFullname(fullName) {
  if (
    fullName &&
    !(
      validator.isString(fullName) &&
      fullName.length >= MIN_LEN_FULLNAME &&
      fullName.length <= MAX_LEN_FULLNAME
    )
  ) {
    throw new BadRequest(
      null,
      `Độ dài của tên trong khoảng từ ${MIN_LEN_FULLNAME} đến ${MAX_LEN_FULLNAME}`,
    );
  }
  return true;
}

function validateDOB(birthday) {
  let dob = null;
  if (birthday) {
    const temp = moment(birthday, 'DD-MM-YYYY');
    if (!temp.isBefore(moment(Date.now()))) {
      throw new BadRequest(null, 'Ngày sinh không hợp lệ.');
    }
    dob = temp.toDate();
  }
  return dob;
}

function validateGender(userGender) {
  if (
    lodash.isNumber(userGender) &&
    !Object.values(gender).includes(userGender)
  ) {
    throw new BadRequest(null, 'Giới tính không đúng.');
  }
  return true;
}

function isEmail(email) {
  if (email && validator.isEmail(email)) {
    throw new BadRequest(null, 'Email không hợp lệ.');
  }
  return true;
}

module.exports = {
  validatePhoneNumber,
  validateReceiveCode,
  validatePassword,
  validateFullname,
  validateGender,
  validateDOB,
  isUUID,
  isEmail,
};
