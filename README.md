## Yhoc-Vowel-API

- MySQL
- Sequelize: ORM
- Twilio: verify phone number
- REST API services
- AWS S3: store images
- Redis: memcache

## Database

### Create database with charset and collate

```sql
CREATE DATABASE yhoc_vowel CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
```

### Add privileges to user

```sql
GRANT ALL PRIVILEGES ON yhoc_vowel.* TO 'yhoc_vowel'@'localhost';
```

### Import sql database

```bash
mysql -u yhoc_vowel -p yhoc_vowel < ~/yhoc-vowel-api/source/db_backup/db.sql
```

### Export sql database

```bash
mysqldump -u yhoc_vowel -p --databases yhoc_vowel > ~/backup/sql-$(date +'%F-%H-%M').sql
```

### SSL Configuration

[https://gist.github.com/bradtraversy/cd90d1ed3c462fe3bddd11bf8953a896](https://gist.github.com/bradtraversy/cd90d1ed3c462fe3bddd11bf8953a896)

### Build webapp

#### Compress

```bash
tar -czvf client.tar.gz ./*
```

#### Send to server

```bash
scp ~/dev/projects/yhoc-vowel-webapp/gvb-web/build/client.tar.gz root@165.22.104.29:/root/yhoc-vowel-api/source
```

#### Extract

```bash
tar -xzvf ~/yhoc-vowel-api/source/client.tar.gz -C ~/yhoc-vowel-api/source/client
```
