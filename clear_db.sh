echo "===================> Backup Mysql database <====================="
mysqldump -u yhoc_vowel -ptoimuontotnghiep --databases yhoc_vowel > ~/backup/sql-$(date +'%F-%H-%M').sql <<EOF
toimuontotnghiep
EOF

echo "===================> Delete Mysql Database <====================="
mysql -u yhoc_vowel -ptoimuontotnghiep <<EOF
use yhoc_vowel;
update selecting_path set current_path = null;
delete from path_suggestion;
delete from workout_times_comments;
delete from practice;
delete from workout_times;
delete from connect_doctor;
EOF

echo "===================> Remove audio files <====================="
rm ~/yhoc-vowel-api/source/public/audio/*


echo "===================> All Redis keys <====================="
redis-cli keys "*"
echo "===================> Remove Redis keys <====================="
redis-cli keys "*" | xargs redis-cli del

echo "===================> Remove Mongo db <====================="
~/mongo-client/bin/mongo "mongodb+srv://yhoc-vowel.n9muy.mongodb.net/yhoc-vowel" --username yhoc_vowel <<EOF
toimuontotnghiep
use yhoc-vowel
show collections
db.runCommand({find:"conversations"})
db.runCommand({delete:"conversations", deletes: [ { q: { }, limit: 0 } ]})
EOF
